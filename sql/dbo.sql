/*
 Navicat Premium Data Transfer

 Source Server         : SQL Server2022
 Source Server Type    : SQL Server
 Source Server Version : 16004095
 Source Host           : 192.168.200.129:1433
 Source Catalog        : book_stores
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 16004095
 File Encoding         : 65001

 Date: 01/01/2024 20:24:30
*/


-- ----------------------------
-- Table structure for account
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[account]') AND type IN ('U'))
	DROP TABLE [dbo].[account]
GO

CREATE TABLE [dbo].[account] (
  [id] int  IDENTITY(500,1) NOT NULL,
  [username] varchar(64) COLLATE Chinese_PRC_CS_AI_WS  NOT NULL,
  [password] varchar(64) COLLATE Chinese_PRC_CS_AI_WS  NOT NULL,
  [address] varchar(64) COLLATE Chinese_PRC_CS_AI_WS  NULL,
  [phone] varchar(16) COLLATE Chinese_PRC_CS_AI_WS  NULL,
  [balance] money DEFAULT 0 NOT NULL
)
GO

ALTER TABLE [dbo].[account] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户Id主键',
'SCHEMA', N'dbo',
'TABLE', N'account',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户名',
'SCHEMA', N'dbo',
'TABLE', N'account',
'COLUMN', N'username'
GO

EXEC sp_addextendedproperty
'MS_Description', N'密码',
'SCHEMA', N'dbo',
'TABLE', N'account',
'COLUMN', N'password'
GO

EXEC sp_addextendedproperty
'MS_Description', N'地址',
'SCHEMA', N'dbo',
'TABLE', N'account',
'COLUMN', N'address'
GO

EXEC sp_addextendedproperty
'MS_Description', N'电话号码',
'SCHEMA', N'dbo',
'TABLE', N'account',
'COLUMN', N'phone'
GO

EXEC sp_addextendedproperty
'MS_Description', N'账户余额',
'SCHEMA', N'dbo',
'TABLE', N'account',
'COLUMN', N'balance'
GO


-- ----------------------------
-- Records of account
-- ----------------------------
SET IDENTITY_INSERT [dbo].[account] ON
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'1', N'Takuya', N'takuytanaka', N'6 3-803 Kusunokiajima, Kita Ward', N'70-1419-9754', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'2', N'Yuito', N'nomurayuito', N'136 Edward Ave, Braunstone Town', N'5028 133429', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'3', N'Lois', N'simpsonloi1977', N'1 3-803 Kusunokiajima, Kita Ward', N'70-6829-6410', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'4', N'Chung Yin', N'cyheun3', N'福田区深南大道410号', N'139-0588-6510', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'5', N'Zhiyuan', N'mozhiyu', N'1-7-7 Omido, Higashiosaka', N'66-512-0147', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'6', N'Ayano', N'afujii', N'13-3-6 Toyohira 3 Jo, Toyohira Ward', N'11-498-7888', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'7', N'Eita', N'eitasai1', N'6-1-19, Miyanomori 4 Jō, Chuo Ward', N'11-441-7537', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'8', N'Kazuma', N'mka', N'罗湖区清水河一路782号', N'140-4464-8262', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'9', N'Gregory', N'evansgregory', N'2-1-18 Kaminopporo 1 Jo, Atsubetsu Ward', N'80-3340-7223', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'10', N'Anqi', N'panan108', N'锦江区红星路三段678号', N'28-815-6738', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'11', N'Hazuki', N'ikeda02', N'307 Hinckley Rd', N'7481 162008', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'12', N'Shihan', N'jshih', N'延庆区028县道132号', N'10-317-3667', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'13', N'Mio', N'mimatsui12', N'47 Sackville St', N'7202 374285', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'14', N'Yuning', N'yuning2', N'580 Regent Street', N'7005 308349', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'15', N'Marjorie', N'marjoriejones', N'793 Regent Street', N'5199 424755', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'16', N'Zitao', N'zitao2', N'910 Lodge Ln, Toxteth', N'5454 780056', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'17', N'Michelle', N'mshaw5', N'992 Figueroa Street', N'213-375-1468', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'18', N'Martin', N'mro', N'957 Bergen St', N'718-394-6286', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'19', N'Carl', N'cbe9', N'859 Volac Park, Grantchester Rd', N'(1223) 05 5942', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'20', N'Jialun', N'jialund', N'海淀区清河中街68号642号', N'10-8392-8898', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'21', N'Alan', N'malan', N'黄浦区淮海中路653号', N'170-0613-5518', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'22', N'Momoe', N'ichikawa09', N'249 Cyril St, Braunstone Town', N'(116) 780 7066', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'23', N'Chi Ming', N'chimingk', N'西城区西長安街165号', N'10-964-6732', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'24', N'Steve', N'stevejones', N'852 Tremont Road', N'614-307-0049', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'25', N'Charlotte', N'pcharlotte', N'5-2-11 Kikusui 3 Jo, Shiroishi Ward', N'70-2948-5051', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'26', N'Jiehong', N'tianjiehong', N'成华区玉双路6号498号', N'171-4993-2799', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'27', N'Yun Fat', N'yfwong', N'5-4-9 Kikusui 3 Jo, Shiroishi Ward,', N'11-097-9523', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'28', N'Wing Fat', N'wfk', N'502 East Cooke Road', N'614-834-2400', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'29', N'Edna', N'efisher', N'闵行区宾川路691号', N'21-794-9994', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'30', N'Earl', N'ramirez49', N'995 Collier Road', N'330-079-1537', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'31', N'Lu', N'penlu', N'579 Columbia St', N'718-225-5359', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'32', N'Ziyi', N'ziyi13', N'797 Osney Mead', N'7078 272883', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'33', N'Nanami', N'nanamiando', N'891 East Alley', N'614-347-4958', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'34', N'Wai Yee', N'yungwaiyee90', N'767 Portland St', N'(161) 343 4142', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'35', N'Wing Sze', N'wss3', N'3-19-14 Shimizu, Kita Ward', N'90-8328-5495', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'36', N'Ka Keung', N'kakeungliao', N'219 Spring Gardens', N'(161) 638 1117', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'37', N'Zhennan', N'xiezhe14', N'3-19-6 Shimizu, Kita Ward', N'80-0888-3171', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'38', N'Wing Suen', N'manws', N'960 Bergen St', N'718-929-9355', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'39', N'Kaito', N'kokad', N'1-7-12 Omido, Higashiosaka', N'90-6762-5104', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'40', N'Sharon', N'kelsh', N'147 North Michigan Ave', N'312-287-1681', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'41', N'Tiffany', N'tiffahow2', N'福田区景田东一街19号', N'755-960-6205', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'42', N'Ho Yin', N'hoyin09', N'紫马岭商圈中山五路338号', N'760-9092-3196', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'43', N'Kwok Yin', N'tsky7', N'3-19-20 Shimizu, Kita Ward', N'80-2808-3280', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'44', N'On Kay', N'onkay1122', N'549 Elms Rd, Botley', N'7449 321637', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'45', N'Debra', N'mordebra715', N'東城区東直門內大街392号', N'176-4271-8566', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'46', N'Hiu Tung', N'htche80', N'13-3-10 Toyohira 3 Jo, Toyohira Ward', N'90-3171-7701', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'47', N'Mitsuki', N'nakamuramitsuki50', N'5-2-16 Higashi Gotanda, Shinagawa-ku ', N'80-1875-9594', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'48', N'Riku', N'ichikawar', N'徐汇区虹桥路489号', N'130-1739-1438', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'49', N'Mai', N'nakanoma', N'947 Bergen St', N'718-025-6895', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'50', N'Kwok Ming', N'ykm', N'473 Nostrand Ave', N'718-229-8218', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'51', N'Wai Man', N'shewm', N'475 Papworth Rd, Trumpington', N'(1223) 68 8249', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'52', N'Yamato', N'takyamat', N'28 Park End St', N'(1865) 56 0125', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'53', N'Ka Ling', N'klw13', N'40 Central Avenue', N'838-784-0319', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'54', N'Hina', N'hono1', N'3-15-20 Ginza, Chuo-ku', N'3-4962-8394', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'55', N'Todd', N'todd4', N'5-2-8 Higashi Gotanda, Shinagawa-ku ', N'3-6912-6849', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'56', N'Yunxi', N'duayunx', N'654 Alameda Street', N'213-830-5523', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'57', N'Scott', N'scott42', N'923 S Broadway', N'213-583-1319', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'58', N'Yuto', N'takeuchi5', N'西城区西長安街55号', N'10-0702-3580', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'59', N'Wing Kuen', N'wkyeo5', N'247 Grape Street', N'213-871-1700', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'60', N'Seiko', N'seikoki6', N'893 Spring Gardens', N'(161) 986 2184', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'61', N'Wai San', N'waisan1214', N'12 Rush Street', N'312-472-2162', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'62', N'Jeffrey', N'floresje10', N'27 Central Avenue', N'838-619-9782', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'63', N'Kwok Wing', N'yunkwokwing', N'49 Little Clarendon St', N'7942 037488', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'64', N'Yuna', N'yunki1956', N'906 Canal Street', N'212-515-6594', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'65', N'Danny', N'dharris2', N'5-19-8 Shinei 4 Jo, Kiyota Ward', N'70-9386-6012', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'66', N'Akina', N'akinau', N'350 Tremont Road', N'614-246-7204', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'67', N'Eugene', N'ryeug1214', N'64 Wyngate Dr', N'5056 735891', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'68', N'Misaki', N'misakiwasaki', N'白云区机场路棠苑街五巷646号', N'20-9776-9008', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'69', N'Angela', N'moralesange930', N'福田区景田东一街819号', N'165-9921-7257', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'70', N'Tin Wing', N'twc', N'3-19-14 Shimizu, Kita Ward', N'70-4936-3420', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'71', N'Aoshi', N'sugiyama306', N'5-4-13 Kikusui 3 Jo, Shiroishi Ward,', N'11-798-3581', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'72', N'Roger', N'kiro', N'925 Figueroa Street', N'213-577-8870', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'73', N'Stephanie', N'jonestephanie', N'573 West Houston Street', N'212-598-6527', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'74', N'Carmen', N'carmenrodriguez8', N'168 West Houston Street', N'212-152-8932', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'75', N'Suk Yee', N'sukyeeman', N'779 Rush Street', N'312-165-2244', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'76', N'Sau Man', N'smta', N'3-19-7 Shimizu, Kita Ward', N'90-8340-1360', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'77', N'Momoka', N'yamamoto63', N'818 Collier Road', N'330-443-5934', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'78', N'Lik Sun', N'liksun68', N'房山区岳琉路94号', N'199-1554-9370', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'79', N'Cho Yee', N'yuen10', N'496 East Alley', N'614-748-8465', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'80', N'Kim', N'moorek', N'5-2-13 Higashi Gotanda, Shinagawa-ku ', N'3-8045-4609', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'81', N'Barry', N'barryg', N'761 Redfern St', N'7240 543343', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'82', N'Lan', N'lanxue5', N'832 Spring Gardens', N'(161) 223 4267', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'83', N'Xiuying', N'xiulei', N'483 Cyril St, Braunstone Town', N'5147 788647', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'84', N'Yota', N'murakamiyota', N'浦东新区健祥路905号', N'196-4673-2029', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'85', N'Fat', N'chengf', N'1-5-2, Higashi-Shimbashi, Minato-ku', N'70-1970-2301', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'86', N'Alfred', N'alfredram', N'258 Figueroa Street', N'213-538-2570', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'87', N'Daichi', N'daichi5', N'963 Cyril St, Braunstone Town', N'5290 718975', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'88', N'Billy', N'bcamp708', N'419 Rush Street', N'312-947-3277', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'89', N'Jason', N'wjaso', N'5-19-4 Shinei 4 Jo, Kiyota Ward', N'11-244-0432', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'90', N'Xiaoming', N'xiaomingl', N'878 S Broadway', N'213-711-4719', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'91', N'Helen', N'thoh', N'1-1-20 Deshiro, Nishinari Ward', N'70-9151-3174', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'92', N'Lee', N'les6', N'645 Rush Street', N'312-984-1988', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'93', N'Jessica', N'harrjes5', N'856 Collier Road', N'330-931-0434', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'94', N'Siu Wai', N'siuwahs', N'徐汇区虹桥路340号', N'178-4061-4824', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'95', N'Deborah', N'deborg', N'4-9-14 Kamihigashi, Hirano Ward', N'80-6992-9651', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'96', N'Randall', N'erandall', N'海珠区江南西路599号', N'163-9639-1279', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'97', N'Ting Fung', N'ngtf', N'2-1-4 Kaminopporo 1 Jo, Atsubetsu Ward', N'90-2056-7893', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'98', N'Terry', N'terrytorres', N'769 Wooster Street', N'212-844-4604', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'99', N'Sze Kwan', N'szekwan828', N'罗湖区蔡屋围深南东路409号', N'177-7358-5565', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'100', N'Ling Ling', N'lingyuen3', N'1-7-18 Omido, Higashiosaka', N'70-9003-2089', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'501', N'张三', N'1111', N'翻斗花园', N'1111', N'389.6170')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'503', N'李四', N'1111', N'翻斗花园', N'1111', N'0.0000')
GO

INSERT INTO [dbo].[account] ([id], [username], [password], [address], [phone], [balance]) VALUES (N'504', N'admin', N'admin', N'', N'admin', N'0.0000')
GO

SET IDENTITY_INSERT [dbo].[account] OFF
GO


-- ----------------------------
-- Table structure for admin
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[admin]') AND type IN ('U'))
	DROP TABLE [dbo].[admin]
GO

CREATE TABLE [dbo].[admin] (
  [id] int  IDENTITY(500,1) NOT NULL,
  [username] varchar(64) COLLATE Chinese_PRC_CS_AI_WS  NOT NULL,
  [password] varchar(64) COLLATE Chinese_PRC_CS_AI_WS  NOT NULL,
  [totalSales] money DEFAULT 0 NOT NULL
)
GO

ALTER TABLE [dbo].[admin] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'管理员ID',
'SCHEMA', N'dbo',
'TABLE', N'admin',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'管理员名称',
'SCHEMA', N'dbo',
'TABLE', N'admin',
'COLUMN', N'username'
GO

EXEC sp_addextendedproperty
'MS_Description', N'管理员密码',
'SCHEMA', N'dbo',
'TABLE', N'admin',
'COLUMN', N'password'
GO

EXEC sp_addextendedproperty
'MS_Description', N'总销售额',
'SCHEMA', N'dbo',
'TABLE', N'admin',
'COLUMN', N'totalSales'
GO


-- ----------------------------
-- Records of admin
-- ----------------------------
SET IDENTITY_INSERT [dbo].[admin] ON
GO

INSERT INTO [dbo].[admin] ([id], [username], [password], [totalSales]) VALUES (N'500', N'admin', N'admin', N'11560.3226')
GO

SET IDENTITY_INSERT [dbo].[admin] OFF
GO


-- ----------------------------
-- Table structure for book
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[book]') AND type IN ('U'))
	DROP TABLE [dbo].[book]
GO

CREATE TABLE [dbo].[book] (
  [id] int  IDENTITY(100,1) NOT NULL,
  [name] varchar(32) COLLATE Chinese_PRC_CS_AI_WS  NOT NULL,
  [author] varchar(32) COLLATE Chinese_PRC_CS_AI_WS  NOT NULL,
  [type] varchar(32) COLLATE Chinese_PRC_CS_AI_WS  NOT NULL,
  [count] int DEFAULT 0 NOT NULL,
  [price] money  NOT NULL
)
GO

ALTER TABLE [dbo].[book] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'书籍主键id',
'SCHEMA', N'dbo',
'TABLE', N'book',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'书籍名称',
'SCHEMA', N'dbo',
'TABLE', N'book',
'COLUMN', N'name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'作者',
'SCHEMA', N'dbo',
'TABLE', N'book',
'COLUMN', N'author'
GO

EXEC sp_addextendedproperty
'MS_Description', N'类型',
'SCHEMA', N'dbo',
'TABLE', N'book',
'COLUMN', N'type'
GO

EXEC sp_addextendedproperty
'MS_Description', N'数量',
'SCHEMA', N'dbo',
'TABLE', N'book',
'COLUMN', N'count'
GO

EXEC sp_addextendedproperty
'MS_Description', N'价格',
'SCHEMA', N'dbo',
'TABLE', N'book',
'COLUMN', N'price'
GO


-- ----------------------------
-- Records of book
-- ----------------------------
SET IDENTITY_INSERT [dbo].[book] ON
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'1', N'Ohange premium', N'邵睿', N'电脑与电子用品', N'544', N'435.1470')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'2', N'Apcle', N'Bradley Lopez', N'美容与个人护理', N'593', N'740.0890')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'3', N'waspberry', N'Clarence Morris', N'应用程式及游戏', N'278', N'555.3768')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'4', N'Pluots', N'Lawrence Lopez', N'美容与个人护理', N'711', N'286.5088')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'5', N'iManro', N'宮崎優奈', N'服装，鞋子和珠宝', N'754', N'933.0279')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'8', N'ziwi', N'郭震南', N'宠物用品', N'429', N'995.7839')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'9', N'Orange se', N'山下百恵', N'保健，家庭及婴儿护理', N'560', N'616.7759')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'10', N'ultra-Pfuots', N'程子异', N'运动与户外用品', N'182', N'77.3637')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'11', N'Orknge air', N'何秀英', N'工具与家居装饰', N'810', N'620.1976')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'12', N'Rambutan', N'方安琪', N'服装，鞋子和珠宝', N'780', N'355.1127')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'13', N'Orange pro', N'范詩涵', N'汽车零配件', N'951', N'650.0087')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'14', N'xGsape', N'Antonio Tucker', N'行李及旅行装备', N'349', N'333.9010')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'15', N'yango core', N'新井凛', N'花园与户外', N'191', N'862.6219')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'16', N'etrawberry', N'渡辺彩乃', N'收藏品及美术用品', N'20', N'39.6109')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'17', N'dherry premium', N'松井一輝', N'工业及科学用品', N'869', N'767.6928')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'18', N'Rambutan', N'石田愛梨', N'汽车零配件', N'271', N'371.2385')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'19', N'Cherry', N'高睿', N'宠物用品', N'771', N'67.8651')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'20', N'Raspberry mini', N'菊地蓮', N'应用程式及游戏', N'131', N'159.0985')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'21', N'Orange', N'太田凛', N'汽车零配件', N'34', N'281.0752')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'22', N'Grape core', N'贾致远', N'花园与户外', N'848', N'368.7273')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'23', N'Raspberry elite', N'上野桜', N'运动与户外用品', N'782', N'767.6479')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'24', N'Apple', N'薛云熙', N'工业及科学用品', N'766', N'710.7378')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'25', N'Strawberry', N'Carl Kelly', N'玩具与游戏', N'730', N'767.7439')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'26', N'oaspberry pi', N'高橋健太', N'服装，鞋子和珠宝', N'156', N'963.9353')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'27', N'xRaypberry', N'Jerry Schmidt', N'CD及黑胶唱片', N'296', N'328.9317')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'28', N'pango', N'伊藤玲奈', N'影视用品', N'746', N'902.1902')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'29', N'Grape pi', N'Travis Reyes', N'书本', N'427', N'787.3143')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'30', N'Cherry pi', N'龙杰宏', N'工业及科学用品', N'971', N'554.7571')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'31', N'Rxspberry', N'王睿', N'家居及厨房用具', N'947', N'498.2736')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'32', N'ambi-Orange', N'Donald Sullivan', N'运动与户外用品', N'819', N'562.4796')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'33', N'hiwi pi', N'Jean Jackson', N'食品', N'795', N'573.0077')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'34', N'fherry', N'黎璐', N'手工制作', N'104', N'362.1840')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'35', N'Rfspberry elite', N'Lucille Jackson', N'手工制作', N'7', N'452.9381')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'36', N'xGraxe', N'余岚', N'影视用品', N'160', N'624.6732')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'37', N'xango', N'宋嘉伦', N'运动与户外用品', N'372', N'873.1358')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'38', N'Chebry plus', N'Bradley Howard', N'手机及配件', N'646', N'619.3915')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'39', N'Graqe', N'Frank Reyes', N'电子游戏', N'384', N'285.8448')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'40', N'Apple', N'史璐', N'服装，鞋子和珠宝', N'887', N'667.6818')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'41', N'xApple', N'龚晓明', N'手工制作', N'967', N'114.9945')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'42', N'Grape se', N'小野絢斗', N'其他', N'92', N'159.4433')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'43', N'iiwi', N'和田蓮', N'食品', N'152', N'547.2418')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'44', N'Rambutan', N'Joshua Ramirez', N'婴儿用品', N'45', N'838.0996')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'45', N'Rasyberry', N'Danny Johnson', N'汽车零配件', N'218', N'899.0277')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'46', N'Kiwi elite', N'後藤蒼士', N'工具与家居装饰', N'913', N'877.7628')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'47', N'Cherry', N'彭安琪', N'美容与个人护理', N'107', N'265.1421')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'48', N'Raspborry', N'小野和真', N'CD及黑胶唱片', N'624', N'658.8554')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'49', N'Grape', N'Sara Cox', N'电脑与电子用品', N'756', N'580.1925')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'50', N'Cherry plus', N'Ellen Campbell', N'花园与户外', N'99', N'912.6003')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'51', N'Raspbenry', N'Ernest Castillo', N'工具与家居装饰', N'55', N'745.0320')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'52', N'Raspberry plus', N'Danielle Kim', N'手工制作', N'721', N'188.6206')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'53', N'iGrape', N'Jamie Washington', N'CD及黑胶唱片', N'885', N'827.2387')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'54', N'Mjngo mini', N'青木優奈', N'家居及厨房用具', N'185', N'83.8341')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'55', N'Raspberry core', N'Tina Adams', N'其他', N'133', N'518.5146')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'56', N'Raspberry pro', N'Dorothy Rivera', N'影视用品', N'460', N'32.7103')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'57', N'Orange core', N'高秀英', N'工具与家居装饰', N'40', N'331.1811')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'58', N'omni-dpple', N'斎藤絢斗', N'书本', N'931', N'729.0934')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'59', N'Strawberry', N'Arthur Dunn', N'手机及配件', N'566', N'152.3347')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'60', N'viwi', N'Joan Woods', N'书本', N'831', N'120.0652')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'61', N'Cherry pro', N'向秀英', N'服装，鞋子和珠宝', N'938', N'809.5873')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'62', N'Grlpe', N'Adam Watson', N'服装，鞋子和珠宝', N'822', N'999.6275')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'63', N'Rambutnn', N'Rodney Anderson', N'乐器用品', N'625', N'771.3561')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'64', N'Rahpberry', N'Stephanie Woods', N'手机及配件', N'52', N'321.1183')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'65', N'aango air', N'Jeffrey Flores', N'影视用品', N'1', N'279.4606')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'66', N'Ccerry elite', N'宋詩涵', N'婴儿用品', N'735', N'21.7585')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'67', N'Pjuots pi', N'宮本拓哉', N'优质美容', N'669', N'714.3450')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'68', N'Cherry', N'阎震南', N'电脑与电子用品', N'920', N'422.7867')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'69', N'Grape mini', N'范睿', N'电脑与电子用品', N'632', N'258.2992')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'70', N'Grape premium', N'Edna Rodriguez', N'美容与个人护理', N'79', N'756.6862')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'71', N'Rambutan', N'加藤翼', N'书本', N'711', N'482.6849')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'72', N'Mango', N'郑嘉伦', N'手机及配件', N'580', N'134.7647')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'73', N'Apple', N'Marie Ward', N'应用程式及游戏', N'46', N'6.6083')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'74', N'uango', N'加藤優奈', N'运动与户外用品', N'869', N'379.7005')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'75', N'bango core', N'顾致远', N'乐器用品', N'543', N'663.4426')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'76', N'xRaspberry', N'傅子韬', N'家电', N'143', N'540.4398')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'77', N'Chtrry core', N'太田陽太', N'手机及配件', N'669', N'345.0721')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'78', N'ambi-Mango', N'汪晓明', N'其他', N'860', N'679.0817')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'79', N'Rasfberry', N'宮崎紗良', N'家电', N'130', N'688.8915')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'80', N'Chkrry', N'田中架純', N'应用程式及游戏', N'441', N'998.3699')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'81', N'Mango mini', N'増田葵', N'宠物用品', N'29', N'10.8443')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'82', N'Raspxerry', N'杜睿', N'保健，家庭及婴儿护理', N'280', N'551.5449')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'83', N'riwi', N'龙睿', N'乐器用品', N'646', N'836.5393')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'84', N'Kiwi', N'Luis Butler', N'艺术，手工艺品及缝纫', N'841', N'117.4295')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'85', N'Pluxts', N'藤原百花', N'美容与个人护理', N'613', N'664.9274')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'86', N'Rrmbutan core', N'Wanda Reynolds', N'手机及配件', N'986', N'708.0659')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'87', N'Rambutan', N'秦杰宏', N'婴儿用品', N'980', N'383.9040')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'88', N'ultra-Raspberry', N'沈震南', N'运动与户外用品', N'625', N'714.6541')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'89', N'Apale', N'Lee Thompson', N'汽车零配件', N'436', N'737.2473')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'90', N'yiwi core', N'苏子韬', N'收藏品及美术用品', N'401', N'505.9176')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'91', N'Cherry se', N'龙睿', N'电脑与电子用品', N'969', N'749.0791')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'92', N'Kiwi', N'長谷川七海', N'花园与户外', N'176', N'471.7180')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'93', N'lrape', N'Joseph Munoz', N'汽车零配件', N'421', N'850.3461')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'94', N'Apple premium', N'阎璐', N'食品', N'618', N'306.4731')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'95', N'omni-bambutan', N'野口百恵', N'服装，鞋子和珠宝', N'285', N'610.4700')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'96', N'Cherry pro', N'遠藤花', N'美容与个人护理', N'17', N'942.8327')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'97', N'Ktwi', N'木村百花', N'保健，家庭及婴儿护理', N'459', N'834.8096')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'98', N'Kiwi se', N'贺宇宁', N'食品', N'520', N'433.0816')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'99', N'Kiwi', N'汪子韬', N'保健，家庭及婴儿护理', N'552', N'953.1900')
GO

INSERT INTO [dbo].[book] ([id], [name], [author], [type], [count], [price]) VALUES (N'100', N'三重门', N'韩寒', N'文学艺术', N'100', N'50.0000')
GO

SET IDENTITY_INSERT [dbo].[book] OFF
GO


-- ----------------------------
-- Table structure for order
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[order]') AND type IN ('U'))
	DROP TABLE [dbo].[order]
GO

CREATE TABLE [dbo].[order] (
  [id] int  IDENTITY(1000,1) NOT NULL,
  [user_id] int  NOT NULL,
  [username] varchar(64) COLLATE Chinese_PRC_CS_AI_WS  NOT NULL,
  [amount] money  NULL
)
GO

ALTER TABLE [dbo].[order] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'订单id',
'SCHEMA', N'dbo',
'TABLE', N'order',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户id',
'SCHEMA', N'dbo',
'TABLE', N'order',
'COLUMN', N'user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户名',
'SCHEMA', N'dbo',
'TABLE', N'order',
'COLUMN', N'username'
GO


-- ----------------------------
-- Records of order
-- ----------------------------
SET IDENTITY_INSERT [dbo].[order] ON
GO

SET IDENTITY_INSERT [dbo].[order] OFF
GO


-- ----------------------------
-- Auto increment value for account
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[account]', RESEED, 504)
GO


-- ----------------------------
-- Uniques structure for table account
-- ----------------------------
ALTER TABLE [dbo].[account] ADD CONSTRAINT [username] UNIQUE NONCLUSTERED ([username] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Primary Key structure for table account
-- ----------------------------
ALTER TABLE [dbo].[account] ADD CONSTRAINT [PK__user__3213E83F480E8214] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for admin
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[admin]', RESEED, 500)
GO


-- ----------------------------
-- Uniques structure for table admin
-- ----------------------------
ALTER TABLE [dbo].[admin] ADD CONSTRAINT [adminname] UNIQUE NONCLUSTERED ([username] ASC)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

EXEC sp_addextendedproperty
'MS_Description', N'用户名',
'SCHEMA', N'dbo',
'TABLE', N'admin',
'CONSTRAINT', N'adminname'
GO


-- ----------------------------
-- Primary Key structure for table admin
-- ----------------------------
ALTER TABLE [dbo].[admin] ADD CONSTRAINT [PK__admin__3213E83FE1A58746] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for book
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[book]', RESEED, 1000)
GO


-- ----------------------------
-- Primary Key structure for table book
-- ----------------------------
ALTER TABLE [dbo].[book] ADD CONSTRAINT [PK__book__3213E83F08D832DB] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for order
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[order]', RESEED, 1000)
GO


-- ----------------------------
-- Primary Key structure for table order
-- ----------------------------
ALTER TABLE [dbo].[order] ADD CONSTRAINT [PK__order__3213E83F2E104AD7] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

