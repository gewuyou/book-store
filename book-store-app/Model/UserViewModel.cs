﻿using System;
using System.Collections.Generic;
using book_store_app.Data.Dao;
using book_store_app.Entity;

namespace book_store_app.Model
{
    /// <summary>
    /// 用户管理视图模型
    /// </summary>
    public class UserViewModel
    {
        private static readonly Lazy<UserViewModel> Instance = new Lazy<UserViewModel>(() => new UserViewModel());
        
        private readonly UserDao _userDao;

        private UserViewModel()
        {
            _userDao = UserDao.GetInstance();
        }

        public static UserViewModel GetInstance()
        {
           return Instance.Value;
        }

        /// <summary>
        /// 保存用户
        /// </summary>
        /// <param name="user">用户对象</param>
        /// <returns>受影响的行数</returns>
        public int Save(User user)
        {
            if (user.Id == 0 || _userDao.GetUserById(user.Id) == null)
            {
                return _userDao.Save(user);
                
            }
            return _userDao.Update(user);
        }

        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <returns>用户列表集合</returns>
        public List<User> GetUsers()
        {
            return _userDao.GetUsers();
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="id">用户Id</param>
        /// <returns>受影响的行数</returns>
        public int Delete(int id)
        {
            return _userDao.Delete(id);
        }

        /// <summary>
        /// 搜索用户
        /// </summary>
        /// <param name="searchValueText">搜索值</param>
        /// <returns>搜索结果集合</returns>
        public List<User> Search(string searchValueText)
        {
            return _userDao.Search(searchValueText);
        }
    }
}