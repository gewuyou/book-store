﻿using System;
using book_store_app.Constant;
using book_store_app.Data.Dao;
using book_store_app.Entity;

namespace book_store_app.Model
{
    /// <summary>
    /// 注册视图模型
    /// </summary>
    public class RegisterViewModel
    {
        private static readonly Lazy<RegisterViewModel> Instance =
            new Lazy<RegisterViewModel>(() => new RegisterViewModel());
        

        private readonly UserDao _userDao;

        private RegisterViewModel()
        {
            _userDao = UserDao.GetInstance();
        }

        public static RegisterViewModel GetInstance()
        {
            return Instance.Value;
        }

        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="username">用户名</param>
        /// <param name="password">密码</param>
        /// <returns>成功返回true反之false</returns>
        public bool Register(string username, string password)
        {
            var user = _userDao.GetUserByUsername(username);
            if (user != null)
            {
                return false;
            }

            return _userDao.Save(new User
            {
                Username = username,
                Password = password,
                Address = StringConstant.EmptyString,
                Balance = 0,
                Phone = StringConstant.EmptyString,
            }) > 0;
        }
    }
}