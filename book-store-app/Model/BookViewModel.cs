﻿using System;
using System.Collections.Generic;
using book_store_app.Data.Dao;
using book_store_app.Entity;

namespace book_store_app.Model
{
    /// <summary>
    /// 书籍视图模型
    /// </summary>
    public class BookViewModel
    {
        private static readonly Lazy<BookViewModel> Instance = new Lazy<BookViewModel>(() => new BookViewModel());
        
        private readonly BookDao _bookDao;

        private BookViewModel()
        {
            _bookDao = BookDao.GetInstance();
        }

        public static BookViewModel GetInstance()
        {
          return Instance.Value;
        }

        /// <summary>
        /// 保存书籍
        /// </summary>
        /// <param name="book">书籍</param>
        /// <returns>受影响行数</returns>
        public int Save(Book book)
        {
            if (book.Id == 0||_bookDao.GetBookById(book.Id) == null)
            {
                return _bookDao.Save(book);
            }
            return _bookDao.Update(book);
        }

        /// <summary>
        /// 获取书籍列表
        /// </summary>
        /// <returns>书籍列表</returns>
        public List<Book> GetBooks()
        {
            return _bookDao.GetBooks();
        }

        /// <summary>
        /// 删除书籍
        /// </summary>
        /// <param name="id">书籍ID</param>
        /// <returns>受影响行数</returns>
        public int Delete(int id)
        {
            return _bookDao.Delete(id);
        }

        /// <summary>
        /// 搜索书籍
        /// </summary>
        /// <param name="searchValueText">搜索值</param>
        /// <returns>搜索结果集合</returns>
        public List<Book> Search(string searchValueText)
        {
            return _bookDao.Search(searchValueText);
        }
    }
}