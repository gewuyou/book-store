﻿using System;
using book_store_app.Data.Dao;
using book_store_app.Entity;

namespace book_store_app.Model
{
    /// <summary>
    /// 个人中心视图模型
    /// </summary>
    public class PersonalCenterViewModel
    {
        private static readonly Lazy<PersonalCenterViewModel> Instance = new Lazy<PersonalCenterViewModel>(() => new PersonalCenterViewModel());

        private readonly UserDao _userDao;

        private PersonalCenterViewModel()
        {
            _userDao = UserDao.GetInstance();
        }

        public static PersonalCenterViewModel GetInstance()
        {
           return Instance.Value;
        }

        public int Update(User user)
        {
            return _userDao.Update(user);
        }
    }
}