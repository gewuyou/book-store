﻿using System;
using book_store_app.Data.Dao;

namespace book_store_app.Model
{
    /// <summary>
    /// 账户管理视图模型
    /// </summary>
    public class AccountManagerViewModel
    {
        private static readonly Lazy<AccountManagerViewModel> Instance =
            new Lazy<AccountManagerViewModel>(() => new AccountManagerViewModel());
        
        private readonly UserDao _userDao;

        private readonly AdminDao _adminDao;

        private readonly BookDao _bookDao;

        private AccountManagerViewModel()
        {
            _userDao = UserDao.GetInstance();
            _adminDao = AdminDao.GetInstance();
            _bookDao = BookDao.GetInstance();
        }

        public static AccountManagerViewModel GetInstance()
        {
            return Instance.Value;
        }

        /// <summary>
        /// 返回书籍库存数
        /// </summary>
        /// <returns>书籍库存数</returns>
        public int BookCount()
        {
            return _bookDao.BookCount();
        }

        /// <summary>
        /// 返回总用户数
        /// </summary>
        /// <returns>总用户数</returns>
        public int UserCount()
        {
            return _userDao.UserCount();
        }

        /// <summary>
        /// 返回总收益
        /// </summary>
        /// <returns>总收益</returns>
        public decimal GetTotalSales()
        {
            return _adminDao.GetTotalSales();
        }
    }
}