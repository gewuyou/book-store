﻿using System;
using book_store_app.Data;
using book_store_app.Data.Dao;

namespace book_store_app.Model
{
    /// <summary>
    /// 登录视图模型
    /// </summary>
    public class LoginViewModel
    {
        private static readonly Lazy<LoginViewModel> Instance = new Lazy<LoginViewModel>(() => new LoginViewModel());
        
        private readonly DataContext _dataContext;

        private readonly UserDao _userDao;

        private readonly AdminDao _adminDao;

        private LoginViewModel()
        {
            _userDao = UserDao.GetInstance();
            _adminDao = AdminDao.GetInstance();
            _dataContext = DataContext.GetInstance();
        }

        public static LoginViewModel GetInstance()
        {
           return Instance.Value;
        }
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="username">用户名</param>
        /// <param name="password">密码</param>
        /// <returns>登录结果</returns>
        public bool Login(string username, string password)
        {
            // 判断登录的是否是管理员
            var admin = _adminDao.GetAdminByUsername(username);
            if (admin != null)
            {
                if (!password.Equals(admin.Password))
                {
                    return false;
                }
                _dataContext.Add("loginAdmin", admin);
                return true;
            }
            // 判断登录的是否是用户
            var user = _userDao.GetUserByUsername(username);
            if (user == null)
            {
                return false;
            }
            if (!password.Equals(user.Password))
            {
                return false;
            }
            _dataContext.Add("loginUser", user);
            return true;
        }
    }
}