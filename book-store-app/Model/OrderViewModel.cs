﻿using System;
using System.Collections.Generic;
using book_store_app.Data.Dao;
using book_store_app.Entity;

namespace book_store_app.Model
{
    /// <summary>
    /// 书籍商城视图模型(懒得改类名了)
    /// </summary>
    public class OrderViewModel
    {
        private static readonly Lazy<OrderViewModel> Instance = new Lazy<OrderViewModel>(() => new OrderViewModel());
        
        private readonly BookDao _bookDao;
        

        private OrderViewModel()
        {
            _bookDao = BookDao.GetInstance();
        }

        public static OrderViewModel GetInstance()
        {
         return Instance.Value;
        }

        /// <summary>
        /// 搜索书籍 
        /// </summary>
        /// <param name="searchValueText">搜索值</param>
        /// <returns>搜索结果集合</returns>
        public List<Book> Search(string searchValueText)
        {
            return _bookDao.Search(searchValueText);
        }

        /// <summary>
        /// 获取书籍列表
        /// </summary>
        /// <returns>书籍列表</returns>
        public List<Book> GetBooks()
        {
            return _bookDao.GetBooks();
        }
    }
}