﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using book_store_app.Data;
using book_store_app.Data.Dao;
using book_store_app.Entity;

namespace book_store_app.Model
{
    /// <summary>
    /// 购物车视图模型
    /// </summary>
    public class ShoppingCartViewModel
    {
        private static readonly Lazy<ShoppingCartViewModel> Instance = new Lazy<ShoppingCartViewModel>(() => new ShoppingCartViewModel());

        private readonly BookDao _bookDao;
        
        private readonly AdminDao _adminDao;
        
        private readonly UserDao _userDao;
        
        private readonly DataContext _dataContext;
        
        /// <summary>
        /// 数据库连接
        /// </summary>
        private readonly SqlConnection _sqlConnection;

        private ShoppingCartViewModel()
        {
            _bookDao = BookDao.GetInstance();
            _adminDao = AdminDao.GetInstance();
            _userDao = UserDao.GetInstance();
            _dataContext = DataContext.GetInstance();
            _sqlConnection = DatabaseHelper.GetConnection();
        }

        public static ShoppingCartViewModel GetInstance()
        {
          return Instance.Value;
        }

        // /// <summary>
        // /// 检查指定集合中的书籍库存
        // /// </summary>
        // /// <param name="books">需要检查的集合</param>
        // /// <returns>超过库存的书籍集合</returns>
        // public List<Book> CheckInventory(List<Book> books)
        // {
        //     return _bookDao.CheckInventory(books);
        // }

        /// <summary>
        /// 结算
        /// </summary>
        /// <param name="books">书籍集合</param>
        /// <param name="earnings">收益</param>
        /// <param name="user">用户</param>
        /// <returns>受影响的行数</returns>
        public int Settlement(User user,IEnumerable<Book> books,decimal earnings)
        {
            var bookIdAndCount = books.ToDictionary(book => book.Id, book => book.Count);
            /*
             *  todo 如果有多个管理员，这个程序的逻辑将非常复杂(实际上是我懒得写)，这里我只考虑只有一个管理员，所以直接硬编码写死
             *  实际上要实现的话思需要在book表中添加管理员id字段通过查询购物车中书籍中所有的管理员id并计算各自的收益，
             *  然后通过这个id集合来更新对应的管理员的收益即可
             *  其次需要在程序中添加管理员注册或者添加管理员创建逻辑即可(要不然管理员怎么产生)
             */
            var admin = _adminDao.GetAdminByUsername("admin");
            // 开启事务
            var transaction = _sqlConnection.BeginTransaction();
            try
            {
                _bookDao.UpdateBookCount(transaction,bookIdAndCount);
                if (admin == null)
                {
                    // readme 强迫症犯了，实际上一定能查到
                    // 回滚操作
                    transaction.Rollback();
                    return 0;
                }
                admin.Totalsales+= earnings; 
                _adminDao.Update(transaction, admin);
                var count =  _userDao.Update(transaction,user);
                // 提交事务
                transaction.Commit();
                return count;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                // 回滚操作
                transaction.Rollback();
                return 0;
            }
            
        }

        /// <summary>
        /// 更新用户数据
        /// </summary>
        public void UpdateUserData()
        {
            if (!(_dataContext.Get("loginUser") is User user))
            {
                return;
            }
            _userDao.GetUserByUsername(user.Username);
            _dataContext.Add("loginUser", user);
            
        }
    }
}