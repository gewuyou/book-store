﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace book_store_app.View
{
    partial class Books
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Books));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.windowRestore = new System.Windows.Forms.PictureBox();
            this.windowClose = new System.Windows.Forms.PictureBox();
            this.windowMinimization = new System.Windows.Forms.PictureBox();
            this.windowMaximize = new System.Windows.Forms.PictureBox();
            this.type = new System.Windows.Forms.TextBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.searchBtn = new System.Windows.Forms.Button();
            this.searchValue = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.BooksDataGridView = new System.Windows.Forms.DataGridView();
            this.saveBtn = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.price = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.count = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.author = new System.Windows.Forms.TextBox();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Title = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ExitBtn = new System.Windows.Forms.Panel();
            this.ExitPic = new System.Windows.Forms.PictureBox();
            this.ExitLabel = new System.Windows.Forms.Label();
            this.AccountManagerBtn = new System.Windows.Forms.Panel();
            this.AccountManagerPic = new System.Windows.Forms.PictureBox();
            this.AccountManagerLabel = new System.Windows.Forms.Label();
            this.UsersBtn = new System.Windows.Forms.Panel();
            this.UsesPic = new System.Windows.Forms.PictureBox();
            this.UsersLabel = new System.Windows.Forms.Label();
            this.BooksBtn = new System.Windows.Forms.Panel();
            this.BooksPic = new System.Windows.Forms.PictureBox();
            this.BooksLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.windowRestore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowMinimization)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowMaximize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BooksDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.ExitBtn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ExitPic)).BeginInit();
            this.AccountManagerBtn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AccountManagerPic)).BeginInit();
            this.UsersBtn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UsesPic)).BeginInit();
            this.BooksBtn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BooksPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.windowRestore);
            this.panel2.Controls.Add(this.windowClose);
            this.panel2.Controls.Add(this.windowMinimization);
            this.panel2.Controls.Add(this.windowMaximize);
            this.panel2.Controls.Add(this.type);
            this.panel2.Controls.Add(this.pictureBox7);
            this.panel2.Controls.Add(this.searchBtn);
            this.panel2.Controls.Add(this.searchValue);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.BooksDataGridView);
            this.panel2.Controls.Add(this.saveBtn);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.price);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.count);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.author);
            this.panel2.Controls.Add(this.usernameLabel);
            this.panel2.Controls.Add(this.name);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.Title);
            this.panel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel2.Location = new System.Drawing.Point(214, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1058, 860);
            this.panel2.TabIndex = 3;
            // 
            // windowRestore
            // 
            this.windowRestore.BackColor = System.Drawing.Color.Transparent;
            this.windowRestore.Image = ((System.Drawing.Image)(resources.GetObject("windowRestore.Image")));
            this.windowRestore.Location = new System.Drawing.Point(1006, 3);
            this.windowRestore.Name = "windowRestore";
            this.windowRestore.Size = new System.Drawing.Size(24, 24);
            this.windowRestore.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.windowRestore.TabIndex = 43;
            this.windowRestore.TabStop = false;
            this.windowRestore.Visible = false;
            this.windowRestore.Click += new System.EventHandler(this.MaximizeOrRestore);
            // 
            // windowClose
            // 
            this.windowClose.BackColor = System.Drawing.Color.Transparent;
            this.windowClose.Image = ((System.Drawing.Image)(resources.GetObject("windowClose.Image")));
            this.windowClose.Location = new System.Drawing.Point(1030, 3);
            this.windowClose.Name = "windowClose";
            this.windowClose.Size = new System.Drawing.Size(24, 24);
            this.windowClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.windowClose.TabIndex = 42;
            this.windowClose.TabStop = false;
            this.windowClose.Click += new System.EventHandler(this.Close);
            // 
            // windowMinimization
            // 
            this.windowMinimization.BackColor = System.Drawing.Color.Transparent;
            this.windowMinimization.Image = ((System.Drawing.Image)(resources.GetObject("windowMinimization.Image")));
            this.windowMinimization.Location = new System.Drawing.Point(982, 3);
            this.windowMinimization.Name = "windowMinimization";
            this.windowMinimization.Size = new System.Drawing.Size(24, 24);
            this.windowMinimization.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.windowMinimization.TabIndex = 41;
            this.windowMinimization.TabStop = false;
            this.windowMinimization.Click += new System.EventHandler(this.Minimization);
            // 
            // windowMaximize
            // 
            this.windowMaximize.BackColor = System.Drawing.Color.Transparent;
            this.windowMaximize.Image = ((System.Drawing.Image)(resources.GetObject("windowMaximize.Image")));
            this.windowMaximize.Location = new System.Drawing.Point(1006, 3);
            this.windowMaximize.Name = "windowMaximize";
            this.windowMaximize.Size = new System.Drawing.Size(24, 24);
            this.windowMaximize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.windowMaximize.TabIndex = 40;
            this.windowMaximize.TabStop = false;
            this.windowMaximize.Click += new System.EventHandler(this.MaximizeOrRestore);
            // 
            // type
            // 
            this.type.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.type.Location = new System.Drawing.Point(467, 204);
            this.type.Name = "type";
            this.type.Size = new System.Drawing.Size(170, 34);
            this.type.TabIndex = 39;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(351, 332);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(36, 36);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 38;
            this.pictureBox7.TabStop = false;
            // 
            // searchBtn
            // 
            this.searchBtn.BackColor = System.Drawing.Color.Transparent;
            this.searchBtn.FlatAppearance.BorderColor = System.Drawing.Color.ForestGreen;
            this.searchBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchBtn.Font = new System.Drawing.Font("霞鹜文楷", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.searchBtn.ForeColor = System.Drawing.Color.ForestGreen;
            this.searchBtn.Location = new System.Drawing.Point(655, 332);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(90, 34);
            this.searchBtn.TabIndex = 37;
            this.searchBtn.Text = "搜索";
            this.searchBtn.UseVisualStyleBackColor = false;
            this.searchBtn.Click += new System.EventHandler(this.SearchBtnClick);
            // 
            // searchValue
            // 
            this.searchValue.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.searchValue.Location = new System.Drawing.Point(391, 332);
            this.searchValue.Name = "searchValue";
            this.searchValue.Size = new System.Drawing.Size(259, 34);
            this.searchValue.TabIndex = 36;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.Font = new System.Drawing.Font("霞鹜文楷", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.ForeColor = System.Drawing.Color.ForestGreen;
            this.label10.Location = new System.Drawing.Point(453, 295);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(130, 33);
            this.label10.TabIndex = 35;
            this.label10.Text = "书籍列表";
            // 
            // BooksDataGridView
            // 
            this.BooksDataGridView.AllowUserToAddRows = false;
            this.BooksDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.BooksDataGridView.BackgroundColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Indigo;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.BooksDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.BooksDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.BooksDataGridView.GridColor = System.Drawing.Color.LightGray;
            this.BooksDataGridView.Location = new System.Drawing.Point(4, 389);
            this.BooksDataGridView.Name = "BooksDataGridView";
            this.BooksDataGridView.RowHeadersVisible = false;
            this.BooksDataGridView.RowHeadersWidth = 51;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.MediumSlateBlue;
            this.BooksDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.BooksDataGridView.RowTemplate.Height = 27;
            this.BooksDataGridView.Size = new System.Drawing.Size(1050, 468);
            this.BooksDataGridView.TabIndex = 34;
            // 
            // saveBtn
            // 
            this.saveBtn.BackColor = System.Drawing.Color.Transparent;
            this.saveBtn.FlatAppearance.BorderColor = System.Drawing.Color.ForestGreen;
            this.saveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveBtn.Font = new System.Drawing.Font("霞鹜文楷", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.saveBtn.ForeColor = System.Drawing.Color.ForestGreen;
            this.saveBtn.Location = new System.Drawing.Point(929, 204);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(90, 34);
            this.saveBtn.TabIndex = 33;
            this.saveBtn.Text = "保存";
            this.saveBtn.UseVisualStyleBackColor = false;
            this.saveBtn.Click += new System.EventHandler(this.SaveBtnClick);
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label9.Location = new System.Drawing.Point(805, 179);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 23);
            this.label9.TabIndex = 32;
            this.label9.Text = "售价";
            // 
            // price
            // 
            this.price.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.price.Location = new System.Drawing.Point(805, 204);
            this.price.Name = "price";
            this.price.Size = new System.Drawing.Size(80, 34);
            this.price.TabIndex = 31;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label3.Location = new System.Drawing.Point(681, 179);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 23);
            this.label3.TabIndex = 30;
            this.label3.Text = "数量";
            // 
            // count
            // 
            this.count.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.count.Location = new System.Drawing.Point(681, 204);
            this.count.Name = "count";
            this.count.Size = new System.Drawing.Size(80, 34);
            this.count.TabIndex = 29;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label2.Location = new System.Drawing.Point(458, 178);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 23);
            this.label2.TabIndex = 27;
            this.label2.Text = "书籍类型";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label1.Location = new System.Drawing.Point(248, 179);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 23);
            this.label1.TabIndex = 26;
            this.label1.Text = "作者";
            // 
            // author
            // 
            this.author.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.author.Location = new System.Drawing.Point(253, 204);
            this.author.Name = "author";
            this.author.Size = new System.Drawing.Size(170, 34);
            this.author.TabIndex = 25;
            // 
            // usernameLabel
            // 
            this.usernameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.usernameLabel.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.usernameLabel.ForeColor = System.Drawing.SystemColors.GrayText;
            this.usernameLabel.Location = new System.Drawing.Point(33, 178);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(101, 23);
            this.usernameLabel.TabIndex = 24;
            this.usernameLabel.Text = "书籍名称";
            // 
            // name
            // 
            this.name.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.name.Location = new System.Drawing.Point(39, 204);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(170, 34);
            this.name.TabIndex = 23;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(483, 96);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(70, 44);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // Title
            // 
            this.Title.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Title.Font = new System.Drawing.Font("霞鹜文楷", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Title.ForeColor = System.Drawing.Color.ForestGreen;
            this.Title.Location = new System.Drawing.Point(481, 48);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(82, 33);
            this.Title.TabIndex = 21;
            this.Title.Text = "书籍";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Menu;
            this.panel1.Controls.Add(this.ExitBtn);
            this.panel1.Controls.Add(this.AccountManagerBtn);
            this.panel1.Controls.Add(this.UsersBtn);
            this.panel1.Controls.Add(this.BooksBtn);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(202, 860);
            this.panel1.TabIndex = 2;
            // 
            // ExitBtn
            // 
            this.ExitBtn.BackColor = System.Drawing.Color.Transparent;
            this.ExitBtn.Controls.Add(this.ExitPic);
            this.ExitBtn.Controls.Add(this.ExitLabel);
            this.ExitBtn.Location = new System.Drawing.Point(18, 352);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Size = new System.Drawing.Size(166, 49);
            this.ExitBtn.TabIndex = 15;
            // 
            // ExitPic
            // 
            this.ExitPic.Image = ((System.Drawing.Image)(resources.GetObject("ExitPic.Image")));
            this.ExitPic.Location = new System.Drawing.Point(3, 3);
            this.ExitPic.Name = "ExitPic";
            this.ExitPic.Size = new System.Drawing.Size(43, 43);
            this.ExitPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ExitPic.TabIndex = 11;
            this.ExitPic.TabStop = false;
            this.ExitPic.Click += new System.EventHandler(this.ExitPic_Click);
            // 
            // ExitLabel
            // 
            this.ExitLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ExitLabel.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ExitLabel.ForeColor = System.Drawing.SystemColors.MenuText;
            this.ExitLabel.Location = new System.Drawing.Point(53, 9);
            this.ExitLabel.Name = "ExitLabel";
            this.ExitLabel.Size = new System.Drawing.Size(110, 33);
            this.ExitLabel.TabIndex = 12;
            this.ExitLabel.Text = "退出";
            this.ExitLabel.Click += new System.EventHandler(this.ExitLabel_Click);
            // 
            // AccountManagerBtn
            // 
            this.AccountManagerBtn.BackColor = System.Drawing.Color.Transparent;
            this.AccountManagerBtn.Controls.Add(this.AccountManagerPic);
            this.AccountManagerBtn.Controls.Add(this.AccountManagerLabel);
            this.AccountManagerBtn.Location = new System.Drawing.Point(18, 264);
            this.AccountManagerBtn.Name = "AccountManagerBtn";
            this.AccountManagerBtn.Size = new System.Drawing.Size(166, 49);
            this.AccountManagerBtn.TabIndex = 15;
            // 
            // AccountManagerPic
            // 
            this.AccountManagerPic.Image = ((System.Drawing.Image)(resources.GetObject("AccountManagerPic.Image")));
            this.AccountManagerPic.Location = new System.Drawing.Point(3, 3);
            this.AccountManagerPic.Name = "AccountManagerPic";
            this.AccountManagerPic.Size = new System.Drawing.Size(43, 43);
            this.AccountManagerPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AccountManagerPic.TabIndex = 11;
            this.AccountManagerPic.TabStop = false;
            this.AccountManagerPic.Click += new System.EventHandler(this.AccountManagerPic_Click);
            // 
            // AccountManagerLabel
            // 
            this.AccountManagerLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AccountManagerLabel.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.AccountManagerLabel.ForeColor = System.Drawing.SystemColors.MenuText;
            this.AccountManagerLabel.Location = new System.Drawing.Point(52, 9);
            this.AccountManagerLabel.Name = "AccountManagerLabel";
            this.AccountManagerLabel.Size = new System.Drawing.Size(113, 33);
            this.AccountManagerLabel.TabIndex = 12;
            this.AccountManagerLabel.Text = "账户管理";
            this.AccountManagerLabel.Click += new System.EventHandler(this.AccountManagerLabel_Click);
            // 
            // UsersBtn
            // 
            this.UsersBtn.BackColor = System.Drawing.Color.Transparent;
            this.UsersBtn.Controls.Add(this.UsesPic);
            this.UsersBtn.Controls.Add(this.UsersLabel);
            this.UsersBtn.Location = new System.Drawing.Point(18, 178);
            this.UsersBtn.Name = "UsersBtn";
            this.UsersBtn.Size = new System.Drawing.Size(166, 49);
            this.UsersBtn.TabIndex = 14;
            // 
            // UsesPic
            // 
            this.UsesPic.Image = ((System.Drawing.Image)(resources.GetObject("UsesPic.Image")));
            this.UsesPic.Location = new System.Drawing.Point(3, 3);
            this.UsesPic.Name = "UsesPic";
            this.UsesPic.Size = new System.Drawing.Size(43, 43);
            this.UsesPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.UsesPic.TabIndex = 11;
            this.UsesPic.TabStop = false;
            this.UsesPic.Click += new System.EventHandler(this.UsesPic_Click);
            // 
            // UsersLabel
            // 
            this.UsersLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.UsersLabel.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.UsersLabel.ForeColor = System.Drawing.SystemColors.MenuText;
            this.UsersLabel.Location = new System.Drawing.Point(53, 9);
            this.UsersLabel.Name = "UsersLabel";
            this.UsersLabel.Size = new System.Drawing.Size(111, 33);
            this.UsersLabel.TabIndex = 12;
            this.UsersLabel.Text = "用户管理";
            this.UsersLabel.Click += new System.EventHandler(this.UsersLabel_Click);
            // 
            // BooksBtn
            // 
            this.BooksBtn.BackColor = System.Drawing.Color.Ivory;
            this.BooksBtn.Controls.Add(this.BooksPic);
            this.BooksBtn.Controls.Add(this.BooksLabel);
            this.BooksBtn.Location = new System.Drawing.Point(18, 94);
            this.BooksBtn.Name = "BooksBtn";
            this.BooksBtn.Size = new System.Drawing.Size(166, 49);
            this.BooksBtn.TabIndex = 13;
            // 
            // BooksPic
            // 
            this.BooksPic.Image = ((System.Drawing.Image)(resources.GetObject("BooksPic.Image")));
            this.BooksPic.Location = new System.Drawing.Point(3, 3);
            this.BooksPic.Name = "BooksPic";
            this.BooksPic.Size = new System.Drawing.Size(43, 43);
            this.BooksPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BooksPic.TabIndex = 11;
            this.BooksPic.TabStop = false;
            this.BooksPic.Click += new System.EventHandler(this.BooksPic_Click);
            // 
            // BooksLabel
            // 
            this.BooksLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BooksLabel.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BooksLabel.ForeColor = System.Drawing.SystemColors.MenuText;
            this.BooksLabel.Location = new System.Drawing.Point(52, 9);
            this.BooksLabel.Name = "BooksLabel";
            this.BooksLabel.Size = new System.Drawing.Size(111, 33);
            this.BooksLabel.TabIndex = 12;
            this.BooksLabel.Text = "书籍";
            this.BooksLabel.Click += new System.EventHandler(this.BooksLabel_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.Font = new System.Drawing.Font("霞鹜文楷", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label4.Location = new System.Drawing.Point(62, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(132, 40);
            this.label4.TabIndex = 10;
            this.label4.Text = "书籍";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(13, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(45, 47);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 10;
            this.pictureBox2.TabStop = false;
            // 
            // Books
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Indigo;
            this.ClientSize = new System.Drawing.Size(1285, 884);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Books";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Books";
            this.Load += new System.EventHandler(this.Books_Load);
            this.SizeChanged+=new System.EventHandler(Books_SizeChanged);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.windowRestore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowMinimization)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowMaximize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BooksDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ExitBtn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ExitPic)).EndInit();
            this.AccountManagerBtn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AccountManagerPic)).EndInit();
            this.UsersBtn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UsesPic)).EndInit();
            this.BooksBtn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BooksPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
        }

        private System.Windows.Forms.TextBox type;

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel BooksBtn;
        private System.Windows.Forms.PictureBox BooksPic;
        private System.Windows.Forms.Label BooksLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel UsersBtn;
        private System.Windows.Forms.PictureBox UsesPic;
        private System.Windows.Forms.Label UsersLabel;
        private System.Windows.Forms.Panel AccountManagerBtn;
        private System.Windows.Forms.PictureBox AccountManagerPic;
        private System.Windows.Forms.Label AccountManagerLabel;
        private System.Windows.Forms.Panel ExitBtn;
        private System.Windows.Forms.PictureBox ExitPic;
        private System.Windows.Forms.Label ExitLabel;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox price;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox count;
        private Label label2;
        private Label label1;
        private TextBox author;
        private Label usernameLabel;
        private TextBox name;
        private PictureBox pictureBox1;
        private Label Title;
        private Label label10;
        private System.Windows.Forms.DataGridView BooksDataGridView;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.TextBox searchValue;
        private PictureBox windowRestore;
        private System.Windows.Forms.PictureBox windowClose;
        private System.Windows.Forms.PictureBox windowMinimization;
        private PictureBox windowMaximize;
    }
}