﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace book_store_app.View
{
    partial class Orders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Orders));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.AddToCartBtn = new System.Windows.Forms.Button();
            this.price = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.count = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.author = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.TextBox();
            this.windowRestore = new System.Windows.Forms.PictureBox();
            this.windowClose = new System.Windows.Forms.PictureBox();
            this.windowMinimization = new System.Windows.Forms.PictureBox();
            this.windowMaximize = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.searchBtn = new System.Windows.Forms.Button();
            this.searchValue = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.BooksDataGridView = new System.Windows.Forms.DataGridView();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Title = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.OrdersPic = new System.Windows.Forms.PictureBox();
            this.OrdersLabel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ShoppingCartPic = new System.Windows.Forms.PictureBox();
            this.ShoppingCartLabel = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.PersonalCenterPic = new System.Windows.Forms.PictureBox();
            this.PersonalCenterLabel = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.ExitPic = new System.Windows.Forms.PictureBox();
            this.ExitLabel = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.windowRestore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowMinimization)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowMaximize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BooksDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OrdersPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ShoppingCartPic)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PersonalCenterPic)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ExitPic)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.AddToCartBtn);
            this.panel2.Controls.Add(this.price);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.count);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.author);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.name);
            this.panel2.Controls.Add(this.windowRestore);
            this.panel2.Controls.Add(this.windowClose);
            this.panel2.Controls.Add(this.windowMinimization);
            this.panel2.Controls.Add(this.windowMaximize);
            this.panel2.Controls.Add(this.pictureBox7);
            this.panel2.Controls.Add(this.searchBtn);
            this.panel2.Controls.Add(this.searchValue);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.BooksDataGridView);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.Title);
            this.panel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel2.Location = new System.Drawing.Point(214, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1058, 860);
            this.panel2.TabIndex = 5;
            // 
            // AddToCartBtn
            // 
            this.AddToCartBtn.BackColor = System.Drawing.Color.Transparent;
            this.AddToCartBtn.FlatAppearance.BorderColor = System.Drawing.Color.ForestGreen;
            this.AddToCartBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddToCartBtn.Font = new System.Drawing.Font("霞鹜文楷", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.AddToCartBtn.ForeColor = System.Drawing.Color.ForestGreen;
            this.AddToCartBtn.Location = new System.Drawing.Point(896, 282);
            this.AddToCartBtn.Name = "AddToCartBtn";
            this.AddToCartBtn.Size = new System.Drawing.Size(125, 34);
            this.AddToCartBtn.TabIndex = 54;
            this.AddToCartBtn.Text = "加入购物车";
            this.AddToCartBtn.UseVisualStyleBackColor = false;
            this.AddToCartBtn.Click += new System.EventHandler(this.AddToCartBtnClick);
            // 
            // price
            // 
            this.price.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.price.Location = new System.Drawing.Point(468, 282);
            this.price.Name = "price";
            this.price.ReadOnly = true;
            this.price.Size = new System.Drawing.Size(170, 34);
            this.price.TabIndex = 53;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label3.Location = new System.Drawing.Point(682, 257);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 23);
            this.label3.TabIndex = 52;
            this.label3.Text = "数量";
            // 
            // count
            // 
            this.count.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.count.Location = new System.Drawing.Point(682, 282);
            this.count.Name = "count";
            this.count.Size = new System.Drawing.Size(170, 34);
            this.count.TabIndex = 51;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label2.Location = new System.Drawing.Point(459, 256);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 23);
            this.label2.TabIndex = 50;
            this.label2.Text = "价格";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label4.Location = new System.Drawing.Point(249, 257);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 23);
            this.label4.TabIndex = 49;
            this.label4.Text = "作者";
            // 
            // author
            // 
            this.author.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.author.Location = new System.Drawing.Point(254, 282);
            this.author.Name = "author";
            this.author.ReadOnly = true;
            this.author.Size = new System.Drawing.Size(170, 34);
            this.author.TabIndex = 48;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label9.Location = new System.Drawing.Point(34, 256);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 23);
            this.label9.TabIndex = 47;
            this.label9.Text = "书籍名称";
            // 
            // name
            // 
            this.name.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.name.Location = new System.Drawing.Point(40, 282);
            this.name.Name = "name";
            this.name.ReadOnly = true;
            this.name.Size = new System.Drawing.Size(170, 34);
            this.name.TabIndex = 46;
            // 
            // windowRestore
            // 
            this.windowRestore.BackColor = System.Drawing.Color.Transparent;
            this.windowRestore.Image = ((System.Drawing.Image)(resources.GetObject("windowRestore.Image")));
            this.windowRestore.Location = new System.Drawing.Point(1006, 3);
            this.windowRestore.Name = "windowRestore";
            this.windowRestore.Size = new System.Drawing.Size(24, 24);
            this.windowRestore.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.windowRestore.TabIndex = 43;
            this.windowRestore.TabStop = false;
            this.windowRestore.Visible = false;
            this.windowRestore.Click += new System.EventHandler(this.MaximizeOrRestore);
            // 
            // windowClose
            // 
            this.windowClose.BackColor = System.Drawing.Color.Transparent;
            this.windowClose.Image = ((System.Drawing.Image)(resources.GetObject("windowClose.Image")));
            this.windowClose.Location = new System.Drawing.Point(1030, 3);
            this.windowClose.Name = "windowClose";
            this.windowClose.Size = new System.Drawing.Size(24, 24);
            this.windowClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.windowClose.TabIndex = 42;
            this.windowClose.TabStop = false;
            this.windowClose.Click += new System.EventHandler(this.Close);
            // 
            // windowMinimization
            // 
            this.windowMinimization.BackColor = System.Drawing.Color.Transparent;
            this.windowMinimization.Image = ((System.Drawing.Image)(resources.GetObject("windowMinimization.Image")));
            this.windowMinimization.Location = new System.Drawing.Point(982, 3);
            this.windowMinimization.Name = "windowMinimization";
            this.windowMinimization.Size = new System.Drawing.Size(24, 24);
            this.windowMinimization.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.windowMinimization.TabIndex = 41;
            this.windowMinimization.TabStop = false;
            this.windowMinimization.Click += new System.EventHandler(this.Minimization);
            // 
            // windowMaximize
            // 
            this.windowMaximize.BackColor = System.Drawing.Color.Transparent;
            this.windowMaximize.Image = ((System.Drawing.Image)(resources.GetObject("windowMaximize.Image")));
            this.windowMaximize.Location = new System.Drawing.Point(1006, 3);
            this.windowMaximize.Name = "windowMaximize";
            this.windowMaximize.Size = new System.Drawing.Size(24, 24);
            this.windowMaximize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.windowMaximize.TabIndex = 40;
            this.windowMaximize.TabStop = false;
            this.windowMaximize.Click += new System.EventHandler(this.MaximizeOrRestore);
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(353, 204);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(36, 36);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 38;
            this.pictureBox7.TabStop = false;
            // 
            // searchBtn
            // 
            this.searchBtn.BackColor = System.Drawing.Color.Transparent;
            this.searchBtn.FlatAppearance.BorderColor = System.Drawing.Color.ForestGreen;
            this.searchBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchBtn.Font = new System.Drawing.Font("霞鹜文楷", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.searchBtn.ForeColor = System.Drawing.Color.ForestGreen;
            this.searchBtn.Location = new System.Drawing.Point(657, 204);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(90, 34);
            this.searchBtn.TabIndex = 37;
            this.searchBtn.Text = "搜索";
            this.searchBtn.UseVisualStyleBackColor = false;
            this.searchBtn.Click += new System.EventHandler(this.SearchBtnClick);
            // 
            // searchValue
            // 
            this.searchValue.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.searchValue.Location = new System.Drawing.Point(393, 204);
            this.searchValue.Name = "searchValue";
            this.searchValue.Size = new System.Drawing.Size(259, 34);
            this.searchValue.TabIndex = 36;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.Font = new System.Drawing.Font("霞鹜文楷", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.ForeColor = System.Drawing.Color.ForestGreen;
            this.label10.Location = new System.Drawing.Point(455, 167);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(130, 33);
            this.label10.TabIndex = 35;
            this.label10.Text = "书籍列表";
            // 
            // BooksDataGridView
            // 
            this.BooksDataGridView.AllowUserToAddRows = false;
            this.BooksDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.BooksDataGridView.BackgroundColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Indigo;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.BooksDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.BooksDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.BooksDataGridView.GridColor = System.Drawing.Color.LightGray;
            this.BooksDataGridView.Location = new System.Drawing.Point(5, 329);
            this.BooksDataGridView.Name = "BooksDataGridView";
            this.BooksDataGridView.RowHeadersVisible = false;
            this.BooksDataGridView.RowHeadersWidth = 51;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.MediumSlateBlue;
            this.BooksDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.BooksDataGridView.RowTemplate.Height = 27;
            this.BooksDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.BooksDataGridView.Size = new System.Drawing.Size(1050, 528);
            this.BooksDataGridView.TabIndex = 34;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(484, 96);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(70, 44);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // Title
            // 
            this.Title.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Title.Font = new System.Drawing.Font("霞鹜文楷", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Title.ForeColor = System.Drawing.Color.ForestGreen;
            this.Title.Location = new System.Drawing.Point(455, 48);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(160, 33);
            this.Title.TabIndex = 21;
            this.Title.Text = "书籍商城";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Menu;
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.pictureBox5);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(202, 860);
            this.panel1.TabIndex = 4;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Ivory;
            this.panel5.Controls.Add(this.OrdersPic);
            this.panel5.Controls.Add(this.OrdersLabel);
            this.panel5.Location = new System.Drawing.Point(18, 256);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(166, 49);
            this.panel5.TabIndex = 16;
            // 
            // OrdersPic
            // 
            this.OrdersPic.Image = ((System.Drawing.Image)(resources.GetObject("OrdersPic.Image")));
            this.OrdersPic.Location = new System.Drawing.Point(2, 3);
            this.OrdersPic.Name = "OrdersPic";
            this.OrdersPic.Size = new System.Drawing.Size(43, 43);
            this.OrdersPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.OrdersPic.TabIndex = 11;
            this.OrdersPic.TabStop = false;
            this.OrdersPic.Click += new System.EventHandler(this.OrdersPic_Click);
            // 
            // OrdersLabel
            // 
            this.OrdersLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.OrdersLabel.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.OrdersLabel.ForeColor = System.Drawing.SystemColors.MenuText;
            this.OrdersLabel.Location = new System.Drawing.Point(48, 10);
            this.OrdersLabel.Name = "OrdersLabel";
            this.OrdersLabel.Size = new System.Drawing.Size(115, 33);
            this.OrdersLabel.TabIndex = 12;
            this.OrdersLabel.Text = "书籍商城";
            this.OrdersLabel.Click += new System.EventHandler(this.OrdersLabel_Click);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.Font = new System.Drawing.Font("霞鹜文楷", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label7.Location = new System.Drawing.Point(67, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(132, 40);
            this.label7.TabIndex = 18;
            this.label7.Text = "书籍商城";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(18, 13);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(45, 47);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 19;
            this.pictureBox5.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.ShoppingCartPic);
            this.panel3.Controls.Add(this.ShoppingCartLabel);
            this.panel3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel3.Location = new System.Drawing.Point(18, 172);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(166, 49);
            this.panel3.TabIndex = 17;
            // 
            // ShoppingCartPic
            // 
            this.ShoppingCartPic.Image = ((System.Drawing.Image)(resources.GetObject("ShoppingCartPic.Image")));
            this.ShoppingCartPic.Location = new System.Drawing.Point(2, 3);
            this.ShoppingCartPic.Name = "ShoppingCartPic";
            this.ShoppingCartPic.Size = new System.Drawing.Size(43, 43);
            this.ShoppingCartPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ShoppingCartPic.TabIndex = 11;
            this.ShoppingCartPic.TabStop = false;
            this.ShoppingCartPic.Click += new System.EventHandler(this.ShoppingCartPic_Click);
            // 
            // ShoppingCartLabel
            // 
            this.ShoppingCartLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ShoppingCartLabel.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ShoppingCartLabel.ForeColor = System.Drawing.SystemColors.MenuText;
            this.ShoppingCartLabel.Location = new System.Drawing.Point(51, 8);
            this.ShoppingCartLabel.Name = "ShoppingCartLabel";
            this.ShoppingCartLabel.Size = new System.Drawing.Size(110, 33);
            this.ShoppingCartLabel.TabIndex = 12;
            this.ShoppingCartLabel.Text = "购物车";
            this.ShoppingCartLabel.Click += new System.EventHandler(this.ShoppingCartLabel_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.Controls.Add(this.PersonalCenterPic);
            this.panel4.Controls.Add(this.PersonalCenterLabel);
            this.panel4.Location = new System.Drawing.Point(18, 88);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(166, 49);
            this.panel4.TabIndex = 16;
            // 
            // PersonalCenterPic
            // 
            this.PersonalCenterPic.Image = ((System.Drawing.Image)(resources.GetObject("PersonalCenterPic.Image")));
            this.PersonalCenterPic.Location = new System.Drawing.Point(2, 3);
            this.PersonalCenterPic.Name = "PersonalCenterPic";
            this.PersonalCenterPic.Size = new System.Drawing.Size(43, 43);
            this.PersonalCenterPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PersonalCenterPic.TabIndex = 11;
            this.PersonalCenterPic.TabStop = false;
            this.PersonalCenterPic.Click += new System.EventHandler(this.PersonalCenterPic_Click);
            // 
            // PersonalCenterLabel
            // 
            this.PersonalCenterLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PersonalCenterLabel.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.PersonalCenterLabel.ForeColor = System.Drawing.SystemColors.MenuText;
            this.PersonalCenterLabel.Location = new System.Drawing.Point(48, 8);
            this.PersonalCenterLabel.Name = "PersonalCenterLabel";
            this.PersonalCenterLabel.Size = new System.Drawing.Size(113, 33);
            this.PersonalCenterLabel.TabIndex = 12;
            this.PersonalCenterLabel.Text = "个人中心";
            this.PersonalCenterLabel.Click += new System.EventHandler(this.PersonalCenterLabel_Click);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Transparent;
            this.panel6.Controls.Add(this.ExitPic);
            this.panel6.Controls.Add(this.ExitLabel);
            this.panel6.Location = new System.Drawing.Point(18, 340);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(166, 49);
            this.panel6.TabIndex = 15;
            // 
            // ExitPic
            // 
            this.ExitPic.Image = ((System.Drawing.Image)(resources.GetObject("ExitPic.Image")));
            this.ExitPic.Location = new System.Drawing.Point(2, 3);
            this.ExitPic.Name = "ExitPic";
            this.ExitPic.Size = new System.Drawing.Size(43, 43);
            this.ExitPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ExitPic.TabIndex = 11;
            this.ExitPic.TabStop = false;
            this.ExitPic.Click += new System.EventHandler(this.ExitPic_Click);
            // 
            // ExitLabel
            // 
            this.ExitLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ExitLabel.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ExitLabel.ForeColor = System.Drawing.SystemColors.MenuText;
            this.ExitLabel.Location = new System.Drawing.Point(51, 9);
            this.ExitLabel.Name = "ExitLabel";
            this.ExitLabel.Size = new System.Drawing.Size(112, 33);
            this.ExitLabel.TabIndex = 12;
            this.ExitLabel.Text = "退出";
            this.ExitLabel.Click += new System.EventHandler(this.ExitLabel_Click);
            // 
            // Orders
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Indigo;
            this.ClientSize = new System.Drawing.Size(1285, 884);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(15, 15);
            this.Name = "Orders";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Orders_Load);
            this.SizeChanged += new System.EventHandler(this.Orders_SizeChanged);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.windowRestore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowMinimization)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowMaximize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BooksDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OrdersPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ShoppingCartPic)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PersonalCenterPic)).EndInit();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ExitPic)).EndInit();
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox windowRestore;
        private System.Windows.Forms.PictureBox windowClose;
        private System.Windows.Forms.PictureBox windowMinimization;
        private System.Windows.Forms.PictureBox windowMaximize;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.TextBox searchValue;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView BooksDataGridView;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label Title;
        private Panel panel1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.PictureBox ExitPic;
        private System.Windows.Forms.Label ExitLabel;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox PersonalCenterPic;
        private System.Windows.Forms.Label PersonalCenterLabel;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox ShoppingCartPic;
        private System.Windows.Forms.Label ShoppingCartLabel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.PictureBox OrdersPic;
        private System.Windows.Forms.Label OrdersLabel;
        private System.Windows.Forms.Button AddToCartBtn;
        private TextBox price;
        private Label label3;
        private TextBox count;
        private System.Windows.Forms.Label label2;
        private Label label4;
        private TextBox author;
        private Label label9;
        private TextBox name;
    }
}