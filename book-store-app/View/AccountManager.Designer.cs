﻿namespace book_store_app.View
{
    partial class AccountManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AccountManager));
            this.ExitPic = new System.Windows.Forms.PictureBox();
            this.ExitLabel = new System.Windows.Forms.Label();
            this.ExitBtn = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.AccountManagerBtn = new System.Windows.Forms.Panel();
            this.AccountManagerPic = new System.Windows.Forms.PictureBox();
            this.AccountManagerLabel = new System.Windows.Forms.Label();
            this.UsersBtn = new System.Windows.Forms.Panel();
            this.UsersPic = new System.Windows.Forms.PictureBox();
            this.UsersLabel = new System.Windows.Forms.Label();
            this.BooksBtn = new System.Windows.Forms.Panel();
            this.BooksPic = new System.Windows.Forms.PictureBox();
            this.BooksLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Title = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.windowRestore = new System.Windows.Forms.PictureBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.number = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.windowClose = new System.Windows.Forms.PictureBox();
            this.windowMinimization = new System.Windows.Forms.PictureBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.salesAmount = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.windowMaximize = new System.Windows.Forms.PictureBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.bookCount = new System.Windows.Forms.Label();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ExitPic)).BeginInit();
            this.ExitBtn.SuspendLayout();
            this.panel1.SuspendLayout();
            this.AccountManagerBtn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AccountManagerPic)).BeginInit();
            this.UsersBtn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UsersPic)).BeginInit();
            this.BooksBtn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BooksPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.windowRestore)).BeginInit();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowMinimization)).BeginInit();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowMaximize)).BeginInit();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.SuspendLayout();
            // 
            // ExitPic
            // 
            this.ExitPic.Image = ((System.Drawing.Image)(resources.GetObject("ExitPic.Image")));
            this.ExitPic.Location = new System.Drawing.Point(3, 3);
            this.ExitPic.Name = "ExitPic";
            this.ExitPic.Size = new System.Drawing.Size(43, 43);
            this.ExitPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ExitPic.TabIndex = 11;
            this.ExitPic.TabStop = false;
            this.ExitPic.Click += new System.EventHandler(this.ExitPic_Click);
            // 
            // ExitLabel
            // 
            this.ExitLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ExitLabel.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ExitLabel.ForeColor = System.Drawing.SystemColors.MenuText;
            this.ExitLabel.Location = new System.Drawing.Point(53, 9);
            this.ExitLabel.Name = "ExitLabel";
            this.ExitLabel.Size = new System.Drawing.Size(110, 33);
            this.ExitLabel.TabIndex = 12;
            this.ExitLabel.Text = "退出";
            this.ExitLabel.Click += new System.EventHandler(this.ExitLabel_Click);
            // 
            // ExitBtn
            // 
            this.ExitBtn.BackColor = System.Drawing.Color.Transparent;
            this.ExitBtn.Controls.Add(this.ExitPic);
            this.ExitBtn.Controls.Add(this.ExitLabel);
            this.ExitBtn.Location = new System.Drawing.Point(18, 352);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Size = new System.Drawing.Size(166, 49);
            this.ExitBtn.TabIndex = 15;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Menu;
            this.panel1.Controls.Add(this.ExitBtn);
            this.panel1.Controls.Add(this.AccountManagerBtn);
            this.panel1.Controls.Add(this.UsersBtn);
            this.panel1.Controls.Add(this.BooksBtn);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(202, 860);
            this.panel1.TabIndex = 6;
            // 
            // AccountManagerBtn
            // 
            this.AccountManagerBtn.BackColor = System.Drawing.Color.Ivory;
            this.AccountManagerBtn.Controls.Add(this.AccountManagerPic);
            this.AccountManagerBtn.Controls.Add(this.AccountManagerLabel);
            this.AccountManagerBtn.Location = new System.Drawing.Point(18, 264);
            this.AccountManagerBtn.Name = "AccountManagerBtn";
            this.AccountManagerBtn.Size = new System.Drawing.Size(166, 49);
            this.AccountManagerBtn.TabIndex = 15;
            // 
            // AccountManagerPic
            // 
            this.AccountManagerPic.Image = ((System.Drawing.Image)(resources.GetObject("AccountManagerPic.Image")));
            this.AccountManagerPic.Location = new System.Drawing.Point(3, 3);
            this.AccountManagerPic.Name = "AccountManagerPic";
            this.AccountManagerPic.Size = new System.Drawing.Size(43, 43);
            this.AccountManagerPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AccountManagerPic.TabIndex = 11;
            this.AccountManagerPic.TabStop = false;
            this.AccountManagerPic.Click += new System.EventHandler(this.AccountManagerPic_Click);
            // 
            // AccountManagerLabel
            // 
            this.AccountManagerLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AccountManagerLabel.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.AccountManagerLabel.ForeColor = System.Drawing.SystemColors.MenuText;
            this.AccountManagerLabel.Location = new System.Drawing.Point(52, 9);
            this.AccountManagerLabel.Name = "AccountManagerLabel";
            this.AccountManagerLabel.Size = new System.Drawing.Size(113, 33);
            this.AccountManagerLabel.TabIndex = 12;
            this.AccountManagerLabel.Text = "账户管理";
            this.AccountManagerLabel.Click += new System.EventHandler(this.AccountManagerLabel_Click);
            // 
            // UsersBtn
            // 
            this.UsersBtn.BackColor = System.Drawing.Color.Transparent;
            this.UsersBtn.Controls.Add(this.UsersPic);
            this.UsersBtn.Controls.Add(this.UsersLabel);
            this.UsersBtn.Location = new System.Drawing.Point(18, 178);
            this.UsersBtn.Name = "UsersBtn";
            this.UsersBtn.Size = new System.Drawing.Size(166, 49);
            this.UsersBtn.TabIndex = 14;
            // 
            // UsersPic
            // 
            this.UsersPic.Image = ((System.Drawing.Image)(resources.GetObject("UsersPic.Image")));
            this.UsersPic.Location = new System.Drawing.Point(3, 3);
            this.UsersPic.Name = "UsersPic";
            this.UsersPic.Size = new System.Drawing.Size(43, 43);
            this.UsersPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.UsersPic.TabIndex = 11;
            this.UsersPic.TabStop = false;
            this.UsersPic.Click += new System.EventHandler(this.UsersPic_Click);
            // 
            // UsersLabel
            // 
            this.UsersLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.UsersLabel.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.UsersLabel.ForeColor = System.Drawing.SystemColors.MenuText;
            this.UsersLabel.Location = new System.Drawing.Point(53, 9);
            this.UsersLabel.Name = "UsersLabel";
            this.UsersLabel.Size = new System.Drawing.Size(111, 33);
            this.UsersLabel.TabIndex = 12;
            this.UsersLabel.Text = "用户管理";
            this.UsersLabel.Click += new System.EventHandler(this.UsersLabel_Click);
            // 
            // BooksBtn
            // 
            this.BooksBtn.BackColor = System.Drawing.Color.Transparent;
            this.BooksBtn.Controls.Add(this.BooksPic);
            this.BooksBtn.Controls.Add(this.BooksLabel);
            this.BooksBtn.Location = new System.Drawing.Point(18, 94);
            this.BooksBtn.Name = "BooksBtn";
            this.BooksBtn.Size = new System.Drawing.Size(166, 49);
            this.BooksBtn.TabIndex = 13;
            // 
            // BooksPic
            // 
            this.BooksPic.Image = ((System.Drawing.Image)(resources.GetObject("BooksPic.Image")));
            this.BooksPic.Location = new System.Drawing.Point(3, 3);
            this.BooksPic.Name = "BooksPic";
            this.BooksPic.Size = new System.Drawing.Size(43, 43);
            this.BooksPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BooksPic.TabIndex = 11;
            this.BooksPic.TabStop = false;
            this.BooksPic.Click += new System.EventHandler(this.BooksPic_Click);
            // 
            // BooksLabel
            // 
            this.BooksLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BooksLabel.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BooksLabel.ForeColor = System.Drawing.SystemColors.MenuText;
            this.BooksLabel.Location = new System.Drawing.Point(53, 9);
            this.BooksLabel.Name = "BooksLabel";
            this.BooksLabel.Size = new System.Drawing.Size(110, 33);
            this.BooksLabel.TabIndex = 12;
            this.BooksLabel.Text = "书籍";
            this.BooksLabel.Click += new System.EventHandler(this.BooksLabel_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.Font = new System.Drawing.Font("霞鹜文楷", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label4.Location = new System.Drawing.Point(62, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(132, 40);
            this.label4.TabIndex = 10;
            this.label4.Text = "账户管理";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(13, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(45, 47);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 10;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(489, 96);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(70, 44);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // Title
            // 
            this.Title.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Title.Font = new System.Drawing.Font("霞鹜文楷", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Title.ForeColor = System.Drawing.Color.ForestGreen;
            this.Title.Location = new System.Drawing.Point(460, 48);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(160, 33);
            this.Title.TabIndex = 21;
            this.Title.Text = "账户管理";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.windowRestore);
            this.panel2.Controls.Add(this.panel9);
            this.panel2.Controls.Add(this.windowClose);
            this.panel2.Controls.Add(this.windowMinimization);
            this.panel2.Controls.Add(this.panel8);
            this.panel2.Controls.Add(this.windowMaximize);
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.Title);
            this.panel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel2.Location = new System.Drawing.Point(214, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1058, 860);
            this.panel2.TabIndex = 7;
            // 
            // windowRestore
            // 
            this.windowRestore.BackColor = System.Drawing.Color.Transparent;
            this.windowRestore.Image = ((System.Drawing.Image)(resources.GetObject("windowRestore.Image")));
            this.windowRestore.Location = new System.Drawing.Point(1007, 3);
            this.windowRestore.Name = "windowRestore";
            this.windowRestore.Size = new System.Drawing.Size(24, 24);
            this.windowRestore.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.windowRestore.TabIndex = 47;
            this.windowRestore.TabStop = false;
            this.windowRestore.Visible = false;
            this.windowRestore.Click += new System.EventHandler(this.MaximizeOrRestore);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.panel9.Controls.Add(this.number);
            this.panel9.Controls.Add(this.pictureBox9);
            this.panel9.Controls.Add(this.label11);
            this.panel9.Location = new System.Drawing.Point(393, 200);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(275, 154);
            this.panel9.TabIndex = 25;
            // 
            // number
            // 
            this.number.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.number.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.number.ForeColor = System.Drawing.SystemColors.MenuText;
            this.number.Location = new System.Drawing.Point(22, 88);
            this.number.Name = "number";
            this.number.Size = new System.Drawing.Size(112, 32);
            this.number.TabIndex = 13;
            this.number.Text = "人数";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(139, 14);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(125, 125);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox9.TabIndex = 11;
            this.pictureBox9.TabStop = false;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label11.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label11.Location = new System.Drawing.Point(16, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(118, 32);
            this.label11.TabIndex = 12;
            this.label11.Text = "总用户数";
            // 
            // windowClose
            // 
            this.windowClose.BackColor = System.Drawing.Color.Transparent;
            this.windowClose.Image = ((System.Drawing.Image)(resources.GetObject("windowClose.Image")));
            this.windowClose.Location = new System.Drawing.Point(1031, 3);
            this.windowClose.Name = "windowClose";
            this.windowClose.Size = new System.Drawing.Size(24, 24);
            this.windowClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.windowClose.TabIndex = 46;
            this.windowClose.TabStop = false;
            this.windowClose.Click += new System.EventHandler(this.Close);
            // 
            // windowMinimization
            // 
            this.windowMinimization.BackColor = System.Drawing.Color.Transparent;
            this.windowMinimization.Image = ((System.Drawing.Image)(resources.GetObject("windowMinimization.Image")));
            this.windowMinimization.Location = new System.Drawing.Point(983, 3);
            this.windowMinimization.Name = "windowMinimization";
            this.windowMinimization.Size = new System.Drawing.Size(24, 24);
            this.windowMinimization.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.windowMinimization.TabIndex = 45;
            this.windowMinimization.TabStop = false;
            this.windowMinimization.Click += new System.EventHandler(this.Minimization);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.panel8.Controls.Add(this.salesAmount);
            this.panel8.Controls.Add(this.pictureBox8);
            this.panel8.Controls.Add(this.label9);
            this.panel8.Location = new System.Drawing.Point(722, 200);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(275, 154);
            this.panel8.TabIndex = 24;
            // 
            // salesAmount
            // 
            this.salesAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.salesAmount.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.salesAmount.ForeColor = System.Drawing.SystemColors.MenuText;
            this.salesAmount.Location = new System.Drawing.Point(22, 88);
            this.salesAmount.Name = "salesAmount";
            this.salesAmount.Size = new System.Drawing.Size(112, 32);
            this.salesAmount.TabIndex = 13;
            this.salesAmount.Text = "金额";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(139, 14);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(125, 125);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox8.TabIndex = 11;
            this.pictureBox8.TabStop = false;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label9.Location = new System.Drawing.Point(16, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(118, 32);
            this.label9.TabIndex = 12;
            this.label9.Text = "总销售额";
            // 
            // windowMaximize
            // 
            this.windowMaximize.BackColor = System.Drawing.Color.Transparent;
            this.windowMaximize.Image = ((System.Drawing.Image)(resources.GetObject("windowMaximize.Image")));
            this.windowMaximize.Location = new System.Drawing.Point(1007, 3);
            this.windowMaximize.Name = "windowMaximize";
            this.windowMaximize.Size = new System.Drawing.Size(24, 24);
            this.windowMaximize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.windowMaximize.TabIndex = 44;
            this.windowMaximize.TabStop = false;
            this.windowMaximize.Click += new System.EventHandler(this.MaximizeOrRestore);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Gainsboro;
            this.panel7.Controls.Add(this.bookCount);
            this.panel7.Controls.Add(this.pictureBox7);
            this.panel7.Controls.Add(this.label1);
            this.panel7.Location = new System.Drawing.Point(59, 200);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(275, 154);
            this.panel7.TabIndex = 23;
            // 
            // bookCount
            // 
            this.bookCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bookCount.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.bookCount.ForeColor = System.Drawing.SystemColors.MenuText;
            this.bookCount.Location = new System.Drawing.Point(22, 88);
            this.bookCount.Name = "bookCount";
            this.bookCount.Size = new System.Drawing.Size(112, 32);
            this.bookCount.TabIndex = 13;
            this.bookCount.Text = "数量";
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(140, 14);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(125, 125);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 11;
            this.pictureBox7.TabStop = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label1.Location = new System.Drawing.Point(16, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 32);
            this.label1.TabIndex = 12;
            this.label1.Text = "书籍库存";
            // 
            // AccountManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Indigo;
            this.ClientSize = new System.Drawing.Size(1285, 884);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AccountManager";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AccountManager";
            this.Load += new System.EventHandler(this.AccountManager_Load);
            this.SizeChanged += new System.EventHandler(this.AccountManager_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.ExitPic)).EndInit();
            this.ExitBtn.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.AccountManagerBtn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AccountManagerPic)).EndInit();
            this.UsersBtn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UsersPic)).EndInit();
            this.BooksBtn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BooksPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.windowRestore)).EndInit();
            this.panel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowMinimization)).EndInit();
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowMaximize)).EndInit();
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.PictureBox ExitPic;
        private System.Windows.Forms.Label ExitLabel;
        private System.Windows.Forms.Panel ExitBtn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel AccountManagerBtn;
        private System.Windows.Forms.PictureBox AccountManagerPic;
        private System.Windows.Forms.Label AccountManagerLabel;
        private System.Windows.Forms.Panel UsersBtn;
        private System.Windows.Forms.PictureBox UsersPic;
        private System.Windows.Forms.Label UsersLabel;
        private System.Windows.Forms.Panel BooksBtn;
        private System.Windows.Forms.PictureBox BooksPic;
        private System.Windows.Forms.Label BooksLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label salesAmount;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label bookCount;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label number;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox windowRestore;
        private System.Windows.Forms.PictureBox windowClose;
        private System.Windows.Forms.PictureBox windowMinimization;
        private System.Windows.Forms.PictureBox windowMaximize;
    }
}