﻿using System;
using System.Drawing;
using System.Windows.Forms;
using book_store_app.Constant;
using book_store_app.Entity;
using book_store_app.Enum;
using book_store_app.Model;
using book_store_app.Properties;
using book_store_app.View.Navigation;
using book_store_app.View.Utils;

namespace book_store_app.View
{
    public partial class Books : Form
    {
        private readonly BookViewModel _bookViewModel;

        private readonly ViewNavigation _viewNavigation;
        
        private readonly AutoResizeForm _autoResizeForm = new AutoResizeForm();

        public Books()
        {
            // 启用双缓冲
            SetStyle(ControlStyles.DoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
            UpdateStyles();
            InitializeComponent();
            _bookViewModel = BookViewModel.GetInstance();
            _viewNavigation = ViewNavigation.GetInstance();
            InitBookList();
        }

        #region 窗口右上角点击事件

        private void Close(object sender, EventArgs e)
        {
            // 退出
            Application.Exit();
        }
        
        private void Minimization(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
        
        private void MaximizeOrRestore(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                WindowState = FormWindowState.Maximized;
                windowMaximize.Visible = false;
                windowRestore.Visible = true;
            }
            else
            {
                WindowState = FormWindowState.Normal;
                windowMaximize.Visible = true;
                windowRestore.Visible = false;
            }
        }

        #endregion
        
        #region 初始化书籍列表

        /// <summary>
        /// 初始化书籍列表
        /// </summary>
        private void InitBookList()
        {
            FlushedData();
            var saveEditBtn = new DataGridViewButtonColumn
            {
                Name = "saveEditBtn",
                HeaderText = "",
                Text = "保存编辑",
                UseColumnTextForButtonValue = true
            };
            saveEditBtn.FlatStyle = FlatStyle.Flat;
            saveEditBtn.DefaultCellStyle.ForeColor = Color.ForestGreen;
            var deleteBtn = new DataGridViewButtonColumn
            {
                Name = "deleteBtn",
                HeaderText = "",
                Text = Resources.delete,
                UseColumnTextForButtonValue = true
            };
            deleteBtn.FlatStyle = FlatStyle.Flat;
            deleteBtn.DefaultCellStyle.ForeColor = Color.OrangeRed;

            // 设置书籍ID列为只读
            BooksDataGridView.Columns[0].ReadOnly = true;
            BooksDataGridView.Columns.Add(saveEditBtn);
            BooksDataGridView.Columns.Add(deleteBtn);
            BooksDataGridView.CellContentClick += SaveEditBtnClick;
            BooksDataGridView.CellContentClick += DeleteBtnClick;
        }

        #endregion
        
        #region 刷新数据方法

        /// <summary>
        /// 刷新数据
        /// </summary>
        public void FlushedData()
        {
            // 刷新书籍列表
            ViewUtil.FlushedBooks(BooksDataGridView, _bookViewModel.GetBooks());
        }

        #endregion
        
        #region 单元格点击事件

        /// <summary>
        /// 删除按钮点击事件
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">数据网格视图单元格事件参数</param>
        private void DeleteBtnClick(object sender, DataGridViewCellEventArgs e)
        {
            if (ViewUtil.DataGridViewColumnIsNull(BooksDataGridView, e, "deleteBtn"))
            {
                return;
            }
            
            if(ViewUtil.ConfirmDialogBoxIsDisplayedByQuestion(Resources.delete, Resources.tips_action_text))
            {
                return;
            }

            var row = BooksDataGridView.Rows[e.RowIndex];
            var id = Convert.ToInt32(row.Cells[2].Value);
            if (_bookViewModel.Delete(id) > 0)
            {
                MessageBox.Show(Resources.message_delete_success);
                if (searchValue.Text != StringConstant.EmptyString)
                {
                    ViewUtil.FlushedBooks(BooksDataGridView, _bookViewModel.Search(searchValue.Text));
                }
                else
                {
                    FlushedData();
                }
            }
            else
            {
                MessageBox.Show(Resources.message_delete_fail);
            }
        }

        /// <summary>
        /// 保存编辑按钮点击事件
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">数据网格视图单元格事件参数</param>
        private void SaveEditBtnClick(object sender, DataGridViewCellEventArgs e)
        {
            if (ViewUtil.DataGridViewColumnIsNull(BooksDataGridView, e, "saveEditBtn"))
            {
                return;
            }

            var row = BooksDataGridView.Rows[e.RowIndex];
            var book = new Book
            {
                Id = Convert.ToInt32(row.Cells[2].Value),
                Name = row.Cells[3].Value.ToString(),
                Author = row.Cells[4].Value.ToString(),
                Type = row.Cells[5].Value.ToString(),
                Count = Convert.ToInt32(row.Cells[6].Value),
                Price = Convert.ToDecimal(row.Cells[7].Value)
            };
            if (_bookViewModel.Save(book) > 0)
            {
                MessageBox.Show(Resources.message_save_success);
                FlushedData();
            }
            else
            {
                MessageBox.Show(Resources.message_save_fail);
            }
        }

        #endregion
        
        #region 按钮点击事件

        /// <summary>
        /// 保存按钮点击事件
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">事件参数</param>
        private void SaveBtnClick(object sender, EventArgs e)
        {
            if (StringConstant.EmptyString.Equals(name.Text) ||
                StringConstant.EmptyString.Equals(author.Text) ||
                StringConstant.EmptyString.Equals(price.Text) ||
                StringConstant.EmptyString.Equals(count.Text) ||
                StringConstant.EmptyString.Equals(type.Text))
            {
                ViewUtil.ConfirmDialogBoxIsDisplayedByError("错误", "信息不能为空");
                return;
            }

            try
            {
                var book = new Book
                {
                    Name = name.Text,
                    Author = author.Text,
                    Type = type.Text,
                    Price = Convert.ToDecimal(price.Text),
                    Count = Convert.ToInt32(count.Text)
                };
                if (_bookViewModel.Save(book) > 0)
                {
                    // 清空数据
                    name.Text = StringConstant.EmptyString;
                    author.Text = StringConstant.EmptyString;
                    price.Text = StringConstant.EmptyString;
                    count.Text = StringConstant.EmptyString;
                    type.Text = StringConstant.EmptyString;
                    ViewUtil.ConfirmDialogBoxIsDisplayedByInfo("提示", Resources.message_save_success);
                    FlushedData();
                }
                else
                {
                    ViewUtil.ConfirmDialogBoxIsDisplayedByError("错误", Resources.message_save_fail);
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                ViewUtil.ConfirmDialogBoxIsDisplayedByError("错误", exception.Message);
            }
        }

        

        /// <summary>
        /// 搜索按钮点击事件
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">数据网格视图单元格事件参数</param>
        private void SearchBtnClick(object sender, EventArgs e)
        {
            ViewUtil.FlushedBooks(BooksDataGridView, _bookViewModel.Search(searchValue.Text));
        }

        #endregion
        
        #region 跳转页面按钮点击事件

        private void BooksLabel_Click(object sender, EventArgs e)
        {
            FlushedData();
        }

        private void BooksPic_Click(object sender, EventArgs e)
        {
            FlushedData();
        }

        private void UsersLabel_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Users, this);
        }

        private void UsesPic_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Users, this);
        }

        private void AccountManagerLabel_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.AccountManager, this);
        }

        private void AccountManagerPic_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.AccountManager, this);
        }

        private void ExitLabel_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Login, this);
        }

        private void ExitPic_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Login, this);
        }

        #endregion

        #region 窗口事件

        private void Books_Load(object sender, EventArgs e)
        {
            _autoResizeForm.ControlInitializeSize(this);
        }

        private void Books_SizeChanged(object sender, EventArgs e)
        {
            _autoResizeForm.ControlAutoSize(this);
        }

        #endregion
    }
}