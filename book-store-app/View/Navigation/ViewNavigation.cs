﻿using System;
using System.Windows.Forms;
using book_store_app.Data;
using book_store_app.Enum;
using book_store_app.Properties;
using book_store_app.View.Utils;
using Timer = System.Windows.Forms.Timer;

namespace book_store_app.View.Navigation
{
    /// <summary>
    /// 视图导航类
    /// </summary>
    public class ViewNavigation
    {
        private static readonly ViewNavigation Instance = new ViewNavigation();

        private AccountManager _accountManager;

        private Books _books;

        private Login _login;

        private Orders _orders;

        private PersonalCenter _personalCenter;

        private Register _register;

        private ShoppingCart _shoppingCart;

        private Users _users;

        private readonly DataContext _dataContext;

        private ViewNavigation()
        {
            _dataContext = DataContext.GetInstance();
        }

        public static ViewNavigation GetInstance()
        {
            return Instance;
        }

        /// <summary>
        /// 跳转页面
        /// </summary>
        /// <param name="viewEnum">页面枚举</param>
        /// <param name="currentPage">当前页面</param>
        public void Jump(ViewEnum viewEnum, Form currentPage)
        {
            currentPage.Hide();
            switch (viewEnum)
            {
                case ViewEnum.AccountManager:
                    _accountManager.FlushedData();
                    _accountManager.Show();
                    break;
                case ViewEnum.Books:
                    _books.FlushedData();
                    _books.Show();
                    break;
                case ViewEnum.Login:
                    _dataContext.ClearLoginData();
                    _login.Show();
                    break;
                case ViewEnum.Orders:
                    _orders.FlushedData();
                    _orders.Show();
                    break;
                case ViewEnum.PersonalCenter:
                    _personalCenter.FlushedData();
                    _personalCenter.Show();
                    break;
                case ViewEnum.Register:
                    _register.Show();
                    break;
                case ViewEnum.ShoppingCart:
                    _shoppingCart.FlushedData();
                    _shoppingCart.Show();
                    break;
                case ViewEnum.Users:
                    _users.FlushedData();
                    _users.Show();
                    break;

                default:
                    ViewUtil.ConfirmDialogBoxIsDisplayedByError("错误", "未知页面!\n将退出到登录页!");
                    _login.Show();
                    break;
            }
        }

        /// <summary>
        /// 初始化视图导航类
        /// </summary>
        /// <param name="loadingProgressBar">进度条</param>
        /// <param name="loadingTimer">进度条计时器</param>
        public void Init(ProgressBar loadingProgressBar, Timer loadingTimer)
        {
            loadingProgressBar.Value = 0;
            // 初始化所有页面
            _accountManager = new AccountManager();
            loadingProgressBar.Value = 13;
            _books = new Books();
            loadingProgressBar.Value = 25;
            _login = new Login();
            loadingProgressBar.Value = 37;
            _orders = new Orders();
            loadingProgressBar.Value = 50;
            _personalCenter = new PersonalCenter();
            loadingProgressBar.Value = 62;
            _register = new Register();
            loadingProgressBar.Value = 75;
            _shoppingCart = new ShoppingCart();
            loadingProgressBar.Value = 87;
            _users = new Users();
            loadingProgressBar.Value = 100;
            loadingTimer.Stop();
            loadingTimer.Dispose();
        }

        /// <summary>
        /// 释放页面资源
        /// </summary>
        public void Destroy()
        {
            Console.WriteLine(Resources.ViewNavigation_Destroy_start_closing_windows_and_freeing_up_resources_);
            _accountManager.Close();
            _accountManager.Dispose();
            _books.Close();
            _books.Dispose();
            _login.Close();
            _login.Dispose();
            _orders.Close();
            _orders.Dispose();
            _personalCenter.Close();
            _personalCenter.Dispose();
            _register.Close();
            _register.Dispose();
            _shoppingCart.Close();
            _shoppingCart.Dispose();
            _users.Close();
            _users.Dispose();
            Console.WriteLine(Resources.ViewNavigation_Destroy_windows_resources_released_);
        }
    }
}