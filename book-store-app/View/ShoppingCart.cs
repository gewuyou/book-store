﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using book_store_app.Constant;
using book_store_app.Data;
using book_store_app.Entity;
using book_store_app.Enum;
using book_store_app.Model;
using book_store_app.Properties;
using book_store_app.View.Navigation;
using book_store_app.View.Utils;

namespace book_store_app.View
{
    /// <summary>
    /// 购物车(基于内存的购物车)
    /// </summary>
    public partial class ShoppingCart : Form
    {
        private readonly DataContext _dataContext;

        private readonly ShoppingCartViewModel _shoppingCartViewModel;
        
        private readonly ViewNavigation _viewNavigation;
        
        private readonly AutoResizeForm _autoResizeForm = new AutoResizeForm();

        private Book _book;

        public ShoppingCart()
        {
            // 启用双缓冲
            SetStyle(ControlStyles.DoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
            UpdateStyles();
            InitializeComponent();
            _dataContext = DataContext.GetInstance();
            _shoppingCartViewModel = ShoppingCartViewModel.GetInstance();
            _viewNavigation = ViewNavigation.GetInstance();
            InitBookList();
        }

        #region 窗口右上角按钮点击事件

        private void Close(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Minimization(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void MaximizeOrRestore(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                WindowState = FormWindowState.Maximized;
                windowMaximize.Visible = false;
                windowRestore.Visible = true;
            }
            else
            {
                WindowState = FormWindowState.Normal;
                windowMaximize.Visible = true;
                windowRestore.Visible = false;
            }
        }

        #endregion
        
        #region 初始化书籍列表

        /// <summary>
        /// 初始化书籍列表
        /// </summary>
        private void InitBookList()
        {
            FlushedData();
            // 设置书籍列表为只读
            ShoppingCartDataGridView.Columns[0].ReadOnly = true;
            ShoppingCartDataGridView.Columns[1].ReadOnly = true;
            ShoppingCartDataGridView.Columns[2].ReadOnly = true;
            ShoppingCartDataGridView.Columns[3].ReadOnly = true;
            ShoppingCartDataGridView.Columns[4].ReadOnly = true;
            ShoppingCartDataGridView.Columns[5].ReadOnly = true;
            ShoppingCartDataGridView.CellContentClick += CellContentClick;
        }

        #endregion
        
        #region 刷新数据方法
        public void FlushedData()
        {
            RefreshBalance();
            RefreshBooks();
        }

        private void RefreshBooks()
        {
            var dataTable = new DataTable();
            dataTable.Columns.Add("书籍ID", typeof(int));
            dataTable.Columns.Add("书籍名称", typeof(string));
            dataTable.Columns.Add("书籍作者", typeof(string));
            dataTable.Columns.Add("书籍类型", typeof(string));
            dataTable.Columns.Add("购买数量", typeof(int));
            dataTable.Columns.Add("书籍价格", typeof(decimal));
            // 获取购物车数据
            if (!(_dataContext.Get("shoppingCartList") is List<Book> shoppingCartList))
            {
                return;
            }

            foreach (var book in shoppingCartList)
            {
                dataTable.Rows.Add(book.Id, book.Name, book.Author, book.Type, book.Count, book.Price);
            }

            // 将数据源绑定到 DataGridView
            ShoppingCartDataGridView.DataSource = dataTable;
        }

        private void RefreshBalance()
        {
            if (!(_dataContext.Get("loginUser") is User user))
            {
                return;
            }

            accountBalance.Text = user.Balance + Resources.AccountManager_FlushedData_yuan;
        }

        private void UpdateUserData()
        {
            _shoppingCartViewModel.UpdateUserData();
        }
        

        #endregion

        #region 单元格点击事件
        /// <summary>
        /// 添加单元格点击事件
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">数据网格视图单元格事件参数</param>
        private void CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex <0|| e.ColumnIndex < 0)
            {
                return;
            }
            // 获取当前选中的Book对象
            var row = ShoppingCartDataGridView.Rows[e.RowIndex];
            _book = new Book
            {
                Id = Convert.ToInt32(row.Cells[0].Value),
                Name = row.Cells[1].Value.ToString(),
                Author = row.Cells[2].Value.ToString(),
                Type = row.Cells[3].Value.ToString(),
                Count = Convert.ToInt32(row.Cells[4].Value),
                Price = Convert.ToDecimal(row.Cells[5].Value)
            };
            name.Text = _book.Name;
            author.Text = _book.Author;
            count.Text = _book.Count.ToString();
        }
        

        #endregion

        #region 按钮点击事件

        /// <summary>
        /// 清空购物车
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">事件参数</param>
        private void ResetShoppingCartBtnClick(object sender, EventArgs e)
        {
            if (ViewUtil.ConfirmDialogBoxIsDisplayedByQuestion("清空", "确定要清空购物车吗?"))
            {
                return;
            }

            var books = _dataContext.Get("shoppingCartList") as List<Book>;
            var bookIdAndCount = _dataContext.Get("shoppingCartIdAndCount") as Dictionary<int, int>;
            books?.Clear();
            bookIdAndCount?.Clear();
            MessageBox.Show(Resources.ShoppingCart_ResetShoppingCartBtnClick_the_cart_is_empty_);
            FlushedData();
        }

        /// <summary>
        /// 结算
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">事件参数</param>
        private void SettlementBtnClick(object sender, EventArgs e)
        {
            if (ViewUtil.ConfirmDialogBoxIsDisplayedByQuestion("结算", "确定要结算吗?"))
            {
                return;
            }

            if (!(_dataContext.Get("shoppingCartList") is List<Book> books) || books.Count == 0)
            {
                MessageBox.Show(Resources.ShoppingCart_SettlementBtnClick_the_cart_is_empty_);
                return;
            }

            // 结算
            // 判断库存是否足够
            // var booksInventory = _shoppingCartViewModel.CheckInventory(books);
            // if (booksInventory.Count > 0)
            // {
            //     var message = new StringBuilder("以下书籍库存不足:\n");
            //     foreach (var book in booksInventory)
            //     {
            //         message.Append(book.Name).Append("\n");
            //     }
            //
            //     MessageBox.Show(message.ToString());
            //     return;
            // }
            UpdateUserData();
            // 检查用户余额是否足够
            if (!(_dataContext.Get("loginUser") is User user))
            {
                return;
            }
            // 计算收益
            var earnings = books.Sum(book => (book.Count * book.Price));
            if (earnings.CompareTo(user.Balance) > 0)
            {
                MessageBox.Show(Resources.ShoppingCart_SettlementBtnClick_insufficient_balance_settlement_failed_);
                return;
            }
            // 计算用户余额
            user.Balance -= earnings;
            if (!(_dataContext.Get("bookIdAndCount") is Dictionary<int, int> bookIdAndCount))
            {
                ViewUtil.ConfirmDialogBoxIsDisplayedByError("错误", "内部错误，请联系管理员!");
                return;
            }
            // 计算结算后的书本数量
            foreach (var book in books)
            {
                book.Count =bookIdAndCount[book.Id]-book.Count; 
            }
            if (_shoppingCartViewModel.Settlement(user,books,earnings) > 0)
            {
                MessageBox.Show(Resources.ShoppingCart_SettlementBtnClick_settlement_successful_);
                // 清空购物车
                var booksList = _dataContext.Get("shoppingCartList") as List<Book>;
                booksList?.Clear();
                bookIdAndCount.Clear();
                UpdateUserData();
                FlushedData();
                return;
            }

            MessageBox.Show(Resources.ShoppingCart_SettlementBtnClick_);
        }

        /// <summary>
        /// 保存编辑按钮
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">事件参数</param>
        private void SaveEditBtnClick(object sender, EventArgs e)
        {
            var countText = count.Text;
            if (StringConstant.EmptyString.Equals(countText))
            {
                return;
            }

            try
            {
                var buyCount = Convert.ToInt32(count.Text);
                // 更新购物车
                if (!(_dataContext.Get("shoppingCartList") is List<Book> books))
                {
                    return;
                }

                if (!(_dataContext.Get("bookIdAndCount") is Dictionary<int, int> bookIdAndCount))
                {
                    return;
                }
                foreach (var b in books.Where(b => b.Id == _book.Id))
                {
                    if (bookIdAndCount[b.Id] >= buyCount)
                    {
                        b.Count = buyCount;
                    }else
                    {
                        MessageBox.Show(Resources.Orders_AddToCartBtnClick_purchase_quantity_cannot_be_greater_inventory_);
                        return;
                    }
                }

                RefreshBooks();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                ViewUtil.ConfirmDialogBoxIsDisplayedByError("错误", "购买数量必须是整数!");
            }
            
        }
        /// <summary>
        /// 移出购物车
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">事件参数</param>
        private void RemoveToCartBtnClick(object sender, EventArgs e)
        {
            if (ViewUtil.ConfirmDialogBoxIsDisplayedByQuestion(Resources.delete, Resources.tips_action_text))
            {
                return;
            }
            
            var books = _dataContext.Get("shoppingCartList") as List<Book>;
            var bookIdAndCount = _dataContext.Get("bookIdAndCount") as Dictionary<int, int>;
            // 删除列表中的书籍
            books?.RemoveAll(book => book.Id == _book.Id);
            // 删除字典中的书籍
            bookIdAndCount?.Remove(_book.Id);
            MessageBox.Show(Resources.message_delete_success);
            RefreshBooks();
        }

        #endregion

        #region 跳转页面按钮点击事件
        private void PersonalCenterLabel_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.PersonalCenter, this);
        }

        private void PersonalCenterPic_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.PersonalCenter, this);
        }

        private void ShoppingCartLabel_Click(object sender, EventArgs e)
        {
            FlushedData();
        }

        private void ShoppingCartPic_Click(object sender, EventArgs e)
        {
            FlushedData();
        }

        private void OrdersLabel_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Orders, this);
        }

        private void OrdersPic_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Orders, this);
        }

        private void ExitLabel_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Login, this);
        }

        private void ExitPic_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Login, this);
        }
        #endregion

        #region 窗口事件

        private void ShoppingCart_Load(object sender, EventArgs e)
        {
            _autoResizeForm.ControlInitializeSize(this);
        }

        private void ShoppingCart_SizeChanged(object sender, EventArgs e)
        {
            _autoResizeForm.ControlAutoSize(this);
        }

        #endregion
    }
}