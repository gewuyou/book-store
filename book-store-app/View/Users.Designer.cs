﻿using System.Windows.Forms;

namespace book_store_app.View
{
    partial class Users
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Users));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.searchBtn = new System.Windows.Forms.Button();
            this.searchValue = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.saveBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.phone = new System.Windows.Forms.TextBox();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.username = new System.Windows.Forms.TextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ExitBtn = new System.Windows.Forms.Panel();
            this.ExitPic = new System.Windows.Forms.PictureBox();
            this.ExitLabel = new System.Windows.Forms.Label();
            this.AccountManagerBtn = new System.Windows.Forms.Panel();
            this.AccountManagerPic = new System.Windows.Forms.PictureBox();
            this.AccountManagerLabel = new System.Windows.Forms.Label();
            this.UsersBtn = new System.Windows.Forms.Panel();
            this.UsersPic = new System.Windows.Forms.PictureBox();
            this.UsersLabel = new System.Windows.Forms.Label();
            this.BooksBtn = new System.Windows.Forms.Panel();
            this.BooksPic = new System.Windows.Forms.PictureBox();
            this.BooksLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Title = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.UsersDataGridView = new System.Windows.Forms.DataGridView();
            this.windowRestore = new System.Windows.Forms.PictureBox();
            this.windowClose = new System.Windows.Forms.PictureBox();
            this.windowMinimization = new System.Windows.Forms.PictureBox();
            this.windowMaximize = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.password = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.address = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.ExitBtn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ExitPic)).BeginInit();
            this.AccountManagerBtn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AccountManagerPic)).BeginInit();
            this.UsersBtn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UsersPic)).BeginInit();
            this.BooksBtn.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BooksPic)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UsersDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowRestore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowMinimization)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowMaximize)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(351, 332);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(36, 36);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 38;
            this.pictureBox7.TabStop = false;
            // 
            // searchBtn
            // 
            this.searchBtn.BackColor = System.Drawing.Color.Transparent;
            this.searchBtn.FlatAppearance.BorderColor = System.Drawing.Color.ForestGreen;
            this.searchBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchBtn.Font = new System.Drawing.Font("霞鹜文楷", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.searchBtn.ForeColor = System.Drawing.Color.ForestGreen;
            this.searchBtn.Location = new System.Drawing.Point(655, 332);
            this.searchBtn.Margin = new System.Windows.Forms.Padding(2);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(90, 34);
            this.searchBtn.TabIndex = 37;
            this.searchBtn.Text = "搜索";
            this.searchBtn.UseVisualStyleBackColor = false;
            this.searchBtn.Click += new System.EventHandler(this.SearchBtnClick);
            // 
            // searchValue
            // 
            this.searchValue.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.searchValue.Location = new System.Drawing.Point(391, 332);
            this.searchValue.Margin = new System.Windows.Forms.Padding(2);
            this.searchValue.Name = "searchValue";
            this.searchValue.Size = new System.Drawing.Size(259, 34);
            this.searchValue.TabIndex = 36;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.Font = new System.Drawing.Font("霞鹜文楷", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.ForeColor = System.Drawing.Color.ForestGreen;
            this.label10.Location = new System.Drawing.Point(461, 294);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(141, 38);
            this.label10.TabIndex = 35;
            this.label10.Text = "用户列表";
            // 
            // saveBtn
            // 
            this.saveBtn.BackColor = System.Drawing.Color.Transparent;
            this.saveBtn.FlatAppearance.BorderColor = System.Drawing.Color.ForestGreen;
            this.saveBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.saveBtn.Font = new System.Drawing.Font("霞鹜文楷", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.saveBtn.ForeColor = System.Drawing.Color.ForestGreen;
            this.saveBtn.Location = new System.Drawing.Point(949, 204);
            this.saveBtn.Margin = new System.Windows.Forms.Padding(2);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(90, 34);
            this.saveBtn.TabIndex = 33;
            this.saveBtn.Text = "保存";
            this.saveBtn.UseVisualStyleBackColor = false;
            this.saveBtn.Click += new System.EventHandler(this.SaveBtnClick);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label1.Location = new System.Drawing.Point(242, 172);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 30);
            this.label1.TabIndex = 26;
            this.label1.Text = "电话号码";
            // 
            // phone
            // 
            this.phone.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.phone.Location = new System.Drawing.Point(247, 204);
            this.phone.Margin = new System.Windows.Forms.Padding(2);
            this.phone.Name = "phone";
            this.phone.Size = new System.Drawing.Size(170, 34);
            this.phone.TabIndex = 25;
            // 
            // usernameLabel
            // 
            this.usernameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.usernameLabel.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.usernameLabel.ForeColor = System.Drawing.SystemColors.GrayText;
            this.usernameLabel.Location = new System.Drawing.Point(16, 172);
            this.usernameLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(67, 30);
            this.usernameLabel.TabIndex = 24;
            this.usernameLabel.Text = "用户名";
            // 
            // username
            // 
            this.username.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.username.Location = new System.Drawing.Point(21, 204);
            this.username.Margin = new System.Windows.Forms.Padding(2);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(170, 34);
            this.username.TabIndex = 23;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(13, 12);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(45, 47);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 10;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(448, 105);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(91, 58);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Menu;
            this.panel1.Controls.Add(this.ExitBtn);
            this.panel1.Controls.Add(this.AccountManagerBtn);
            this.panel1.Controls.Add(this.UsersBtn);
            this.panel1.Controls.Add(this.BooksBtn);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(202, 860);
            this.panel1.TabIndex = 4;
            // 
            // ExitBtn
            // 
            this.ExitBtn.BackColor = System.Drawing.Color.Transparent;
            this.ExitBtn.Controls.Add(this.ExitPic);
            this.ExitBtn.Controls.Add(this.ExitLabel);
            this.ExitBtn.Location = new System.Drawing.Point(18, 346);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Size = new System.Drawing.Size(166, 49);
            this.ExitBtn.TabIndex = 18;
            // 
            // ExitPic
            // 
            this.ExitPic.Image = ((System.Drawing.Image)(resources.GetObject("ExitPic.Image")));
            this.ExitPic.Location = new System.Drawing.Point(3, 3);
            this.ExitPic.Name = "ExitPic";
            this.ExitPic.Size = new System.Drawing.Size(43, 43);
            this.ExitPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ExitPic.TabIndex = 11;
            this.ExitPic.TabStop = false;
            this.ExitPic.Click += new System.EventHandler(this.ExitPic_Click);
            // 
            // ExitLabel
            // 
            this.ExitLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ExitLabel.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ExitLabel.ForeColor = System.Drawing.SystemColors.MenuText;
            this.ExitLabel.Location = new System.Drawing.Point(54, 9);
            this.ExitLabel.Name = "ExitLabel";
            this.ExitLabel.Size = new System.Drawing.Size(109, 33);
            this.ExitLabel.TabIndex = 12;
            this.ExitLabel.Text = "退出";
            this.ExitLabel.Click += new System.EventHandler(this.ExitLabel_Click);
            // 
            // AccountManagerBtn
            // 
            this.AccountManagerBtn.BackColor = System.Drawing.Color.Transparent;
            this.AccountManagerBtn.Controls.Add(this.AccountManagerPic);
            this.AccountManagerBtn.Controls.Add(this.AccountManagerLabel);
            this.AccountManagerBtn.Location = new System.Drawing.Point(18, 262);
            this.AccountManagerBtn.Name = "AccountManagerBtn";
            this.AccountManagerBtn.Size = new System.Drawing.Size(166, 49);
            this.AccountManagerBtn.TabIndex = 19;
            // 
            // AccountManagerPic
            // 
            this.AccountManagerPic.Image = ((System.Drawing.Image)(resources.GetObject("AccountManagerPic.Image")));
            this.AccountManagerPic.Location = new System.Drawing.Point(3, 3);
            this.AccountManagerPic.Name = "AccountManagerPic";
            this.AccountManagerPic.Size = new System.Drawing.Size(43, 43);
            this.AccountManagerPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.AccountManagerPic.TabIndex = 11;
            this.AccountManagerPic.TabStop = false;
            this.AccountManagerPic.Click += new System.EventHandler(this.AccountManagerPic_Click);
            // 
            // AccountManagerLabel
            // 
            this.AccountManagerLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.AccountManagerLabel.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.AccountManagerLabel.ForeColor = System.Drawing.SystemColors.MenuText;
            this.AccountManagerLabel.Location = new System.Drawing.Point(52, 9);
            this.AccountManagerLabel.Name = "AccountManagerLabel";
            this.AccountManagerLabel.Size = new System.Drawing.Size(113, 33);
            this.AccountManagerLabel.TabIndex = 12;
            this.AccountManagerLabel.Text = "账户管理";
            this.AccountManagerLabel.Click += new System.EventHandler(this.AccountManagerLabel_Click);
            // 
            // UsersBtn
            // 
            this.UsersBtn.BackColor = System.Drawing.Color.Ivory;
            this.UsersBtn.Controls.Add(this.UsersPic);
            this.UsersBtn.Controls.Add(this.UsersLabel);
            this.UsersBtn.Location = new System.Drawing.Point(18, 178);
            this.UsersBtn.Name = "UsersBtn";
            this.UsersBtn.Size = new System.Drawing.Size(166, 49);
            this.UsersBtn.TabIndex = 17;
            // 
            // UsersPic
            // 
            this.UsersPic.Image = ((System.Drawing.Image)(resources.GetObject("UsersPic.Image")));
            this.UsersPic.Location = new System.Drawing.Point(3, 3);
            this.UsersPic.Name = "UsersPic";
            this.UsersPic.Size = new System.Drawing.Size(43, 43);
            this.UsersPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.UsersPic.TabIndex = 11;
            this.UsersPic.TabStop = false;
            this.UsersPic.Click += new System.EventHandler(this.UsersPic_Click);
            // 
            // UsersLabel
            // 
            this.UsersLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.UsersLabel.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.UsersLabel.ForeColor = System.Drawing.SystemColors.MenuText;
            this.UsersLabel.Location = new System.Drawing.Point(54, 9);
            this.UsersLabel.Name = "UsersLabel";
            this.UsersLabel.Size = new System.Drawing.Size(112, 33);
            this.UsersLabel.TabIndex = 12;
            this.UsersLabel.Text = "用户管理";
            this.UsersLabel.Click += new System.EventHandler(this.UsersLabel_Click);
            // 
            // BooksBtn
            // 
            this.BooksBtn.BackColor = System.Drawing.Color.Transparent;
            this.BooksBtn.Controls.Add(this.BooksPic);
            this.BooksBtn.Controls.Add(this.BooksLabel);
            this.BooksBtn.Location = new System.Drawing.Point(18, 94);
            this.BooksBtn.Name = "BooksBtn";
            this.BooksBtn.Size = new System.Drawing.Size(166, 49);
            this.BooksBtn.TabIndex = 16;
            // 
            // BooksPic
            // 
            this.BooksPic.Image = ((System.Drawing.Image)(resources.GetObject("BooksPic.Image")));
            this.BooksPic.Location = new System.Drawing.Point(3, 3);
            this.BooksPic.Name = "BooksPic";
            this.BooksPic.Size = new System.Drawing.Size(43, 43);
            this.BooksPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.BooksPic.TabIndex = 11;
            this.BooksPic.TabStop = false;
            this.BooksPic.Click += new System.EventHandler(this.BooksPic_Click);
            // 
            // BooksLabel
            // 
            this.BooksLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BooksLabel.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BooksLabel.ForeColor = System.Drawing.SystemColors.MenuText;
            this.BooksLabel.Location = new System.Drawing.Point(54, 9);
            this.BooksLabel.Name = "BooksLabel";
            this.BooksLabel.Size = new System.Drawing.Size(109, 33);
            this.BooksLabel.TabIndex = 12;
            this.BooksLabel.Text = "书籍";
            this.BooksLabel.Click += new System.EventHandler(this.BooksLabel_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.Font = new System.Drawing.Font("霞鹜文楷", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label4.Location = new System.Drawing.Point(62, 16);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(158, 40);
            this.label4.TabIndex = 10;
            this.label4.Text = "用户管理";
            // 
            // Title
            // 
            this.Title.Location = new System.Drawing.Point(0, 0);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(100, 23);
            this.Title.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.UsersDataGridView);
            this.panel2.Controls.Add(this.windowRestore);
            this.panel2.Controls.Add(this.windowClose);
            this.panel2.Controls.Add(this.windowMinimization);
            this.panel2.Controls.Add(this.windowMaximize);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.password);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.address);
            this.panel2.Controls.Add(this.pictureBox7);
            this.panel2.Controls.Add(this.searchBtn);
            this.panel2.Controls.Add(this.searchValue);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.saveBtn);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.phone);
            this.panel2.Controls.Add(this.usernameLabel);
            this.panel2.Controls.Add(this.username);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.Title);
            this.panel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel2.Location = new System.Drawing.Point(214, 12);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1058, 860);
            this.panel2.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.Font = new System.Drawing.Font("霞鹜文楷", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.ForeColor = System.Drawing.Color.ForestGreen;
            this.label9.Location = new System.Drawing.Point(431, 61);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(154, 33);
            this.label9.TabIndex = 53;
            this.label9.Text = "用户管理";
            // 
            // UsersDataGridView
            // 
            this.UsersDataGridView.AllowUserToAddRows = false;
            this.UsersDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.UsersDataGridView.BackgroundColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Indigo;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.UsersDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.UsersDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.UsersDataGridView.GridColor = System.Drawing.Color.LightGray;
            this.UsersDataGridView.Location = new System.Drawing.Point(5, 392);
            this.UsersDataGridView.Name = "UsersDataGridView";
            this.UsersDataGridView.RowHeadersVisible = false;
            this.UsersDataGridView.RowHeadersWidth = 51;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.MediumSlateBlue;
            this.UsersDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.UsersDataGridView.RowTemplate.Height = 27;
            this.UsersDataGridView.Size = new System.Drawing.Size(1050, 465);
            this.UsersDataGridView.TabIndex = 52;
            // 
            // windowRestore
            // 
            this.windowRestore.BackColor = System.Drawing.Color.Transparent;
            this.windowRestore.Image = ((System.Drawing.Image)(resources.GetObject("windowRestore.Image")));
            this.windowRestore.Location = new System.Drawing.Point(1006, 3);
            this.windowRestore.Margin = new System.Windows.Forms.Padding(2);
            this.windowRestore.Name = "windowRestore";
            this.windowRestore.Size = new System.Drawing.Size(24, 24);
            this.windowRestore.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.windowRestore.TabIndex = 51;
            this.windowRestore.TabStop = false;
            this.windowRestore.Visible = false;
            this.windowRestore.Click += new System.EventHandler(this.MaximizeOrRestore);
            // 
            // windowClose
            // 
            this.windowClose.BackColor = System.Drawing.Color.Transparent;
            this.windowClose.Image = ((System.Drawing.Image)(resources.GetObject("windowClose.Image")));
            this.windowClose.Location = new System.Drawing.Point(1030, 3);
            this.windowClose.Margin = new System.Windows.Forms.Padding(2);
            this.windowClose.Name = "windowClose";
            this.windowClose.Size = new System.Drawing.Size(24, 24);
            this.windowClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.windowClose.TabIndex = 50;
            this.windowClose.TabStop = false;
            this.windowClose.Click += new System.EventHandler(this.Close);
            // 
            // windowMinimization
            // 
            this.windowMinimization.BackColor = System.Drawing.Color.Transparent;
            this.windowMinimization.Image = ((System.Drawing.Image)(resources.GetObject("windowMinimization.Image")));
            this.windowMinimization.Location = new System.Drawing.Point(982, 3);
            this.windowMinimization.Margin = new System.Windows.Forms.Padding(2);
            this.windowMinimization.Name = "windowMinimization";
            this.windowMinimization.Size = new System.Drawing.Size(24, 24);
            this.windowMinimization.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.windowMinimization.TabIndex = 49;
            this.windowMinimization.TabStop = false;
            this.windowMinimization.Click += new System.EventHandler(this.Minimization);
            // 
            // windowMaximize
            // 
            this.windowMaximize.BackColor = System.Drawing.Color.Transparent;
            this.windowMaximize.Image = ((System.Drawing.Image)(resources.GetObject("windowMaximize.Image")));
            this.windowMaximize.Location = new System.Drawing.Point(1006, 3);
            this.windowMaximize.Margin = new System.Windows.Forms.Padding(2);
            this.windowMaximize.Name = "windowMaximize";
            this.windowMaximize.Size = new System.Drawing.Size(24, 24);
            this.windowMaximize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.windowMaximize.TabIndex = 48;
            this.windowMaximize.TabStop = false;
            this.windowMaximize.Click += new System.EventHandler(this.MaximizeOrRestore);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label3.Location = new System.Drawing.Point(710, 172);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 30);
            this.label3.TabIndex = 42;
            this.label3.Text = "密码";
            // 
            // password
            // 
            this.password.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.password.Location = new System.Drawing.Point(715, 204);
            this.password.Margin = new System.Windows.Forms.Padding(2);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(170, 34);
            this.password.TabIndex = 41;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label2.Location = new System.Drawing.Point(476, 172);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 30);
            this.label2.TabIndex = 40;
            this.label2.Text = "地址";
            // 
            // address
            // 
            this.address.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.address.Location = new System.Drawing.Point(481, 204);
            this.address.Margin = new System.Windows.Forms.Padding(2);
            this.address.Name = "address";
            this.address.Size = new System.Drawing.Size(170, 34);
            this.address.TabIndex = 39;
            // 
            // Users
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Indigo;
            this.ClientSize = new System.Drawing.Size(1285, 884);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Users";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.Users_Load);
            this.SizeChanged+= new System.EventHandler(this.Users_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ExitBtn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ExitPic)).EndInit();
            this.AccountManagerBtn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AccountManagerPic)).EndInit();
            this.UsersBtn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UsersPic)).EndInit();
            this.BooksBtn.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.BooksPic)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UsersDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowRestore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowMinimization)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowMaximize)).EndInit();
            this.ResumeLayout(false);
        }

        private System.Windows.Forms.Label label9;

        private System.Windows.Forms.DataGridView UsersDataGridView;

        #endregion

        private System.Windows.Forms.PictureBox BooksPic;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.TextBox searchValue;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.PictureBox UsersPic;
        private System.Windows.Forms.Label UsersLabel;
        private System.Windows.Forms.Panel UsersBtn;
        private System.Windows.Forms.Panel BooksBtn;
        private System.Windows.Forms.Label BooksLabel;
        private System.Windows.Forms.PictureBox AccountManagerPic;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox phone;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.Label AccountManagerLabel;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox ExitPic;
        private System.Windows.Forms.Label ExitLabel;
        private System.Windows.Forms.Panel ExitBtn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel AccountManagerBtn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox address;
        private System.Windows.Forms.PictureBox windowRestore;
        private System.Windows.Forms.PictureBox windowClose;
        private System.Windows.Forms.PictureBox windowMinimization;
        private System.Windows.Forms.PictureBox windowMaximize;
    }
}