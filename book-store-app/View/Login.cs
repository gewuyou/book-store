﻿using System;
using System.Windows.Forms;
using book_store_app.Constant;
using book_store_app.Data;
using book_store_app.Entity;
using book_store_app.Enum;
using book_store_app.Model;
using book_store_app.Properties;
using book_store_app.View.Navigation;
using book_store_app.View.Utils;

namespace book_store_app.View
{
    /// <summary>
    /// 登录页面
    /// </summary>
    public partial class Login : Form
    {
        private readonly LoginViewModel _loginViewModel;

        private readonly DataContext _dataContext;

        private readonly ViewNavigation _viewNavigation;

        public Login()
        {
            // 启用双缓冲
            SetStyle(ControlStyles.DoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
            UpdateStyles();
            InitializeComponent();
            _dataContext = DataContext.GetInstance();
            _loginViewModel = LoginViewModel.GetInstance();
            _viewNavigation = ViewNavigation.GetInstance();
        }


        #region 窗口右上角按钮点击事件

        private void Close(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Minimization(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
        
        #endregion

        #region 按钮点击事件

        private void LoginBtnClick(object sender, EventArgs e)
        {
            // 检查用户名密码是否填写
            if (StringConstant.EmptyString.Equals(username.Text) ||
                StringConstant.EmptyString.Equals(password.Text))
            {
                ViewUtil.ConfirmDialogBoxIsDisplayedByError("错误", "用户名或密码不能为空!");
                return;
            }

            // 登录
            if (_loginViewModel.Login(username.Text, password.Text))
            {
                // 登录成功
                MessageBox.Show(Resources.Login_LoginBtnClick_login_successful);
                // todo 登录跳转
                // 判断当前登录的是管理员还是用户
                if (_dataContext.Get("loginAdmin") is Admin)
                {
                    _viewNavigation.Jump(ViewEnum.Books, this);
                    username.Text = StringConstant.EmptyString;
                    password.Text = StringConstant.EmptyString;
                    return;
                }

                if (!(_dataContext.Get("loginUser") is User))
                {
                    return;
                }
                _viewNavigation.Jump(ViewEnum.PersonalCenter, this);
                username.Text = StringConstant.EmptyString;
                password.Text = StringConstant.EmptyString;
            }
            else
            {
                MessageBox.Show(Resources.Login_LoginBtnClick_login_failed__wrong_username_or_password);
            }
        }

        private void RegisterLabelClick(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Register, this);
        }

        #endregion
        
    }
}