﻿using System;
using System.Windows.Forms;
using book_store_app.Enum;
using book_store_app.Model;
using book_store_app.Properties;
using book_store_app.View.Navigation;

namespace book_store_app.View
{
    /// <summary>
    /// 账户管理页面
    /// </summary>
    public partial class AccountManager : Form
    {
        private readonly AccountManagerViewModel _accountManagerViewModel;

        private readonly ViewNavigation _viewNavigation;
        
        private readonly AutoResizeForm _autoResizeForm = new AutoResizeForm();


        public AccountManager()
        {
            // 启用双缓冲
            SetStyle(ControlStyles.DoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
            UpdateStyles();
            InitializeComponent();
            _accountManagerViewModel = AccountManagerViewModel.GetInstance();
            _viewNavigation = ViewNavigation.GetInstance();
            FlushedData();
        }

        #region 窗口右上角按钮点击事件

        private void Close(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Minimization(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void MaximizeOrRestore(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                WindowState = FormWindowState.Maximized;
                windowMaximize.Visible = false;
                windowRestore.Visible = true;
            }
            else
            {
                WindowState = FormWindowState.Normal;
                windowMaximize.Visible = true;
                windowRestore.Visible = false;
            }
        }

        #endregion
        
        #region 刷新数据方法

        public void FlushedData()
        {
            bookCount.Text = _accountManagerViewModel.BookCount() + Resources.AccountManager_FlushedData_book;
            number.Text = _accountManagerViewModel.UserCount() + Resources.AccountManager_FlushedData_person;
            salesAmount.Text = _accountManagerViewModel.GetTotalSales() + Resources.AccountManager_FlushedData_yuan;
        }

        #endregion
        
        #region 跳转页面按钮点击事件

        private void BooksLabel_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Books, this);
        }

        private void UsersLabel_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Users, this);
        }

        private void AccountManagerLabel_Click(object sender, EventArgs e)
        {
            FlushedData();
        }

        private void ExitLabel_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Login, this);
        }

        private void BooksPic_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Books, this);
        }

        private void UsersPic_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Users, this);
        }

        private void AccountManagerPic_Click(object sender, EventArgs e)
        {
            FlushedData();
        }

        private void ExitPic_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Login, this);
        }

        #endregion
        
        #region 窗口事件

        private void AccountManager_Load(object sender, EventArgs e)
        {
            _autoResizeForm.ControlInitializeSize(this);
        }

        private void AccountManager_SizeChanged(object sender, EventArgs e)
        {
            _autoResizeForm.ControlAutoSize(this);
        }
        
        #endregion
    }
}