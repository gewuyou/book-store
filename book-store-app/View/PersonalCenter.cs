﻿using System;
using System.Windows.Forms;
using book_store_app.Constant;
using book_store_app.Data;
using book_store_app.Entity;
using book_store_app.Enum;
using book_store_app.Model;
using book_store_app.Properties;
using book_store_app.View.Navigation;
using book_store_app.View.Utils;
using static System.Decimal;

namespace book_store_app.View
{
    public partial class PersonalCenter : Form
    {
        private readonly DataContext _dataContext;

        private readonly PersonalCenterViewModel _personalCenterViewModel;

        private readonly ViewNavigation _viewNavigation;
        
        private readonly AutoResizeForm _autoResizeForm = new AutoResizeForm();

        public PersonalCenter()
        {
            // 启用双缓冲
            SetStyle(ControlStyles.DoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
            UpdateStyles();
            InitializeComponent();
            _dataContext = DataContext.GetInstance();
            _personalCenterViewModel = PersonalCenterViewModel.GetInstance();
            _viewNavigation = ViewNavigation.GetInstance();
            FlushedData();
        }
        #region 窗口右上角点击事件

        private void Close(object sender, EventArgs e)
        {
            // 退出
            Application.Exit();
        }
        
        private void Minimization(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
        
        private void MaximizeOrRestore(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                WindowState = FormWindowState.Maximized;
                windowMaximize.Visible = false;
                windowRestore.Visible = true;
            }
            else
            {
                WindowState = FormWindowState.Normal;
                windowMaximize.Visible = true;
                windowRestore.Visible = false;
            }
        }

        #endregion

        #region 刷新数据方法

        /// <summary>
        /// 刷新用户数据 
        /// </summary>
        public void FlushedData()
        {
            if (!(_dataContext.Get("loginUser") is User user))
            {
                return;
            }

            username.Text = user.Username;
            phone.Text = user.Phone;
            address.Text = user.Address;
            accountBalance.Text = user.Balance + Resources.AccountManager_FlushedData_yuan;
        }

        #endregion

        #region 按钮点击事件

        /// <summary>
        /// 保存编辑按钮
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">事件参数</param>
        private void SaveEditBtnClick(object sender, EventArgs e)
        {
            if (StringConstant.EmptyString.Equals(address.Text) ||
                StringConstant.EmptyString.Equals(phone.Text))
            {
                ViewUtil.ConfirmDialogBoxIsDisplayedByError("错误", Resources.message_is_null);
                return;
            }

            if (!(_dataContext.Get("loginUser") is User user))
            {
                return;
            }

            var addressBak = user.Address;
            var phoneBak = user.Phone;
            user.Address = address.Text;
            user.Phone = phone.Text;
            if (_personalCenterViewModel.Update(user) > 0)
            {
                _dataContext.Add("loginUser", user);
                FlushedData();
            }
            else
            {
                ViewUtil.ConfirmDialogBoxIsDisplayedByError("错误", Resources.message_save_fail);
                user.Address = addressBak;
                user.Phone = phoneBak;
            }
        }

        /// <summary>
        /// 重置密码按钮 
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">事件参数</param>
        private void ResetPasswordBtnClick(object sender, EventArgs e)
        {
            if (StringConstant.EmptyString.Equals(ResetPassword.Text))
            {
                ViewUtil.ConfirmDialogBoxIsDisplayedByError("错误", "重置的密码不能为空!");
                return;
            }

            if (ViewUtil.ConfirmDialogBoxIsDisplayedByQuestion("重置密码", "确定要重置密码吗?\n重置后将重新登录!"))
            {
                return;
            }

            if (!(_dataContext.Get("loginUser") is User user))
            {
                return;
            }

            user.Password = ResetPassword.Text;
            if (_personalCenterViewModel.Update(user) > 0)
            {
                ViewUtil.ConfirmDialogBoxIsDisplayedByInfo("重置密码", "重置密码成功!\n将重新登录!");
                _viewNavigation.Jump(ViewEnum.Login,this);
                ResetPassword.Text = StringConstant.EmptyString;
            }
            else
            {
                ViewUtil.ConfirmDialogBoxIsDisplayedByError("错误", "重置密码失败! 请联系管理员!");
            }
        }

        /// <summary>
        /// 充值按钮 
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">事件参数</param>
        private void RechargeBtnClick(object sender, EventArgs e)
        {
            if (StringConstant.EmptyString.Equals(Recharge.Text))
            {
                ViewUtil.ConfirmDialogBoxIsDisplayedByError("错误", Resources.message_is_null);
                return;
            }

            if (!(_dataContext.Get("loginUser") is User user))
            {
                return;
            }

            try
            {
                user.Balance = Add(user.Balance, Parse(Recharge.Text));
                if (_personalCenterViewModel.Update(user) > 0)
                {
                    ViewUtil.ConfirmDialogBoxIsDisplayedByInfo("充值", "充值成功!");
                    FlushedData();
                    Recharge.Text = StringConstant.EmptyString;
                }
                else
                {
                    ViewUtil.ConfirmDialogBoxIsDisplayedByError("错误", "充值失败!");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                ViewUtil.ConfirmDialogBoxIsDisplayedByError("错误", "输入非法，请输入数字!");
            }
        }
        #endregion

        #region 跳转页面按钮点击事件

        private void PersonalCenterPic_Click(object sender, EventArgs e)
        {
            FlushedData();
        }

        private void PersonalCenterLabel_Click(object sender, EventArgs e)
        {
            FlushedData();
        }

        private void ShoppingCartLabel_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.ShoppingCart, this);
        }

        private void ShoppingCartPic_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.ShoppingCart, this);
        }

        private void OrdersLabel_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Orders, this);
        }

        private void OrdersPic_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Orders, this);
        }

        private void ExitLabel_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Login, this);
        }

        private void ExitPic_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Login, this);
        }

        #endregion
        
        #region 窗口事件

        private void PersonalCenter_Load(object sender, EventArgs e)
        {
            _autoResizeForm.ControlInitializeSize(this);
        }

        private void PersonalCenter_SizeChanged(object sender, EventArgs e)
        {
            _autoResizeForm.ControlAutoSize(this);
        }

        #endregion
    }
}