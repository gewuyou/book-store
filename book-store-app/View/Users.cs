﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using book_store_app.Constant;
using book_store_app.Entity;
using book_store_app.Enum;
using book_store_app.Model;
using book_store_app.Properties;
using book_store_app.View.Navigation;
using book_store_app.View.Utils;

namespace book_store_app.View
{
    /// <summary>
    /// 用户管理
    /// </summary>
    public partial class Users : Form
    {
        private readonly UserViewModel _userViewModel;

        private readonly ViewNavigation _viewNavigation;
        
        private readonly AutoResizeForm _autoResizeForm = new AutoResizeForm();

        public Users()
        {
            // 启用双缓冲
            SetStyle(ControlStyles.DoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
            UpdateStyles();
            InitializeComponent();
            _userViewModel = UserViewModel.GetInstance();
            _viewNavigation = ViewNavigation.GetInstance();
            InitUserList();
        }

        #region 窗口右上角点击事件

        private void Close(object sender, EventArgs e)
        {
            // 退出
            Application.Exit();
        }

        private void Minimization(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void MaximizeOrRestore(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                WindowState = FormWindowState.Maximized;
                windowMaximize.Visible = false;
                windowRestore.Visible = true;
            }
            else
            {
                WindowState = FormWindowState.Normal;
                windowMaximize.Visible = true;
                windowRestore.Visible = false;
            }
        }

        #endregion

        #region 初始化书籍列表

        private void InitUserList()
        {
            FlushedData();
            var saveEditBtn = new DataGridViewButtonColumn
            {
                Name = "saveEditBtn",
                HeaderText = "",
                Text = "保存编辑",
                UseColumnTextForButtonValue = true
            };
            saveEditBtn.FlatStyle = FlatStyle.Flat;
            saveEditBtn.DefaultCellStyle.ForeColor = Color.ForestGreen;
            var deleteBtn = new DataGridViewButtonColumn
            {
                Name = "deleteBtn",
                HeaderText = "",
                Text = Resources.delete,
                UseColumnTextForButtonValue = true
            };
            deleteBtn.FlatStyle = FlatStyle.Flat;
            deleteBtn.DefaultCellStyle.ForeColor = Color.OrangeRed;
            UsersDataGridView.Columns[0].ReadOnly = true;
            UsersDataGridView.Columns.Add(saveEditBtn);
            UsersDataGridView.Columns.Add(deleteBtn);
            UsersDataGridView.CellContentClick += SaveEditBtnClick;
            UsersDataGridView.CellContentClick += DeleteBtnClick;
        }

        #endregion

        #region 刷新数据方法

        /// <summary>
        /// 刷新用户数据
        /// </summary>
        public void FlushedData()
        {
            FlushedUsers(_userViewModel.GetUsers());
        }

        /// <summary>
        /// 刷新用户列表
        /// </summary>
        /// <param name="users">新的用户集合</param>
        private void FlushedUsers(List<User> users)
        {
            var dataTable = new DataTable();
            dataTable.Columns.Add("用户ID", typeof(int));
            dataTable.Columns.Add("用户名", typeof(string));
            dataTable.Columns.Add("密码", typeof(string));
            dataTable.Columns.Add("用户地址", typeof(string));
            dataTable.Columns.Add("电话号码", typeof(string));
            foreach (var user in users)
            {
                dataTable.Rows.Add(user.Id, user.Username, user.Password, user.Address, user.Phone);
            }

            // 将数据源绑定到 DataGridView
            UsersDataGridView.DataSource = dataTable;
        }

        #endregion

        #region 单元格点击事件

        /// <summary>
        /// 删除按钮点击事件
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">数据网格视图单元格事件参数</param>
        private void DeleteBtnClick(object sender, DataGridViewCellEventArgs e)
        {
            if (ViewUtil.DataGridViewColumnIsNull(UsersDataGridView, e, "deleteBtn"))
            {
                return;
            }

            var result = MessageBox.Show(Resources.tips_action_text, Resources.delete, MessageBoxButtons.OKCancel);

            if (result == DialogResult.Cancel)
            {
                return;
            }

            var row = UsersDataGridView.Rows[e.RowIndex];
            var id = Convert.ToInt32(row.Cells[2].Value);
            if (_userViewModel.Delete(id) > 0)
            {
                MessageBox.Show(Resources.message_delete_success);
                if (searchValue.Text != StringConstant.EmptyString)
                {
                    FlushedUsers(_userViewModel.Search(searchValue.Text));
                }
                else
                {
                    FlushedData();
                }
            }
            else
            {
                MessageBox.Show(Resources.message_delete_fail);
            }
        }

        /// <summary>
        /// 保存编辑按钮点击事件
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">数据网格视图单元格事件参数</param>
        private void SaveEditBtnClick(object sender, DataGridViewCellEventArgs e)
        {
            if (ViewUtil.DataGridViewColumnIsNull(UsersDataGridView, e, "saveEditBtn"))
            {
                return;
            }

            var row = UsersDataGridView.Rows[e.RowIndex];
            var user = new User
            {
                Id = Convert.ToInt32(row.Cells[2].Value),
                Username = row.Cells[3].Value.ToString(),
                Password = row.Cells[4].Value.ToString(),
                Address = row.Cells[5].Value.ToString(),
                Phone = row.Cells[6].Value.ToString(),
            };
            if (_userViewModel.Save(user) > 0)
            {
                MessageBox.Show(Resources.message_save_success);
                FlushedData();
            }
            else
            {
                MessageBox.Show(Resources.message_save_fail);
            }
        }

        #endregion

        #region 按钮点击事件

        /// <summary>
        /// 保存按钮点击事件
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">事件参数</param>
        private void SaveBtnClick(object sender, EventArgs e)
        {
            if (StringConstant.EmptyString.Equals(username.Text) ||
                StringConstant.EmptyString.Equals(password.Text) ||
                StringConstant.EmptyString.Equals(phone.Text) ||
                StringConstant.EmptyString.Equals(address.Text))
            {
                ViewUtil.ConfirmDialogBoxIsDisplayedByError("错误", Resources.message_is_null);
                return;
            }

            var user = new User
            {
                Username = username.Text,
                Password = password.Text,
                Phone = phone.Text,
                Address = address.Text,
                Balance = 0
            };
            if (_userViewModel.Save(user) > 0)
            {
                // 清空数据
                username.Text = StringConstant.EmptyString;
                password.Text = StringConstant.EmptyString;
                phone.Text = StringConstant.EmptyString;
                address.Text = StringConstant.EmptyString;
                MessageBox.Show(Resources.message_save_success);
                FlushedData();
            }
            else
            {
                MessageBox.Show(Resources.message_save_fail);
            }
        }


        /// <summary>
        /// 搜索按钮点击事件
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">事件参数</param>
        private void SearchBtnClick(object sender, EventArgs e)
        {
            FlushedUsers(_userViewModel.Search(searchValue.Text));
        }

        #endregion

        #region 跳转页面按钮点击事件

        private void BooksLabel_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Books, this);
        }

        private void BooksPic_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Books, this);
        }

        private void UsersLabel_Click(object sender, EventArgs e)
        {
            FlushedData();
        }

        private void UsersPic_Click(object sender, EventArgs e)
        {
            FlushedData();
        }

        private void AccountManagerLabel_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.AccountManager, this);
        }

        private void AccountManagerPic_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.AccountManager, this);
        }

        private void ExitLabel_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Login, this);
        }

        private void ExitPic_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Login, this);
        }

        #endregion

        #region 窗口事件

        private void Users_Load(object sender, EventArgs e)
        {
            _autoResizeForm.ControlInitializeSize(this);
        }

        private void Users_SizeChanged(object sender, EventArgs e)
        {
            _autoResizeForm.ControlAutoSize(this);
        }

        #endregion
    }
}