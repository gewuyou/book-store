﻿using System.ComponentModel;

namespace book_store_app.View
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.panel1 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.windowClose = new System.Windows.Forms.PictureBox();
            this.windowMinimization = new System.Windows.Forms.PictureBox();
            this.registerLabel = new System.Windows.Forms.Label();
            this.loginBtn = new System.Windows.Forms.Button();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.password = new System.Windows.Forms.TextBox();
            this.username = new System.Windows.Forms.TextBox();
            this.appIcon = new System.Windows.Forms.PictureBox();
            this.Title = new System.Windows.Forms.Label();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.windowClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowMinimization)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.appIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.BackColor = System.Drawing.SystemColors.WindowText;
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Location = new System.Drawing.Point(6, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(205, 319);
            this.panel1.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("霞鹜文楷等宽", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(41, 195);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 29);
            this.label6.TabIndex = 11;
            this.label6.Text = "购物无忧";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("霞鹜文楷等宽", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(41, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 29);
            this.label5.TabIndex = 10;
            this.label5.Text = "次日送达";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("霞鹜文楷等宽", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(41, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 29);
            this.label4.TabIndex = 9;
            this.label4.Text = "正品保障";
            // 
            // panel2
            // 
            this.panel2.AutoSize = true;
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.windowClose);
            this.panel2.Controls.Add(this.windowMinimization);
            this.panel2.Controls.Add(this.registerLabel);
            this.panel2.Controls.Add(this.loginBtn);
            this.panel2.Controls.Add(this.passwordLabel);
            this.panel2.Controls.Add(this.usernameLabel);
            this.panel2.Controls.Add(this.password);
            this.panel2.Controls.Add(this.username);
            this.panel2.Controls.Add(this.appIcon);
            this.panel2.Controls.Add(this.Title);
            this.panel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel2.Location = new System.Drawing.Point(208, 7);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(307, 319);
            this.panel2.TabIndex = 1;
            // 
            // windowClose
            // 
            this.windowClose.BackColor = System.Drawing.Color.Transparent;
            this.windowClose.Image = ((System.Drawing.Image)(resources.GetObject("windowClose.Image")));
            this.windowClose.Location = new System.Drawing.Point(278, 6);
            this.windowClose.Name = "windowClose";
            this.windowClose.Size = new System.Drawing.Size(24, 24);
            this.windowClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.windowClose.TabIndex = 13;
            this.windowClose.TabStop = false;
            this.windowClose.Click += new System.EventHandler(this.Close);
            // 
            // windowMinimization
            // 
            this.windowMinimization.BackColor = System.Drawing.Color.Transparent;
            this.windowMinimization.Image = ((System.Drawing.Image)(resources.GetObject("windowMinimization.Image")));
            this.windowMinimization.Location = new System.Drawing.Point(248, 6);
            this.windowMinimization.Name = "windowMinimization";
            this.windowMinimization.Size = new System.Drawing.Size(24, 24);
            this.windowMinimization.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.windowMinimization.TabIndex = 12;
            this.windowMinimization.TabStop = false;
            this.windowMinimization.Click += new System.EventHandler(this.Minimization);
            // 
            // registerLabel
            // 
            this.registerLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.registerLabel.AutoSize = true;
            this.registerLabel.Font = new System.Drawing.Font("霞鹜文楷", 9F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.registerLabel.ForeColor = System.Drawing.SystemColors.GrayText;
            this.registerLabel.Location = new System.Drawing.Point(122, 275);
            this.registerLabel.Name = "registerLabel";
            this.registerLabel.Size = new System.Drawing.Size(39, 19);
            this.registerLabel.TabIndex = 8;
            this.registerLabel.Text = "注册";
            this.registerLabel.Click += new System.EventHandler(this.RegisterLabelClick);
            // 
            // loginBtn
            // 
            this.loginBtn.AutoSize = true;
            this.loginBtn.FlatAppearance.BorderColor = System.Drawing.Color.ForestGreen;
            this.loginBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.loginBtn.Font = new System.Drawing.Font("霞鹜文楷", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.loginBtn.ForeColor = System.Drawing.Color.ForestGreen;
            this.loginBtn.Location = new System.Drawing.Point(74, 235);
            this.loginBtn.Name = "loginBtn";
            this.loginBtn.Size = new System.Drawing.Size(137, 31);
            this.loginBtn.TabIndex = 7;
            this.loginBtn.Text = "登录";
            this.loginBtn.UseVisualStyleBackColor = true;
            this.loginBtn.Click += new System.EventHandler(this.LoginBtnClick);
            // 
            // passwordLabel
            // 
            this.passwordLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.passwordLabel.AutoSize = true;
            this.passwordLabel.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.passwordLabel.ForeColor = System.Drawing.SystemColors.GrayText;
            this.passwordLabel.Location = new System.Drawing.Point(43, 181);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(52, 27);
            this.passwordLabel.TabIndex = 6;
            this.passwordLabel.Text = "密码";
            // 
            // usernameLabel
            // 
            this.usernameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.usernameLabel.AutoSize = true;
            this.usernameLabel.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.usernameLabel.ForeColor = System.Drawing.SystemColors.GrayText;
            this.usernameLabel.Location = new System.Drawing.Point(32, 130);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(72, 27);
            this.usernameLabel.TabIndex = 5;
            this.usernameLabel.Text = "用户名";
            // 
            // password
            // 
            this.password.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.password.Location = new System.Drawing.Point(127, 178);
            this.password.Name = "password";
            this.password.PasswordChar = '*';
            this.password.Size = new System.Drawing.Size(159, 34);
            this.password.TabIndex = 4;
            // 
            // username
            // 
            this.username.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.username.Location = new System.Drawing.Point(127, 127);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(159, 34);
            this.username.TabIndex = 3;
            // 
            // appIcon
            // 
            this.appIcon.Image = ((System.Drawing.Image)(resources.GetObject("appIcon.Image")));
            this.appIcon.Location = new System.Drawing.Point(121, 60);
            this.appIcon.Name = "appIcon";
            this.appIcon.Size = new System.Drawing.Size(61, 37);
            this.appIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.appIcon.TabIndex = 2;
            this.appIcon.TabStop = false;
            // 
            // Title
            // 
            this.Title.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("霞鹜文楷等宽", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Title.ForeColor = System.Drawing.SystemColors.GrayText;
            this.Title.Location = new System.Drawing.Point(125, 24);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(60, 28);
            this.Title.TabIndex = 1;
            this.Title.Text = "登录";
            // 
            // bindingSource1
            // 
            this.bindingSource1.RaiseListChangedEvents = false;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Indigo;
            this.ClientSize = new System.Drawing.Size(520, 330);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(15, 15);
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.windowClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowMinimization)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.appIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.PictureBox windowMinimization;
        private System.Windows.Forms.PictureBox windowClose;

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;

        private System.Windows.Forms.Label label4;

        private System.Windows.Forms.Label registerLabel;

        private System.Windows.Forms.BindingSource bindingSource1;

        private System.Windows.Forms.Button loginBtn;

        private System.Windows.Forms.Label passwordLabel;

        private System.Windows.Forms.Label usernameLabel;

        private System.Windows.Forms.TextBox password;

        private System.Windows.Forms.TextBox username;

        private System.Windows.Forms.PictureBox appIcon;

        private System.Windows.Forms.Label Title;

        private System.Windows.Forms.Panel panel2;

        private System.Windows.Forms.Panel panel1;

        #endregion
    }
}