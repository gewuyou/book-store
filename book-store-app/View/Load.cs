﻿using System;
using System.Windows.Forms;
using book_store_app.Enum;
using book_store_app.View.Navigation;

namespace book_store_app.View
{
    /// <summary>
    /// 加载页面
    /// </summary>
    public partial class Load : Form
    {
        private readonly ViewNavigation _viewNavigation;

        public Load()
        {
            // 启用双缓冲
            SetStyle(ControlStyles.DoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
            UpdateStyles();
            InitializeComponent();
            _viewNavigation = ViewNavigation.GetInstance();
        }

        private void LoadTimerTick(object sender, EventArgs e)
        {
            _viewNavigation.Init(LoadingProgressBar,loadTimer);
            _viewNavigation.Jump(ViewEnum.Login,this);
        }

        private void Load_Load(object sender, EventArgs e)
        {
            loadTimer.Start();
        }
    }
}