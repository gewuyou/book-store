﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using book_store_app.Constant;
using book_store_app.Data;
using book_store_app.Entity;
using book_store_app.Enum;
using book_store_app.Model;
using book_store_app.Properties;
using book_store_app.View.Navigation;
using book_store_app.View.Utils;

namespace book_store_app.View
{
    public partial class Orders : Form
    {
        private readonly OrderViewModel _orderViewModel;

        private readonly DataContext _dataContext;

        private readonly ViewNavigation _viewNavigation;
        
        private readonly AutoResizeForm _autoResizeForm = new AutoResizeForm();

        private Book _book;

        public Orders()
        {
            // 启用双缓冲
            SetStyle(ControlStyles.DoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
            UpdateStyles();
            InitializeComponent();
            _orderViewModel = OrderViewModel.GetInstance();
            _dataContext = DataContext.GetInstance();
            _dataContext.Add("bookIdAndCount", new Dictionary<int, int>());
            _viewNavigation = ViewNavigation.GetInstance();
            InitBookList();
        }

        #region 窗口右上角点击事件

        private void Close(object sender, EventArgs e)
        {
            // 退出
            Application.Exit();
        }

        private void Minimization(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void MaximizeOrRestore(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                WindowState = FormWindowState.Maximized;
                windowMaximize.Visible = false;
                windowRestore.Visible = true;
            }
            else
            {
                WindowState = FormWindowState.Normal;
                windowMaximize.Visible = true;
                windowRestore.Visible = false;
            }
        }

        #endregion

        #region 初始化书籍列表

        /// <summary>
        /// 初始化书籍列表
        /// </summary>
        private void InitBookList()
        {
            FlushedData();

            // 设置书籍列表为只读
            BooksDataGridView.Columns[0].ReadOnly = true;
            BooksDataGridView.Columns[1].ReadOnly = true;
            BooksDataGridView.Columns[2].ReadOnly = true;
            BooksDataGridView.Columns[3].ReadOnly = true;
            BooksDataGridView.Columns[4].ReadOnly = true;
            BooksDataGridView.Columns[5].ReadOnly = true;
            BooksDataGridView.CellContentClick += CellContentClick;
        }

        #endregion

        #region 刷新数据方法

        public void FlushedData()
        {
            ViewUtil.FlushedBooks(BooksDataGridView, _orderViewModel.GetBooks());
        }

        #endregion

        #region 单元格点击事件

        /// <summary>
        /// 添加单元格点击事件
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">数据网格视图单元格事件参数</param>
        private void CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0 || e.ColumnIndex < 0)
            {
                return;
            }

            // 获取当前选中的Book对象
            var row = BooksDataGridView.Rows[e.RowIndex];
            _book = new Book
            {
                Id = Convert.ToInt32(row.Cells[0].Value),
                Name = row.Cells[1].Value.ToString(),
                Author = row.Cells[2].Value.ToString(),
                Type = row.Cells[3].Value.ToString(),
                Count = Convert.ToInt32(row.Cells[4].Value),
                Price = Convert.ToDecimal(row.Cells[5].Value)
            };
            name.Text = _book.Name;
            author.Text = _book.Author;
            price.Text = _book.Price.ToString(CultureInfo.CurrentCulture);
        }

        #endregion
        #region 按钮点击事件

        private void SearchBtnClick(object sender, EventArgs e)
        {
            ViewUtil.FlushedBooks(BooksDataGridView, _orderViewModel.Search(searchValue.Text));
        }


        private void AddToCartBtnClick(object sender, EventArgs e)
        {
            var countText = count.Text;
            if (StringConstant.EmptyString.Equals(name.Text) ||
                StringConstant.EmptyString.Equals(author.Text) ||
                StringConstant.EmptyString.Equals(price.Text) ||
                StringConstant.EmptyString.Equals(countText))
            {
                ViewUtil.ConfirmDialogBoxIsDisplayedByError("错误",Resources.message_is_null);
                return;
            }
            
            try
            {
                var buyCount = Convert.ToInt32(countText);
                if (buyCount > _book.Count)
                {
                    ViewUtil.ConfirmDialogBoxIsDisplayedByWarning(Resources.Orders_AddToCartBtnClick_Warning,Resources.Orders_AddToCartBtnClick_purchase_quantity_cannot_be_greater_inventory_);
                    return;
                }

                var bookIdAndCount = _dataContext.Get("bookIdAndCount") as Dictionary<int, int>;
                bookIdAndCount?.Add(_book.Id, _book.Count);
                // 添加到购物车
                var books = _dataContext.Get("shoppingCartList") as List<Book>;
                _book.Count = buyCount;
                books?.Add(_book);
                MessageBox.Show( Resources.Orders_AddToCartBtnClick_added_successful_);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                ViewUtil.ConfirmDialogBoxIsDisplayedByError("错误", exception.Message);
            }
        }

        #endregion

        #region 跳转页面按钮点击事件

        private void PersonalCenterLabel_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.PersonalCenter, this);
        }


        private void PersonalCenterPic_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.PersonalCenter, this);
        }

        private void OrdersLabel_Click(object sender, EventArgs e)
        {
            FlushedData();
        }

        private void OrdersPic_Click(object sender, EventArgs e)
        {
            FlushedData();
        }

        private void ShoppingCartLabel_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.ShoppingCart, this);
        }

        private void ShoppingCartPic_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.ShoppingCart, this);
        }

        private void ExitLabel_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Login, this);
        }

        private void ExitPic_Click(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Login, this);
        }

        #endregion

        #region 窗口事件

        private void Orders_Load(object sender, EventArgs e)
        {
            _autoResizeForm.ControlInitializeSize(this);
        }

        private void Orders_SizeChanged(object sender, EventArgs e)
        {
            _autoResizeForm.ControlAutoSize(this);
        }

        #endregion
    }
}