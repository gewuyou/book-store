﻿using System;
using System.Windows.Forms;
using book_store_app.Constant;
using book_store_app.Enum;
using book_store_app.Model;
using book_store_app.View.Navigation;
using book_store_app.View.Utils;

namespace book_store_app.View
{
    public partial class Register : Form
    {
        private readonly RegisterViewModel _registerViewModel;

        private readonly ViewNavigation _viewNavigation;

        public Register()
        {
            // 启用双缓冲
            SetStyle(ControlStyles.DoubleBuffer | ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
            UpdateStyles();
            InitializeComponent();
            _registerViewModel = RegisterViewModel.GetInstance();
            _viewNavigation = ViewNavigation.GetInstance();
        }

        #region 窗口右上角点击事件

        private void Close(object sender, EventArgs e)
        {
            // 退出
            Application.Exit();
        }

        private void Minimization(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        #endregion

        #region 按钮点击事件

        private void RegisterBtnClick(object sender, EventArgs e)
        {
            if (StringConstant.EmptyString.Equals(username.Text) ||
                StringConstant.EmptyString.Equals(password.Text))
            {
                ViewUtil.ConfirmDialogBoxIsDisplayedByError("错误", "用户名或密码不能为空!");
                return;
            }

            if (_registerViewModel.Register(username.Text, password.Text))
            {
                ViewUtil.ConfirmDialogBoxIsDisplayedByInfo("成功", "注册成功!");
                _viewNavigation.Jump(ViewEnum.Login, this);
            }
            else
            {
                ViewUtil.ConfirmDialogBoxIsDisplayedByWarning("失败", "该用户名已被注册!");
            }
        }

        #endregion

        #region 跳转页面按钮点击事件

        private void LoginLabelClick(object sender, EventArgs e)
        {
            _viewNavigation.Jump(ViewEnum.Login, this);
        }

        #endregion
    }
}