﻿using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using book_store_app.Entity;

namespace book_store_app.View.Utils
{
    /// <summary>
    /// 视图工具类
    /// </summary>
    public static class ViewUtil
    {
        /// <summary>
        /// 判断数据网格视图列是否为null
        /// </summary>
        /// <param name="dataGridView">数据网格视图</param>
        /// <param name="e">数据网格视图单元格事件参数</param>
        /// <param name="columnName">列名</param>
        /// <returns>为null返回true</returns>
        public static bool DataGridViewColumnIsNull(DataGridView dataGridView, DataGridViewCellEventArgs e,
            string columnName)
        {
            var column = dataGridView.Columns[columnName];
            if (column == null)
            {
                return true;
            }

            return e.ColumnIndex != column.Index || e.RowIndex < 0;
        }

        /// <summary>
        /// 刷新书籍列表
        /// </summary>
        /// <param name="dataGridView">数据网格视图</param>
        /// <param name="books">新的书籍列表数据</param>
        public static void FlushedBooks(DataGridView dataGridView, List<Book> books)
        {
            var dataTable = new DataTable();
            dataTable.Columns.Add("书籍ID", typeof(int));
            dataTable.Columns.Add("书籍名称", typeof(string));
            dataTable.Columns.Add("书籍作者", typeof(string));
            dataTable.Columns.Add("书籍类型", typeof(string));
            dataTable.Columns.Add("书籍数量", typeof(int));
            dataTable.Columns.Add("书籍价格", typeof(decimal));
            foreach (var book in books)
            {
                dataTable.Rows.Add(book.Id, book.Name, book.Author, book.Type, book.Count, book.Price);
            }

            // 将数据源绑定到 DataGridView
            dataGridView.DataSource = dataTable;
        }

        /// <summary>
        /// 显示确认对话框
        /// </summary>
        /// <param name="headText">标题文本</param>
        /// <param name="contentText">内容文本</param>
        /// <returns>点击取消返回true反之false</returns>
        public static bool ConfirmDialogBoxIsDisplayedByQuestion(string headText, string contentText)
        {
            var result = MessageBox.Show(contentText, headText, MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

            return result == DialogResult.Cancel;
        }

        /// <summary>
        /// 显示确认对话框
        /// </summary>
        /// <param name="headText">标题文本</param>
        /// <param name="contentText">内容文本</param>
        /// <returns>点击取消返回true反之false</returns>
        public static void ConfirmDialogBoxIsDisplayedByInfo(string headText, string contentText)
        {
             MessageBox.Show(contentText, headText, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// 显示确认对话框
        /// </summary>
        /// <param name="headText">标题文本</param>
        /// <param name="contentText">内容文本</param>
        /// <returns>点击取消返回true反之false</returns>
        public static void ConfirmDialogBoxIsDisplayedByError(string headText, string contentText)
        {
            MessageBox.Show(contentText, headText, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        /// <summary>
        /// 显示确认对话框
        /// </summary>
        /// <param name="headText">标题文本</param>
        /// <param name="contentText">内容文本</param>
        /// <returns>点击取消返回true反之false</returns>
        public static void ConfirmDialogBoxIsDisplayedByWarning(string headText, string contentText)
        {
            MessageBox.Show(contentText, headText, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        
    }
}