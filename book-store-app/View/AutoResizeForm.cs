﻿using System.Collections.Generic;
using System.Windows.Forms;
using book_store_app.Entity;

namespace book_store_app.View
{
    /// <summary>
    /// 自动调整表单大小
    /// </summary>
    public class AutoResizeForm
    {
        //(2).声明 1个对象
        //注意这里不能使用控件列表记录 List nCtrl;，因为控件的关联性，记录的始终是当前的大小。
        //      public List oldCtrl= new List();//这里将西文的大于小于号都过滤掉了，只能改为中文的，使用中要改回西文
        private readonly List<ControlRect> _oldCtrl = new List<ControlRect>();

        //1;
        private int _controlNo;

        //(3). 创建两个函数
        //(3.1)记录窗体和其控件的初始位置和大小,
        public void ControlInitializeSize(Control mForm)
        {
            var cR = new ControlRect
            {
                Left = mForm.Left,
                Top = mForm.Top,
                Width = mForm.Width,
                Height = mForm.Height
            };
            //第一个为"窗体本身",只加入一次即可
            _oldCtrl.Add(cR);
            //窗体内其余控件还可能嵌套控件(比如panel),要单独抽出,因为要递归调用
            AddControl(mForm);
            //this.WindowState = (System.Windows.Forms.FormWindowState)(2);//记录完控件的初始位置和大小后，再最大化
            //0 - Normalize , 1 - Minimize,2- Maximize
        }

        private void AddControl(Control ctl)
        {
            foreach (Control c in ctl.Controls)
            {
                //**放在这里，是先记录控件的子控件，后记录控件本身
                //if (c.Controls.Count > 0)
                //    AddControl(c);//窗体内其余控件还可能嵌套控件(比如panel),要单独抽出,因为要递归调用
                var objCtrl = new ControlRect
                {
                    Left = c.Left,
                    Top = c.Top,
                    Width = c.Width,
                    Height = c.Height
                };
                _oldCtrl.Add(objCtrl);
                //**放在这里，是先记录控件本身，后记录控件的子控件
                if (c.Controls.Count > 0)
                {
                    //窗体内其余控件还可能嵌套控件(比如panel),要单独抽出,因为要递归调用
                    AddControl(c);
                }
            }
        }

        //(3.2)控件自适应大小,
        public void ControlAutoSize(Control mForm)
        {
            if (_controlNo == 0)
            {
                //*如果在窗体的Form1_Load中，记录控件原始的大小和位置，正常没有问题，但要加入皮肤就会出现问题，因为有些控件如dataGridView的的子控件还没有完成，个数少
                //*要在窗体的Form1_SizeChanged中，第一次改变大小时，记录控件原始的大小和位置,这里所有控件的子控件都已经形成
                //  cR.Left = mForm.Left; cR.Top = mForm.Top; cR.Width = mForm.Width; cR.Height = mForm.Height;
                var cR = new ControlRect
                {
                    Left = 0,
                    Top = 0,
                    Width = mForm.PreferredSize.Width,
                    Height = mForm.PreferredSize.Height
                };
                //第一个为"窗体本身",只加入一次即可
                _oldCtrl.Add(cR);
                //窗体内其余控件可能嵌套其它控件(比如panel),故单独抽出以便递归调用
                AddControl(mForm);
            }

            //新旧窗体之间的比例，与最早的旧窗体
            var wScale = mForm.Width / (float)_oldCtrl[0].Width;
            //.Height;
            var hScale = mForm.Height / (float)_oldCtrl[0].Height;
            //进入=1，第0个为窗体本身,窗体内的控件,从序号1开始
            _controlNo = 1;
            //窗体内其余控件还可能嵌套控件(比如panel),要单独抽出,因为要递归调用
            AutoScaleControl(mForm, wScale, hScale);
        }

        private void AutoScaleControl(Control ctl, float wScale, float hScale)
        {
            //int ctrlNo = 1;//第1个是窗体自身的 Left,Top,Width,Height，所以窗体控件从ctrlNo=1开始
            foreach (Control c in ctl.Controls)
            {
                //**放在这里，是先缩放控件的子控件，后缩放控件本身
                //if (c.Controls.Count > 0)
                //   AutoScaleControl(c, wScale, hScale);//窗体内其余控件还可能嵌套控件(比如panel),要单独抽出,因为要递归调用
                var ctrLeft0 = _oldCtrl[_controlNo].Left;
                var ctrTop0 = _oldCtrl[_controlNo].Top;
                var ctrWidth0 = _oldCtrl[_controlNo].Width;
                var ctrHeight0 = _oldCtrl[_controlNo].Height;
                //c.Left = (int)((ctrLeft0 - wLeft0) * wScale) + wLeft1;//新旧控件之间的线性比例
                //c.Top = (int)((ctrTop0 - wTop0) * h) + wTop1;
                c.Left = (int)((ctrLeft0) * wScale); //新旧控件之间的线性比例。控件位置只相对于窗体，所以不能加 + wLeft1
                c.Top = (int)((ctrTop0) * hScale); //
                c.Width = (int)(ctrWidth0 * wScale); //只与最初的大小相关，所以不能与现在的宽度相乘 (int)(c.Width * w);
                c.Height = (int)(ctrHeight0 * hScale); //
                _controlNo++; //累加序号
                //**放在这里，是先缩放控件本身，后缩放控件的子控件
                if (c.Controls.Count > 0)
                {
                    AutoScaleControl(c, wScale, hScale); //窗体内其余控件还可能嵌套控件(比如panel),要单独抽出,因为要递归调用
                }

                if (!(ctl is DataGridView dataGridView))
                {
                    continue;
                }

                Cursor.Current = Cursors.WaitCursor;

                var widths = 0;
                // var i = 0;
                for (var i = 0; i < dataGridView.Columns.Count; i++)
                {
                    // 自动调整列宽
                    dataGridView.AutoResizeColumn(i, DataGridViewAutoSizeColumnMode.AllCells);
                    // 计算调整列后单元列的宽度和     
                    widths += dataGridView.Columns[i].Width;
                }

                dataGridView.AutoSizeColumnsMode = widths >= dataGridView.Size.Width
                    ? DataGridViewAutoSizeColumnsMode.DisplayedCells
                    : // 调整列的模式 自动  
                    // 如果调整列的宽度大于设定列宽  
                    DataGridViewAutoSizeColumnsMode.Fill; // 如果小于 则填充  

                Cursor.Current = Cursors.Default;
            }
        }
    }
}