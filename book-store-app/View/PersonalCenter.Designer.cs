﻿namespace book_store_app.View
{
    partial class PersonalCenter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PersonalCenter));
            this.panel2 = new System.Windows.Forms.Panel();
            this.SaveEditBtn = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.address = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.phone = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.username = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.ResetPassword = new System.Windows.Forms.TextBox();
            this.RechargeBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Recharge = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ResetPasswordBtn = new System.Windows.Forms.Button();
            this.windowRestore = new System.Windows.Forms.PictureBox();
            this.windowClose = new System.Windows.Forms.PictureBox();
            this.windowMinimization = new System.Windows.Forms.PictureBox();
            this.windowMaximize = new System.Windows.Forms.PictureBox();
            this.usernameLabel = new System.Windows.Forms.Label();
            this.accountBalance = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Title = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.OrdersPic = new System.Windows.Forms.PictureBox();
            this.OrdersLabel = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ShoppingCartPic = new System.Windows.Forms.PictureBox();
            this.ShoppingCartLabel = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.PersonalCenterPic = new System.Windows.Forms.PictureBox();
            this.PersonalCenterLabel = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.ExitPic = new System.Windows.Forms.PictureBox();
            this.ExitLabel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.windowRestore)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowMinimization)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowMaximize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OrdersPic)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ShoppingCartPic)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PersonalCenterPic)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ExitPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.SaveEditBtn);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.address);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.phone);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.username);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.ResetPassword);
            this.panel2.Controls.Add(this.RechargeBtn);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.Recharge);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.ResetPasswordBtn);
            this.panel2.Controls.Add(this.windowRestore);
            this.panel2.Controls.Add(this.windowClose);
            this.panel2.Controls.Add(this.windowMinimization);
            this.panel2.Controls.Add(this.windowMaximize);
            this.panel2.Controls.Add(this.usernameLabel);
            this.panel2.Controls.Add(this.accountBalance);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.Title);
            this.panel2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel2.Location = new System.Drawing.Point(214, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1058, 860);
            this.panel2.TabIndex = 9;
            // 
            // SaveEditBtn
            // 
            this.SaveEditBtn.BackColor = System.Drawing.Color.Transparent;
            this.SaveEditBtn.FlatAppearance.BorderColor = System.Drawing.Color.ForestGreen;
            this.SaveEditBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveEditBtn.Font = new System.Drawing.Font("霞鹜文楷", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.SaveEditBtn.ForeColor = System.Drawing.Color.ForestGreen;
            this.SaveEditBtn.Location = new System.Drawing.Point(794, 423);
            this.SaveEditBtn.Name = "SaveEditBtn";
            this.SaveEditBtn.Size = new System.Drawing.Size(125, 34);
            this.SaveEditBtn.TabIndex = 59;
            this.SaveEditBtn.Text = "保存编辑";
            this.SaveEditBtn.UseVisualStyleBackColor = false;
            this.SaveEditBtn.Click += new System.EventHandler(this.SaveEditBtnClick);
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label10.Location = new System.Drawing.Point(555, 391);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 30);
            this.label10.TabIndex = 58;
            this.label10.Text = "地址";
            // 
            // address
            // 
            this.address.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.address.Location = new System.Drawing.Point(560, 423);
            this.address.Margin = new System.Windows.Forms.Padding(2);
            this.address.Name = "address";
            this.address.Size = new System.Drawing.Size(170, 34);
            this.address.TabIndex = 57;
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label12.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label12.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label12.Location = new System.Drawing.Point(321, 391);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(107, 30);
            this.label12.TabIndex = 56;
            this.label12.Text = "电话号码";
            // 
            // phone
            // 
            this.phone.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.phone.Location = new System.Drawing.Point(326, 423);
            this.phone.Margin = new System.Windows.Forms.Padding(2);
            this.phone.Name = "phone";
            this.phone.Size = new System.Drawing.Size(170, 34);
            this.phone.TabIndex = 55;
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label13.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label13.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label13.Location = new System.Drawing.Point(95, 391);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 30);
            this.label13.TabIndex = 54;
            this.label13.Text = "用户名";
            // 
            // username
            // 
            this.username.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.username.Location = new System.Drawing.Point(100, 423);
            this.username.Margin = new System.Windows.Forms.Padding(2);
            this.username.Name = "username";
            this.username.ReadOnly = true;
            this.username.Size = new System.Drawing.Size(170, 34);
            this.username.TabIndex = 53;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label4.Location = new System.Drawing.Point(647, 238);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 23);
            this.label4.TabIndex = 52;
            this.label4.Text = "重置密码";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label9.Location = new System.Drawing.Point(647, 238);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 23);
            this.label9.TabIndex = 51;
            // 
            // ResetPassword
            // 
            this.ResetPassword.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.ResetPassword.Location = new System.Drawing.Point(652, 264);
            this.ResetPassword.Name = "ResetPassword";
            this.ResetPassword.Size = new System.Drawing.Size(170, 34);
            this.ResetPassword.TabIndex = 50;
            // 
            // RechargeBtn
            // 
            this.RechargeBtn.BackColor = System.Drawing.Color.Transparent;
            this.RechargeBtn.FlatAppearance.BorderColor = System.Drawing.Color.DarkSlateBlue;
            this.RechargeBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RechargeBtn.Font = new System.Drawing.Font("霞鹜文楷", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.RechargeBtn.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.RechargeBtn.Location = new System.Drawing.Point(492, 264);
            this.RechargeBtn.Name = "RechargeBtn";
            this.RechargeBtn.Size = new System.Drawing.Size(112, 34);
            this.RechargeBtn.TabIndex = 49;
            this.RechargeBtn.Text = "充值";
            this.RechargeBtn.UseVisualStyleBackColor = false;
            this.RechargeBtn.Click += new System.EventHandler(this.RechargeBtnClick);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label2.Location = new System.Drawing.Point(269, 238);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 23);
            this.label2.TabIndex = 48;
            this.label2.Text = "充值金额";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label3.Location = new System.Drawing.Point(275, 238);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 23);
            this.label3.TabIndex = 47;
            // 
            // Recharge
            // 
            this.Recharge.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.Recharge.Location = new System.Drawing.Point(274, 264);
            this.Recharge.Name = "Recharge";
            this.Recharge.Size = new System.Drawing.Size(170, 34);
            this.Recharge.TabIndex = 46;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label1.Location = new System.Drawing.Point(50, 238);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 23);
            this.label1.TabIndex = 45;
            this.label1.Text = "账户余额";
            // 
            // ResetPasswordBtn
            // 
            this.ResetPasswordBtn.BackColor = System.Drawing.Color.Transparent;
            this.ResetPasswordBtn.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.ResetPasswordBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ResetPasswordBtn.Font = new System.Drawing.Font("霞鹜文楷", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ResetPasswordBtn.ForeColor = System.Drawing.Color.Red;
            this.ResetPasswordBtn.Location = new System.Drawing.Point(870, 264);
            this.ResetPasswordBtn.Name = "ResetPasswordBtn";
            this.ResetPasswordBtn.Size = new System.Drawing.Size(125, 34);
            this.ResetPasswordBtn.TabIndex = 44;
            this.ResetPasswordBtn.Text = "重置密码";
            this.ResetPasswordBtn.UseVisualStyleBackColor = false;
            this.ResetPasswordBtn.Click += new System.EventHandler(this.ResetPasswordBtnClick);
            // 
            // windowRestore
            // 
            this.windowRestore.BackColor = System.Drawing.Color.Transparent;
            this.windowRestore.Image = ((System.Drawing.Image)(resources.GetObject("windowRestore.Image")));
            this.windowRestore.Location = new System.Drawing.Point(1006, 3);
            this.windowRestore.Name = "windowRestore";
            this.windowRestore.Size = new System.Drawing.Size(24, 24);
            this.windowRestore.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.windowRestore.TabIndex = 43;
            this.windowRestore.TabStop = false;
            this.windowRestore.Visible = false;
            this.windowRestore.Click += new System.EventHandler(this.MaximizeOrRestore);
            // 
            // windowClose
            // 
            this.windowClose.BackColor = System.Drawing.Color.Transparent;
            this.windowClose.Image = ((System.Drawing.Image)(resources.GetObject("windowClose.Image")));
            this.windowClose.Location = new System.Drawing.Point(1030, 3);
            this.windowClose.Name = "windowClose";
            this.windowClose.Size = new System.Drawing.Size(24, 24);
            this.windowClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.windowClose.TabIndex = 42;
            this.windowClose.TabStop = false;
            this.windowClose.Click += new System.EventHandler(this.Close);
            // 
            // windowMinimization
            // 
            this.windowMinimization.BackColor = System.Drawing.Color.Transparent;
            this.windowMinimization.Image = ((System.Drawing.Image)(resources.GetObject("windowMinimization.Image")));
            this.windowMinimization.Location = new System.Drawing.Point(982, 3);
            this.windowMinimization.Name = "windowMinimization";
            this.windowMinimization.Size = new System.Drawing.Size(24, 24);
            this.windowMinimization.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.windowMinimization.TabIndex = 41;
            this.windowMinimization.TabStop = false;
            this.windowMinimization.Click += new System.EventHandler(this.Minimization);
            // 
            // windowMaximize
            // 
            this.windowMaximize.BackColor = System.Drawing.Color.Transparent;
            this.windowMaximize.Image = ((System.Drawing.Image)(resources.GetObject("windowMaximize.Image")));
            this.windowMaximize.Location = new System.Drawing.Point(1006, 3);
            this.windowMaximize.Name = "windowMaximize";
            this.windowMaximize.Size = new System.Drawing.Size(24, 24);
            this.windowMaximize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.windowMaximize.TabIndex = 40;
            this.windowMaximize.TabStop = false;
            this.windowMaximize.Click += new System.EventHandler(this.MaximizeOrRestore);
            // 
            // usernameLabel
            // 
            this.usernameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.usernameLabel.Font = new System.Drawing.Font("霞鹜文楷", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.usernameLabel.ForeColor = System.Drawing.SystemColors.GrayText;
            this.usernameLabel.Location = new System.Drawing.Point(50, 238);
            this.usernameLabel.Name = "usernameLabel";
            this.usernameLabel.Size = new System.Drawing.Size(101, 23);
            this.usernameLabel.TabIndex = 24;
            // 
            // accountBalance
            // 
            this.accountBalance.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.accountBalance.Location = new System.Drawing.Point(56, 264);
            this.accountBalance.Name = "accountBalance";
            this.accountBalance.ReadOnly = true;
            this.accountBalance.Size = new System.Drawing.Size(170, 34);
            this.accountBalance.TabIndex = 23;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(484, 96);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(70, 44);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // Title
            // 
            this.Title.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Title.Font = new System.Drawing.Font("霞鹜文楷", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Title.ForeColor = System.Drawing.Color.ForestGreen;
            this.Title.Location = new System.Drawing.Point(455, 48);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(157, 33);
            this.Title.TabIndex = 21;
            this.Title.Text = "个人中心";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Menu;
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.pictureBox5);
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(202, 860);
            this.panel1.TabIndex = 8;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.panel5.Controls.Add(this.OrdersPic);
            this.panel5.Controls.Add(this.OrdersLabel);
            this.panel5.Location = new System.Drawing.Point(18, 259);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(166, 49);
            this.panel5.TabIndex = 21;
            // 
            // OrdersPic
            // 
            this.OrdersPic.Image = ((System.Drawing.Image)(resources.GetObject("OrdersPic.Image")));
            this.OrdersPic.Location = new System.Drawing.Point(2, 3);
            this.OrdersPic.Name = "OrdersPic";
            this.OrdersPic.Size = new System.Drawing.Size(43, 43);
            this.OrdersPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.OrdersPic.TabIndex = 11;
            this.OrdersPic.TabStop = false;
            this.OrdersPic.Click += new System.EventHandler(this.OrdersPic_Click);
            // 
            // OrdersLabel
            // 
            this.OrdersLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.OrdersLabel.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.OrdersLabel.ForeColor = System.Drawing.SystemColors.MenuText;
            this.OrdersLabel.Location = new System.Drawing.Point(48, 10);
            this.OrdersLabel.Name = "OrdersLabel";
            this.OrdersLabel.Size = new System.Drawing.Size(115, 33);
            this.OrdersLabel.TabIndex = 12;
            this.OrdersLabel.Text = "书籍商城";
            this.OrdersLabel.Click += new System.EventHandler(this.OrdersLabel_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.ShoppingCartPic);
            this.panel3.Controls.Add(this.ShoppingCartLabel);
            this.panel3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel3.Location = new System.Drawing.Point(18, 175);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(166, 49);
            this.panel3.TabIndex = 23;
            // 
            // ShoppingCartPic
            // 
            this.ShoppingCartPic.Image = ((System.Drawing.Image)(resources.GetObject("ShoppingCartPic.Image")));
            this.ShoppingCartPic.Location = new System.Drawing.Point(2, 3);
            this.ShoppingCartPic.Name = "ShoppingCartPic";
            this.ShoppingCartPic.Size = new System.Drawing.Size(43, 43);
            this.ShoppingCartPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ShoppingCartPic.TabIndex = 11;
            this.ShoppingCartPic.TabStop = false;
            this.ShoppingCartPic.Click += new System.EventHandler(this.ShoppingCartPic_Click);
            // 
            // ShoppingCartLabel
            // 
            this.ShoppingCartLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ShoppingCartLabel.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ShoppingCartLabel.ForeColor = System.Drawing.SystemColors.MenuText;
            this.ShoppingCartLabel.Location = new System.Drawing.Point(51, 8);
            this.ShoppingCartLabel.Name = "ShoppingCartLabel";
            this.ShoppingCartLabel.Size = new System.Drawing.Size(110, 33);
            this.ShoppingCartLabel.TabIndex = 12;
            this.ShoppingCartLabel.Text = "购物车";
            this.ShoppingCartLabel.Click += new System.EventHandler(this.ShoppingCartLabel_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Ivory;
            this.panel4.Controls.Add(this.PersonalCenterPic);
            this.panel4.Controls.Add(this.PersonalCenterLabel);
            this.panel4.Location = new System.Drawing.Point(18, 91);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(166, 49);
            this.panel4.TabIndex = 22;
            // 
            // PersonalCenterPic
            // 
            this.PersonalCenterPic.Image = ((System.Drawing.Image)(resources.GetObject("PersonalCenterPic.Image")));
            this.PersonalCenterPic.Location = new System.Drawing.Point(2, 3);
            this.PersonalCenterPic.Name = "PersonalCenterPic";
            this.PersonalCenterPic.Size = new System.Drawing.Size(43, 43);
            this.PersonalCenterPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PersonalCenterPic.TabIndex = 11;
            this.PersonalCenterPic.TabStop = false;
            this.PersonalCenterPic.Click += new System.EventHandler(this.PersonalCenterPic_Click);
            // 
            // PersonalCenterLabel
            // 
            this.PersonalCenterLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PersonalCenterLabel.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.PersonalCenterLabel.ForeColor = System.Drawing.SystemColors.MenuText;
            this.PersonalCenterLabel.Location = new System.Drawing.Point(48, 8);
            this.PersonalCenterLabel.Name = "PersonalCenterLabel";
            this.PersonalCenterLabel.Size = new System.Drawing.Size(113, 33);
            this.PersonalCenterLabel.TabIndex = 12;
            this.PersonalCenterLabel.Text = "个人中心";
            this.PersonalCenterLabel.Click += new System.EventHandler(this.PersonalCenterLabel_Click);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Transparent;
            this.panel6.Controls.Add(this.ExitPic);
            this.panel6.Controls.Add(this.ExitLabel);
            this.panel6.Location = new System.Drawing.Point(18, 343);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(166, 49);
            this.panel6.TabIndex = 20;
            // 
            // ExitPic
            // 
            this.ExitPic.Image = ((System.Drawing.Image)(resources.GetObject("ExitPic.Image")));
            this.ExitPic.Location = new System.Drawing.Point(2, 3);
            this.ExitPic.Name = "ExitPic";
            this.ExitPic.Size = new System.Drawing.Size(43, 43);
            this.ExitPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ExitPic.TabIndex = 11;
            this.ExitPic.TabStop = false;
            this.ExitPic.Click += new System.EventHandler(this.ExitPic_Click);
            // 
            // ExitLabel
            // 
            this.ExitLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ExitLabel.Font = new System.Drawing.Font("霞鹜文楷", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ExitLabel.ForeColor = System.Drawing.SystemColors.MenuText;
            this.ExitLabel.Location = new System.Drawing.Point(51, 9);
            this.ExitLabel.Name = "ExitLabel";
            this.ExitLabel.Size = new System.Drawing.Size(112, 33);
            this.ExitLabel.TabIndex = 12;
            this.ExitLabel.Text = "退出";
            this.ExitLabel.Click += new System.EventHandler(this.ExitLabel_Click);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.Font = new System.Drawing.Font("霞鹜文楷", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.ForeColor = System.Drawing.SystemColors.MenuText;
            this.label7.Location = new System.Drawing.Point(67, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(132, 40);
            this.label7.TabIndex = 18;
            this.label7.Text = "个人中心";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(18, 13);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(45, 47);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 19;
            this.pictureBox5.TabStop = false;
            // 
            // PersonalCenter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.Indigo;
            this.ClientSize = new System.Drawing.Size(1285, 884);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PersonalCenter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PersonalCenter";
            this.Load += new System.EventHandler(this.PersonalCenter_Load);
            this.SizeChanged += new System.EventHandler(this.PersonalCenter_SizeChanged);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.windowRestore)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowMinimization)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.windowMaximize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OrdersPic)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ShoppingCartPic)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PersonalCenterPic)).EndInit();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ExitPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);
        }

        private System.Windows.Forms.PictureBox OrdersPic;
        private System.Windows.Forms.Label OrdersLabel;
        private System.Windows.Forms.PictureBox ShoppingCartPic;
        private System.Windows.Forms.Label ShoppingCartLabel;
        private System.Windows.Forms.PictureBox PersonalCenterPic;
        private System.Windows.Forms.Label PersonalCenterLabel;
        private System.Windows.Forms.PictureBox ExitPic;
        private System.Windows.Forms.Label ExitLabel;

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ResetPasswordBtn;
        private System.Windows.Forms.PictureBox windowRestore;
        private System.Windows.Forms.PictureBox windowClose;
        private System.Windows.Forms.PictureBox windowMinimization;
        private System.Windows.Forms.PictureBox windowMaximize;
        private System.Windows.Forms.Label usernameLabel;
        private System.Windows.Forms.TextBox accountBalance;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Recharge;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox ResetPassword;
        private System.Windows.Forms.Button RechargeBtn;
        private System.Windows.Forms.Button SaveEditBtn;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox address;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox phone;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox username;
    }
}