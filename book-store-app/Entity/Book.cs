﻿namespace book_store_app.Entity
{
    /// <summary>
    /// 书籍实体类
    /// </summary>
    public class Book
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Author { get; set; }

        public string Type { get; set; }

        public int Count { get; set; }

        public decimal Price { get; set; }
    }
}