﻿namespace book_store_app.Entity
{
    public struct ControlRect
    {
        //(1).声明结构,只记录窗体和其控件的初始位置和大小。
        public int Left { get; set; }
        public int Top { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
}