﻿namespace book_store_app.Entity
{
    /// <summary>
    /// 用户实体类
    /// </summary>
    public class User
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }
        
        
        public decimal Balance { get; set; }
    }
}