﻿namespace book_store_app.Entity
{
    /// <summary>
    /// 管理员实体类
    /// </summary>
    public class Admin
    {
        public int Id { get; set; }
        
        public string Username { get; set; }
        
        public string Password { get; set; }
        
        public decimal Totalsales { get; set; }
    }
}