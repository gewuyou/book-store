﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using book_store_app.View;
using book_store_app.Data;
using book_store_app.Entity;
using book_store_app.Properties;
using book_store_app.View.Navigation;

namespace book_store_app
{
    internal static class Program
    {
        private static Load _load;
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Initialize();
            Start();
        }

        /// <summary>
        /// 释放资源
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">事件参数</param>
        private static void Destroy(object sender, EventArgs e)
        {
            // 关闭并释放数据库连接
            DatabaseHelper.CloseAdnDisposeConnection();
            ViewNavigation.GetInstance().Destroy();
            _load.Close();
            _load.Dispose();
            Console.WriteLine(Resources.Program_Destroy_the_program_has_exited_);
        }

        /// <summary>
        /// 启动
        /// </summary>
        private static void Start()
        {
            Application.Run(_load);
        }

        /// <summary>
        /// 初始化
        /// </summary>
        private static void Initialize()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            _load = new Load();
            // 注册 ApplicationExit 事件的处理程序
            Application.ApplicationExit += Destroy;
            // 注册 Application.ThreadException 事件
            // Application.ThreadException += Destroy;
            // 创建购物车集合
            var context = DataContext.GetInstance();
            context.Add("shoppingCartList", new List<Book>());
        }
    }
}