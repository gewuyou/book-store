﻿namespace book_store_app.Enum
{
    /// <summary>
    /// 视图类型枚举
    /// </summary>
    public enum ViewEnum
    {
        AccountManager,
        Books,
        Login,
        Orders,
        PersonalCenter,
        ShoppingCart,
        Users,
        Register,
    }
}