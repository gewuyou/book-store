﻿using System;
using System.Collections.Generic;

namespace book_store_app.Data
{
    /// <summary>
    /// 数据上下文
    /// </summary>
    public class DataContext
    {
        private static readonly Lazy<DataContext> Instance = new Lazy<DataContext>(() => new DataContext());
        
        private readonly Dictionary<string, object> _context;

        private DataContext()
        {
            _context = new Dictionary<string, object>();
        }

        public static DataContext GetInstance()
        {
           return Instance.Value;
        }

        public void Add(string key, object value)
        {
            if (key == null)
            {
                return;
            }

            if (_context.ContainsKey(key))
            {
                _context[key] = value;
                return;
            }
            _context.Add(key, value);
        }

        public object Get(string key)
        {
            return _context.TryGetValue(key, out var value) ? value : null;
        }

        public void Remove(string key)
        {
            if (_context.ContainsKey(key))
            {
                _context.Remove(key);
            }
        }

        public void Clear()
        {
            _context.Clear();
        }

        public void ClearLoginData()
        {
            Remove("loginAdmin");
            Remove("loginUser");
        }
    }
}