﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Windows.Forms;
using book_store_app.Entity;
using book_store_app.Properties;

namespace book_store_app.Data.Dao
{
    /// <summary>
    /// 用户数据层
    /// </summary>
    public class UserDao
    {
        private static readonly Lazy<UserDao> Instance = new Lazy<UserDao>(() => new UserDao());

        /// <summary>
        /// 数据库连接
        /// </summary>
        private readonly SqlConnection _sqlConnection;

        private UserDao()
        {
            _sqlConnection = DatabaseHelper.GetConnection();
        }

        public static UserDao GetInstance()
        {
            return Instance.Value;
        }

        /// <summary>
        /// 保存用户
        /// </summary>
        /// <param name="user">用户对象</param>
        /// <returns>受影响的行数</returns>
        public int Save(User user)
        {
            var count = 0;
            try
            {
                const string sql =
                    "insert into account (username,password,address,phone,balance) values (@username,@password,@address,@phone,@balance)";
                using (var command = new SqlCommand(sql, _sqlConnection))
                {
                    command.Parameters.AddWithValue("@username", user.Username);
                    command.Parameters.AddWithValue("@password", user.Password);
                    command.Parameters.AddWithValue("@address", user.Address);
                    command.Parameters.AddWithValue("@phone", user.Phone);
                    command.Parameters.AddWithValue("@balance", user.Balance);
                    count = command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Resources.error_occurred + e.Message);
                Console.WriteLine(e.Message);
            }

            return count;
        }

        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <returns>用户列表集合</returns>
        public List<User> GetUsers()
        {
            var list = new List<User>();
            const string sql = "select id,username,password,address,phone from account";
            try
            {
                using (var command = new SqlCommand(sql, _sqlConnection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            list.Add(new User()
                            {
                                Id = reader.GetInt32(0),
                                Username = reader.GetString(1),
                                Password = reader.GetString(2),
                                Address = reader.GetString(3),
                                Phone = reader.GetString(4),
                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Resources.error_occurred + e.Message);
                Console.WriteLine(e.Message);
            }

            return list;
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="id">用户Id</param>
        /// <returns>受影响的行数</returns>
        public int Delete(int id)
        {
            var count = 0;
            try
            {
                const string sql = "delete from account where id = @id";
                using (var command = new SqlCommand(sql, _sqlConnection))
                {
                    command.Parameters.AddWithValue("@id", id);
                    count = command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Resources.error_occurred + e.Message);
                Console.WriteLine(e.Message);
            }

            return count;
        }

        /// <summary>
        /// 搜索用户
        /// </summary>
        /// <param name="searchValueText">搜索值</param>
        /// <returns>搜索结果集合</returns>
        public List<User> Search(string searchValueText)
        {
            var list = new List<User>();
            const string sql =
                "select id, username, password, address, phone  from account where username like @username or address like @address or phone like @phone";
            try
            {
                using (var command = new SqlCommand(sql, _sqlConnection))
                {
                    command.Parameters.AddWithValue("@username", "%" + searchValueText + "%");
                    command.Parameters.AddWithValue("@address", "%" + searchValueText + "%");
                    command.Parameters.AddWithValue("@phone", "%" + searchValueText + "%");
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            list.Add(new User()
                            {
                                Id = reader.GetInt32(0),
                                Username = reader.GetString(1),
                                Password = reader.GetString(2),
                                Address = reader.GetString(3),
                                Phone = reader.GetString(4),
                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Resources.error_occurred + e.Message);
                Console.WriteLine(e.Message);
            }

            return list;
        }

        /// <summary>
        /// 根据id获取用户
        /// </summary>
        /// <param name="id">用户id</param>
        /// <returns>用户</returns>
        public User GetUserById(int id)
        {
            try
            {
                const string sql = "select id, username, password, address, phone from account where id = @id";
                using (var command = new SqlCommand(sql, _sqlConnection))
                {
                    command.Parameters.AddWithValue("@id", id);
                    using (var reader = command.ExecuteReader())
                    {
                        var user = new User();
                        while (reader.Read())
                        {
                            user.Id = reader.GetInt32(0);
                            user.Username = reader.GetString(1);
                            user.Password = reader.GetString(2);
                            user.Address = reader.GetString(3);
                            user.Phone = reader.GetString(4);
                        }

                        return user;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Resources.error_occurred + e.Message);
                Console.WriteLine(e.Message);
                return null;
            }
        }

        /// <summary>
        /// 修改用户数据
        /// </summary>
        /// <param name="user">修改后的用户数据</param>
        /// <returns>影响的行数</returns>
        public int Update(User user)
        {
            var count = 0;
            try
            {
                const string sql =
                    "update account set username = @username,password = @password,address = @address,phone = @phone,balance = @balance where id = @id";
                using (var command = new SqlCommand(sql, _sqlConnection))
                {
                    command.Parameters.AddWithValue("@id", user.Id);
                    command.Parameters.AddWithValue("@username", user.Username);
                    command.Parameters.AddWithValue("@password", user.Password);
                    command.Parameters.AddWithValue("@address", user.Address);
                    command.Parameters.AddWithValue("@phone", user.Phone);
                    command.Parameters.AddWithValue("balance", user.Balance);
                    count = command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Resources.error_occurred + e.Message);
                Console.WriteLine(e.Message);
            }

            return count;
        }

        /// <summary>
        /// 修改用户数据
        /// </summary>
        /// <param name="transaction">事务</param>
        /// <param name="user">修改后的用户数据</param>
        /// <returns>影响的行数</returns>
        public int Update(SqlTransaction transaction, User user)
        {
            int count;

            const string sql =
                "update account set username = @username,password = @password,address = @address,phone = @phone,balance = @balance where id = @id";
            using (var command = new SqlCommand(sql, _sqlConnection, transaction))
            {
                command.Parameters.AddWithValue("@id", user.Id);
                command.Parameters.AddWithValue("@username", user.Username);
                command.Parameters.AddWithValue("@password", user.Password);
                command.Parameters.AddWithValue("@address", user.Address);
                command.Parameters.AddWithValue("@phone", user.Phone);
                command.Parameters.AddWithValue("balance", user.Balance);
                count = command.ExecuteNonQuery();
            }

            return count;
        }


        /// <summary>
        /// 根据用户名获取用户数据
        /// </summary>
        /// <param name="username">用户名</param>
        /// <returns>用户数据</returns>
        public User GetUserByUsername(string username)
        {
            const string sql =
                "select id, username, password, address, phone,balance from account where username = @username";
            try
            {
                using (var command = new SqlCommand(sql, _sqlConnection))
                {
                    command.Parameters.AddWithValue("@username", username);
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return new User
                            {
                                Id = reader.GetInt32(0),
                                Username = reader.GetString(1),
                                Password = reader.GetString(2),
                                Address = reader.GetString(3),
                                Phone = reader.GetString(4),
                                Balance = reader.GetDecimal(5)
                            };
                        }

                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        /// <summary>
        /// 返回总用户数
        /// </summary>
        /// <returns>总用户数</returns>
        public int UserCount()
        {
            var count = 0;
            const string sql = "select count(*) from account";
            using (var command = new SqlCommand(sql, _sqlConnection))
            {
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        count = reader.GetInt32(0);
                    }
                }
            }

            return count;
        }
    }
}