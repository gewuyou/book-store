﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using book_store_app.Entity;
using book_store_app.Properties;

namespace book_store_app.Data.Dao
{
    /// <summary>
    /// 书籍数据层
    /// </summary>
    public class BookDao
    {
        private static readonly Lazy<BookDao> Instance = new Lazy<BookDao>(() => new BookDao());

        /// <summary>
        /// 数据库连接
        /// </summary>
        private readonly SqlConnection _sqlConnection;

        private BookDao()
        {
            // 获取数据库连接
            _sqlConnection = DatabaseHelper.GetConnection();
        }

        public static BookDao GetInstance()
        {
            return Instance.Value;
        }

        /// <summary>
        /// 保存书籍
        /// </summary>
        /// <param name="book">书籍</param>
        /// <returns>受影响行数</returns>
        public int Save(Book book)
        {
            var count = 0;
            try
            {
                const string sql =
                    "insert into book(name,author,count,type,price) values (@name,@author,@count,@type,@price)";
                using (var command = new SqlCommand(sql, _sqlConnection))
                {
                    command.Parameters.AddWithValue("@name", book.Name);
                    command.Parameters.AddWithValue("@author", book.Author);
                    command.Parameters.AddWithValue("@count", book.Count);
                    command.Parameters.AddWithValue("@type", book.Type);
                    command.Parameters.AddWithValue("@price", book.Price);
                    count = command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Resources.error_occurred + e.Message);
                Console.WriteLine(e.Message);
            }

            return count;
        }

        /// <summary>
        /// 根据id获取书籍
        /// </summary>
        /// <param name="id">书籍id</param>
        /// <returns>书籍</returns>
        public Book GetBookById(int id)
        {
            try
            {
                const string sql = "select id,name,author,type,count,price from book where id = @id";
                using (var command = new SqlCommand(sql, _sqlConnection))
                {
                    command.Parameters.AddWithValue("@id", id);
                    using (var reader = command.ExecuteReader())
                    {
                        var book = new Book();
                        while (reader.Read())
                        {
                            book.Id = reader.GetInt32(0);
                            book.Name = reader.GetString(1);
                            book.Author = reader.GetString(2);
                            book.Type = reader.GetString(3);
                            book.Count = reader.GetInt32(4);
                            book.Price = reader.GetDecimal(5);
                        }

                        return book;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Resources.error_occurred + e.Message);
                Console.WriteLine(e.Message);
                return null;
            }
        }

        /// <summary>
        /// 获取书籍列表
        /// </summary>
        /// <returns>书籍列表</returns>
        public List<Book> GetBooks()
        {
            var list = new List<Book>();
            const string sql = "select id,name,author,type,count,price from book";
            try
            {
                using (var command = new SqlCommand(sql, _sqlConnection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            list.Add(new Book()
                            {
                                Id = reader.GetInt32(0),
                                Name = reader.GetString(1),
                                Author = reader.GetString(2),
                                Type = reader.GetString(3),
                                Count = reader.GetInt32(4),
                                Price = reader.GetDecimal(5)
                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Resources.error_occurred + e.Message);
                Console.WriteLine(e.Message);
            }

            return list;
        }

        /// <summary>
        /// 修改书籍数据
        /// </summary>
        /// <param name="book">修改后的书籍数据</param>
        /// <returns>影响的行数</returns>
        public int Update(Book book)
        {
            var count = 0;
            try
            {
                const string sql =
                    "update book set name = @name,author = @author,count = @count,type = @type,price = @price where id = @id";
                using (var command = new SqlCommand(sql, _sqlConnection))
                {
                    command.Parameters.AddWithValue("@id", book.Id);
                    command.Parameters.AddWithValue("@name", book.Name);
                    command.Parameters.AddWithValue("@author", book.Author);
                    command.Parameters.AddWithValue("@count", book.Count);
                    command.Parameters.AddWithValue("@type", book.Type);
                    command.Parameters.AddWithValue("@price", book.Price);
                    count = command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Resources.error_occurred + e.Message);
                Console.WriteLine(e.Message);
            }

            return count;
        }

        /// <summary>
        /// 删除书籍
        /// </summary>
        /// <param name="id">书籍ID</param>
        /// <returns>受影响行数</returns>
        public int Delete(int id)
        {
            var count = 0;
            try
            {
                const string sql = "delete from book where id = @id";
                using (var command = new SqlCommand(sql, _sqlConnection))
                {
                    command.Parameters.AddWithValue("@id", id);
                    count = command.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Resources.error_occurred + e.Message);
                Console.WriteLine(e.Message);
            }

            return count;
        }

        /// <summary>
        /// 搜索书籍
        /// </summary>
        /// <param name="searchValueText">搜索值</param>
        /// <returns>搜索结果</returns>
        public List<Book> Search(string searchValueText)
        {
            var list = new List<Book>();
            const string sql =
                "select id,name,author,type,count,price from book where name like @name or author like @author or type like @type";
            try
            {
                using (var command = new SqlCommand(sql, _sqlConnection))
                {
                    command.Parameters.AddWithValue("@name", "%" + searchValueText + "%");
                    command.Parameters.AddWithValue("@author", "%" + searchValueText + "%");
                    command.Parameters.AddWithValue("@type", "%" + searchValueText + "%");
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            list.Add(new Book()
                            {
                                Id = reader.GetInt32(0),
                                Name = reader.GetString(1),
                                Author = reader.GetString(2),
                                Type = reader.GetString(3),
                                Count = reader.GetInt32(4),
                                Price = reader.GetDecimal(5)
                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Resources.error_occurred + e.Message);
                Console.WriteLine(e.Message);
            }

            return list;
        }

        // /// <summary>
        // /// 检查指定集合中的书籍库存
        // /// </summary>
        // /// <param name="books">需要检查的集合</param>
        // /// <returns>超过库存的书籍集合</returns>
        // public List<Book> CheckInventory(IEnumerable<Book> books)
        // {
        //     var list = new List<Book>();
        //     // 收集书籍Id
        //     var bookMap = books.ToDictionary(book => book.Id);
        //     // 执行批量查询
        //     var query = $"SELECT id, count FROM Books WHERE id IN ({string.Join(",", bookMap.Keys)})";
        //     using (var command = new SqlCommand(query, _sqlConnection))
        //     {
        //         using (var reader = command.ExecuteReader())
        //         {
        //             while (reader.Read())
        //             {
        //                 var id = reader.GetInt32(0);
        //                 var count = reader.GetInt32(1);
        //                 var book = bookMap[id];
        //                 if (book.Count > count)
        //                 {
        //                     list.Add(book);
        //                 }
        //             }
        //         }
        //     }
        //
        //     return list;
        // }

        /// <summary>
        /// 更新书籍数量
        /// </summary>
        /// <param name="transaction">事务</param>
        /// <param name="bookIdAndCount">书籍Id与数量键值对</param>
        /// <returns>受影响行数</returns>
        public void UpdateBookCount(SqlTransaction transaction, Dictionary<int, int> bookIdAndCount)
        {
            const string sql =
                "update book set count = B.Count from book inner join @BookUpdateTable as B on book.id = B.Id";
            using (var command = new SqlCommand(sql, _sqlConnection, transaction))
            {
                var bookUpdateTable = new DataTable();
                bookUpdateTable.Columns.Add("Id", typeof(int));
                bookUpdateTable.Columns.Add("Count", typeof(int));

                foreach (var keyValuePair in bookIdAndCount)
                {
                    var row = bookUpdateTable.NewRow();
                    row["Id"] = keyValuePair.Key;
                    row["Count"] = keyValuePair.Value;
                    bookUpdateTable.Rows.Add(row);
                }

                var parameter = new SqlParameter("@BookUpdateTable", SqlDbType.Structured)
                {
                    Value = bookUpdateTable,
                    TypeName = "dbo.BookUpdateTableType"
                };
                command.Parameters.Add(parameter);

                command.ExecuteNonQuery();
            }

            // const string sql = "update book set count = @count where id = @id";
            //
            // using (var command = new SqlCommand(sql, _sqlConnection, transaction))
            // {
            //     var id = command.Parameters.AddWithValue("@id",null);
            //     var count = command.Parameters.AddWithValue("@count", null);
            //     foreach (var keyValuePair in bookIdAndCount)
            //     {
            //         id.Value = keyValuePair.Key;
            //         count.Value = keyValuePair.Value;
            //         command.ExecuteNonQuery();
            //     }
            // }
        }

        /// <summary>
        /// 返回书籍库存数
        /// </summary>
        /// <returns>书籍库存数</returns>
        public int BookCount()
        {
            var count = 0;
            const string sql = "select sum(count) from book";
            using (var command = new SqlCommand(sql, _sqlConnection))
            {
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        count = reader.GetInt32(0);
                    }
                }
            }

            return count;
        }
    }
}