﻿using System;
using System.Data.SqlClient;
using book_store_app.Entity;

namespace book_store_app.Data.Dao
{
    /// <summary>
    /// 管理员数据层
    /// </summary>
    public class AdminDao
    {
        private static readonly Lazy<AdminDao> Instance = new Lazy<AdminDao>(() => new AdminDao());

        /// <summary>
        /// 数据库连接
        /// </summary>
        private readonly SqlConnection _sqlConnection;

        private AdminDao()
        {
            _sqlConnection = DatabaseHelper.GetConnection();
        }

        public static AdminDao GetInstance()
        {
            return Instance.Value;
        }

        /// <summary>
        /// 根据用户名获取管理员数据
        /// </summary>
        /// <param name="username">用户名</param>
        /// <returns>管理员对象</returns>
        public Admin GetAdminByUsername(string username)
        {
            const string sql = "select id,username,password,totalSales from admin where username = @username";
            try
            {
                using (var command = new SqlCommand(sql, _sqlConnection))
                {
                    command.Parameters.AddWithValue("@username", username);
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return new Admin
                            {
                                Id = reader.GetInt32(0),
                                Username = reader.GetString(1),
                                Password = reader.GetString(2),
                                Totalsales = reader.GetDecimal(3)
                            };
                        }

                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }

        /// <summary>
        /// 更新管理员数据
        /// </summary>
        /// <param name="transaction">事务</param>
        /// <param name="admin">管理员数据</param>
        public void Update(SqlTransaction transaction, Admin admin)
        {
            const string sql =
                "update admin set username = @username, password = @password, totalSales = @totalSales where id = @id";

            using (var command = new SqlCommand(sql, _sqlConnection, transaction))
            {
                command.Parameters.AddWithValue("@username", admin.Username);
                command.Parameters.AddWithValue("@password", admin.Password);
                command.Parameters.AddWithValue("@totalSales", admin.Totalsales);
                command.Parameters.AddWithValue("@id", admin.Id);
                command.ExecuteNonQuery();
            }
        }


        /// <summary>
        /// 返回总收益
        /// </summary>
        /// <returns>总收益</returns>
        public decimal GetTotalSales()
        {
            var totalSales = decimal.Zero;
            const string sql = "select totalSales from admin";
            try
            {
                using (var command = new SqlCommand(sql, _sqlConnection))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            totalSales = reader.GetDecimal(0);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return totalSales;
        }
    }
}