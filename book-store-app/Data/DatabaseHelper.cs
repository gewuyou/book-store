﻿using System;
using System.Data.SqlClient;
using book_store_app.Properties;

namespace book_store_app.Data
{
    /// <summary>
    /// 数据库操作类
    /// </summary>
    public static class DatabaseHelper
    {
        /// <summary>
        /// MicrosoftSqlServer 连接字符串
        /// </summary>
        private const string ConnectionString = "Server=localhost;Database=book_stores;Uid=sa;Pwd=ZQ08160816zq!!;";
        // private const string ConnectionString = "Server=192.168.200.129;Database=book_stores;Uid=sa;Pwd=Ge.wuyou21126;";

        /// <summary>
        /// 懒加载连接单例
        /// </summary>
        private static readonly Lazy<SqlConnection> LazyConnection = new Lazy<SqlConnection>(() => new SqlConnection(ConnectionString));
        

        /// <summary>
        /// 获取数据库连接
        /// </summary>
        /// <returns>数据库连接</returns>
        public static SqlConnection GetConnection()
        {
            if (LazyConnection.Value.State == System.Data.ConnectionState.Closed)
            {
                LazyConnection.Value.Open();
                
            }
            return LazyConnection.Value;
        }

        /// <summary>
        /// 关闭并释放数据库连接
        /// </summary>
        public static void CloseAdnDisposeConnection()
        {
           var connection = LazyConnection.Value;
           Console.WriteLine(Resources.DatabaseHelper_CloseAdnDisposeConnection_The_Database_Connection_Is_Closed);
           connection.Close();
           Console.WriteLine(Resources.DatabaseHelper_CloseAdnDisposeConnection_The_Database_Connection_Has_Been_Released);
           connection.Dispose();
        }
    }
}